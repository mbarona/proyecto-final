<meta http-equiv=”Content-Type” content=”text/html; charset=UTF-8″ />
<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/mailer/mailer.php';

require_once('../negocio/ManejoCliente.php');
require_once('../negocio/ManejoGerente.php');
require_once('../negocio/ManejoAdministrador.php');
require_once('../negocio/ManejoFuncionario.php');
require_once('../persistencia/util/Conexion.php');

$con= new Conexion();
$conexion=$con->conectarBD();
if($conexion->connect_error){
	die("Problema de conexión con la base de datos: ".$conexion->connect_error);
}

$correo=$_POST["Correo"];
$tipo=$_POST["tipoUsuario"];
$mail=new Mailer();

if ($tipo == 1) {
  	ManejoCliente::setConexionBD($conexion);
  	$clienteDatos=ManejoCliente::consultarEmail($correo);
    $emailCliente =  $clienteDatos->getEmail();
    if (!empty($emailCliente)) {

            $identificacionCliente =  $clienteDatos->getIdentificacion();
            $nomCliente =  $clienteDatos->getNombre();
            $password=ManejoCliente::contraseniaAleatoria();

            $passEncriptada="#".password_hash($password, PASSWORD_BCRYPT);


            $clienteDatos->setContrasena($passEncriptada);
            ManejoCliente::modificarCliente ($clienteDatos);
            header("location: login.php");


    	 $txt = "¡Hola ".$nomCliente."!, Tu nueva contraseña es: <b>".$password."</b>.Cuando inicies sesión tendrás que cambiarla. <br> Cordialmente, <br> Par-Kea Colombia.";
       $mail->enviarCorreo($correo,$txt,"Recuperar Clave de Ingreso");
       header("location: login.php");

    }
    else {
    		echo '<script language="javascript">alert("No se encontro cliente");
    				window.location.href="recuperarPassword.php"
    				</script>';
    }
}
elseif ($tipo == 2) {
  ManejoFuncionario::setConexionBD($conexion);

	$encontrado = false;
							$listaFuncionarios=ManejoFuncionario::listarFuncionario();
							foreach ($listaFuncionarios as $funcionarioDatos) {
									$email = $funcionarioDatos->getEmail();
									if ($email != $correo) {

									}else {
										$encontrado = true;

									}
							}
	if ($encontrado == true) {

 $funcionarioDatos=ManejoFuncionario::consultarEmail($correo);
  $emailFuncionario=  $funcionarioDatos->getEmail();
  if (!empty($emailFuncionario)) {

      $identificacionFuncionario =  $funcionarioDatos->getIdentificacion();
      $nombreFuncionario =  $funcionarioDatos->getNombre();
      $password=ManejoFuncionario::contraseniaAleatoria();

      $passEncriptada="#".password_hash($password, PASSWORD_BCRYPT);

      $funcionarioDatos->setContrasena($passEncriptada);
      ManejoFuncionario::modificarFuncionario($funcionarioDatos);
      header("location: login.php");


    $txt = "¡Hola ".$nombreFuncionario."!, Tu nueva contraseña es: <b>".$password."</b>.Cuando inicies sesión tendrás que cambiarla. <br> Cordialmente, <br> Par-Kea Colombia.";
   $mail->enviarCorreo($correo,$txt,"Recuperar Clave de Ingreso");
  }
}else {
	echo '<script language="javascript">alert("Este usuario no se encuentra registrado");
	window.location.href="recuperarPassword.php"
	</script>';
}
}
elseif ($tipo == 3) {
  ManejoGerente::setConexionBD($conexion);
  $gerenteDatos=ManejoGerente::consultarEmail($correo);
  $emailGerente =  $gerenteDatos->getEmail();
  if (!empty($emailGerente)) {

    $identificacionGerente =  $gerenteDatos->getIdentificacion();
    $nomGerente =  $gerenteDatos->getNombre();
    $password=ManejoGerente::contraseniaAleatoria();

    $passEncriptada="#".password_hash($password, PASSWORD_BCRYPT);

    $gerenteDatos->setContrasena($passEncriptada);

    ManejoGerente::modificarGerente ($gerenteDatos);
    header("location: login.php");

    $txt = "¡Hola ".$nomGerente."!, Tu nueva contraseña es: <b>".$password."</b>.Cuando inicies sesión tendrás que cambiarla. <br> Cordialmente, <br> Par-Kea Colombia.";
    $mail->enviarCorreo($correo,$txt,"Recuperar Clave de Ingreso");
    header("location: login.php");
  }
  else {
  		echo '<script language="javascript">alert("No se Encontro Administrador");
  				window.location.href="recuperarPassword.php"
  				</script>';
  }
}
elseif ($tipo == 4) {
  ManejoAdministrador::setConexionBD($conexion);
  $adminDatos=ManejoAdministrador::consultarEmail($correo);
  $emailAdmin =  $adminDatos->getEmail();
  if (!empty($emailAdmin)) {

    $identificacionAdmin =  $adminDatos->getIdentificacion();
    $nombreAdmin =  $adminDatos->getNombre();
    $password=ManejoAdministrador::contraseniaAleatoria();

    $passEncriptada="#".password_hash($password, PASSWORD_BCRYPT);

    $adminDatos->setContrasena($passEncriptada);
    ManejoAdministrador::modificarAdministrador($adminDatos);
    header("location: login.php");

    $txt = "¡Hola ".$nomAdmin."!, Tu nueva contraseña es: <b>".$password."</b>.Cuando inicies sesión tendrás que cambiarla. <br> Cordialmente, <br> Par-Kea Colombia.";
    $mail->enviarCorreo($correo,$txt,"Recuperar Clave de Ingreso");

  }
  else {
  		echo '<script language="javascript">alert("No se Encontro Administrador");
  				window.location.href="recuperarPassword.php"
  				</script>';
  }
}

 ?>
