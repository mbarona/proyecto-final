<?php
session_start();

require_once('../negocio/ManejoCliente.php');
require_once('../negocio/ManejoGerente.php');
require_once('../negocio/ManejoAdministrador.php');
require_once('../negocio/ManejoFuncionario.php');
require_once('../negocio/ManejoParqueadero.php');
require_once('../negocio/ManejoZona.php');

require_once('../negocio/ManejoAuditoria.php');
require_once('../negocio/Auditoria.php');
require_once('../persistencia/util/Conexion.php');
$con= new Conexion();
$conexion=$con->conectarBD();
$correo=$_POST["Correo"];
$password=$_POST["password"];
$tipo=$_POST["tipoUsuario"];

if($conexion->connect_error){
	die("Problema de conexión con la base de datos: ".$conexion->connect_error);
}
if(isset($_POST['g-recaptcha-response'])){
//La respuesta del recaptcha
$respuesta=$_POST['g-recaptcha-response'];
//La ip del usuario
$ipuser=$_SERVER['REMOTE_ADDR'];
//Tu clave secretra de recaptcha
$clavesecreta='6LctyvsUAAAAABrlfk3tMCOCfCnPL3CMsHFUvgE-';
//La url preparada para enviar
$urlrecaptcha="https://www.google.com/recaptcha/api/siteverify?secret=$clavesecreta&response=$respuesta&remoteip=$ipuser";
//Leemos la respuesta (suele funcionar solo en remoto)
$respuesta = file_get_contents($urlrecaptcha) ;
//Comprobamos el success
$dividir=explode('"success":',$respuesta);
$obtener=explode(',',$dividir[1]);
//Obtenemos el estado
$estado=trim($obtener[0]);
}

if ($estado=='true'){
	#==================================================================
	#							Ingreso Cliente
	#==================================================================
					if ($tipo == 1) {

					ManejoCliente::setConexionBD($conexion);
					$clienteValidado=ManejoCliente::iniciarSesion($correo, $password);
					$cliente=$clienteValidado[0];
					$primerIngreso=$clienteValidado[1];

					$clienteDatos=ManejoCliente::consultarEmail($correo);

					if ($clienteDatos->getIdentificacion() != null || !empty($clienteDatos->getIdentificacion()) ) {

									if (!empty($cliente)) {
									$estadoC = $cliente->getEstado();
									if ($estadoC == 0) {
									echo '<script language="javascript">alert("Este usuario ha sido inhabilitado, así que no puede ingresar al sistema. Por favor pongase en contacto con el administrdor para solucionar este problema.");
										window.location.href="login.php"
										</script>';
									}else {

													$_SESSION['tipoUsuario']=1;
													$_SESSION['email']=$correo;
													$_SESSION['CodCliente'] = $cliente->getIdentificacion();
													$_SESSION['NomCliente'] = $cliente->getNombre();
													$_SESSION['intentosCli'] = $cliente->getIntentosFallidos();
													$_SESSION['numTarjeta']=null;

													// Auditoria login
													$auditoria = new Auditoria();
													$auditoria->setCedula_usuario($_SESSION['CodCliente']);
													$auditoria->setCod_afectado(0);
													$auditoria->setTabla("-");
													$auditoria->setTipo_operacion("Inicio de sesión");
													ManejoAuditoria::setConexionBD($conexion);
													ManejoAuditoria::crearAuditoria($auditoria);
					if($primerIngreso==1){
					echo "es el primer ingreso";
					header("location: /proyecto-final/parkeaColombia/presentacion/cambiarContrasena.php");
					}else{
					header("location: /proyecto-final/parkeaColombia/presentacion/ModuloCliente/index.php");

					$cliente->setIntentosFallidos(0);
					ManejoCliente::modificarCliente($cliente);
					}
					}
					}else{


					$codCliente=$clienteDatos->getIdentificacion();
					$intentosFallidos=$clienteDatos->getIntentosFallidos();


					$clienteDatos->setIdentificacion($codCliente);
					$clienteDatos->setIntentosFallidos($intentosFallidos+1);

					if(($intentosFallidos+1)>=3){
					$clienteDatos->setEstado(0);
					echo '<script language="javascript">alert("Se ha bloqueado el Ingreso a este Cliente debido al intento repetitivo de Contraseña, pongase en contacto con el administrador para solucionar este problema");
					window.location.href="login.php"
					</script>';
					}
					ManejoCliente::modificarCliente($clienteDatos);
					echo '<script language="javascript">alert("Contraseña Incorrecta, vuelva a intentarlo");
					window.location.href="login.php"
					</script>';
					}


				}else {
					echo '<script language="javascript">alert("No se Encontro Cliente");
					window.location.href="login.php"
					</script>';
				}

					#==================================================================
					#							Ingreso Funcionario
					#==================================================================

					}elseif ($tipo ==2 ) {

					ManejoFuncionario::setConexionBD($conexion);

					$funcionarioValidado=ManejoFuncionario::iniciarSesion($correo, $password);
					$funcionario=$funcionarioValidado[0];
					$indicadorDePass=$funcionarioValidado[1];
					$funcionarioDatos=ManejoFuncionario::consultarEmail($correo);
					if ($funcionarioDatos->getIdentificacion() != null || ! !empty($funcionarioDatos->getIdentificacion())) {


					if (!empty($funcionario)) {
					$estadoF = $funcionario->getEstado();
					if ($estadoF == 0) {
					echo '<script language="javascript">alert("Este usuario ha sido inhabilitado, así que no puede ingresar al sistema. Por favor pongase en contacto con el administrdor para solucionar este problema.");
						window.location.href="login.php"
						</script>';
					}else {
					$_SESSION['tipoUsuario']=2;
					$_SESSION['email']=$correo;
					$_SESSION['NombreFuncionario'] = $funcionario->getNombre();
					$_SESSION['identificacion_funcionario'] = $funcionario->getIdentificacion();
					$_SESSION['cod_parqueadero']=$funcionario->getParqueadero()->getCodParqueadero();
					$_SESSION['numTarjeta']=null;

					ManejoParqueadero::setConexionBD($conexion);

					$parqueadero=ManejoParqueadero::buscarParqueadero($funcionario->getParqueadero()->getCodParqueadero());
					$_SESSION['nom_parqueadero']=$parqueadero->getNomParqueadero();

					$auditoria = new Auditoria();
					$auditoria->setCedula_usuario($_SESSION['identificacion_funcionario']);
					$auditoria->setCod_afectado(0);
					$auditoria->setTabla("-");
					$auditoria->setTipo_operacion("Inicio de sesión");
					ManejoAuditoria::setConexionBD($conexion);
					ManejoAuditoria::crearAuditoria($auditoria);


					if($indicadorDePass==1){
					header("location: /proyecto-final/parkeaColombia/presentacion/cambiarContrasena.php");
					}else{

						$funcionario->setIntentosFallidos(0);
							ManejoFuncionario::modificarFuncionario($funcionario);
					header("location: /proyecto-final/parkeaColombia/presentacion/ModuloFuncionario/indexFuncionario.php");
					}
					}
					}
					else {

					$codFuncionario=$funcionarioDatos->getIdentificacion();
					$intentosFallidos=$funcionarioDatos->getIntentosFallidos();

					$funcionarioDatos->setIdentificacion($codFuncionario);
					$funcionarioDatos->setIntentosFallidos($intentosFallidos+1);

					if(($intentosFallidos+1)>=3){
					$funcionarioDatos->setEstado(0);
					echo '<script language="javascript">alert("Se ha bloqueado el Ingreso a este Funcionario debido al intento repetitivo de Contraseña, pongase en contacto con el administrador para solucionar este problema");
					window.location.href="login.php"
					</script>';
					}
					ManejoFuncionario::modificarFuncionario($funcionarioDatos);
					echo '<script language="javascript">alert("Contraseña Incorrecta, vuelva a intentarlo");
					window.location.href="login.php"
					</script>';
				}

				}else {
					echo '<script language="javascript">alert("No se Encontro Funcionario");
					window.location.href="login.php"
					</script>';
					// code...
				}


					#==================================================================
					#							Ingreso Gerente
					#==================================================================
					}elseif ($tipo ==3 ) {
					ManejoGerente::setConexionBD($conexion);
					$gerenteValidado=ManejoGerente::iniciarSesion($correo, $password);
					$gerente=$gerenteValidado[0];
					$indicadorDePass=$gerenteValidado[1];

					if (!empty($gerente)) {
					$estadoG = $gerente->getEstado();
					if ($estadoG == 0) {
					echo '<script language="javascript">alert("Este usuario ha sido inhabilitado, así que no puede ingresar al sistema. Por favor pongase en contacto con el administrdor para solucionar este problema.");
						window.location.href="login.php"
						</script>';
					}else {

					$_SESSION['tipoUsuario']=3;
					$_SESSION['email']=$correo;
					$_SESSION['nombreGerente'] = $gerente->getNombre();
					$_SESSION['identificacion_gerente'] = $gerente->getIdentificacion();
					$_SESSION['cod_zona']=$gerente->getZona();
					ManejoZona::setConexionBD($conexion);

					$zona=ManejoZona::consultarZona($gerente->getZona());
					$_SESSION['nom_zona']=$zona->getNomZona();

					$auditoria = new Auditoria();
					$auditoria->setCedula_usuario($_SESSION['identificacion_gerente']);
					$auditoria->setCod_afectado(0);
					$auditoria->setTabla("-");
					$auditoria->setTipo_operacion("Inicio de sesión");
					ManejoAuditoria::setConexionBD($conexion);
					ManejoAuditoria::crearAuditoria($auditoria);


					if($indicadorDePass==1){
					header("location: /proyecto-final/parkeaColombia/presentacion/cambiarContrasena.php");
					}else{
					$gerente->setIntentosFallidos(0);
					ManejoGerente::modificarGerente($gerente);
					header("location: /proyecto-final/parkeaColombia/presentacion/gerente.php");
					}
					}
					}else {
					$gerenteDatos=ManejoGerente::consultarEmail($correo);
					if ($gerenteDatos != null) {


					$codGerente=$gerenteDatos->getIdentificacion();
					$intentosFallidos=$gerenteDatos->getIntentosFallidos();


					$gerenteDatos->setIdentificacion($codGerente);
					$gerenteDatos->setIntentosFallidos($intentosFallidos+1);

					if(($intentosFallidos+1)>=3){
					$gerenteDatos->setEstado(0);
					echo '<script language="javascript">alert("Se ha bloqueado el Ingreso a este Gerente debido al intento repetitivo de Contraseña, pongase en contacto con el administrador para solucionar este problema");
					window.location.href="login.php"
					</script>';
					}
					ManejoGerente::modificarGerente($gerenteDatos);
					echo '<script language="javascript">alert("Contraseña Incorrecta, vuelva a intentarlo");
					window.location.href="login.php"
					</script>';
					}

					echo '<script language="javascript">alert("Este usuario no se encuentra registrado");
					window.location.href="login.php"
					</script>';
					}

					#==================================================================
					#							Ingreso Administrador
					#==================================================================

					}elseif ($tipo ==4 ) {
					ManejoAdministrador::setConexionBD($conexion);
					$adminValidado=ManejoAdministrador::iniciarSesion($correo, $password);
					$admin=$adminValidado[0];
					$indicadorDePass=$adminValidado[1];
					if (!empty($admin)) {
					$_SESSION['tipoUsuario']=4;
					$_SESSION['identificacion_admin'] = $admin->getIdentificacion();
					$_SESSION['nom_admin'] = $admin->getNombre();
					$_SESSION['email']=$correo;

					$auditoria = new Auditoria();
					$auditoria->setCedula_usuario($_SESSION['identificacion_admin']);
					$auditoria->setCod_afectado(0);
					$auditoria->setTabla("-");
					$auditoria->setTipo_operacion("Inicio de sesión");
					ManejoAuditoria::setConexionBD($conexion);
					ManejoAuditoria::crearAuditoria($auditoria);


							if($indicadorDePass==1){
								header("location: /proyecto-final/parkeaColombia/presentacion/cambiarContrasena.php");
							}else{
								header("location: /proyecto-final/parkeaColombia/presentacion/administrador.php");
							}
					}else{
					echo '<script language="javascript">alert("Este usuario no se encuentra registrado");
					window.location.href="login.php"
					</script>';
					}
					}
				} else if ($estado=='false'){
					echo '<script language="javascript">alert("La validación reCaptcha debe ser completada, verifique y vuelva a intentarlo.");
					window.location.href="login.php"
					</script>';
}


mysqli_close($conexion);


?>
