<?php
session_start();
require_once '../negocio/ManejoCliente.php';
require_once '../negocio/ManejoCliente.php';
require_once '../negocio/ManejoGerente.php';
require_once '../negocio/ManejoAdministrador.php';
require_once '../negocio/ManejoFuncionario.php';


$con= new Conexion();
$conexion=$con->conectarBD();

$tipoUsuario=$_SESSION['tipoUsuario'];
$contrasena=$_POST['password'];
$confirPass=$_POST['confPassword'];

if($contrasena!=$confirPass){
  $_SESSION["mensaje"]="¡La contraseña debe coincidir en los dos campos!";
  header('location: cambiarContrasena.php');
}else{


if($tipoUsuario==1){
    $codCliente=$_SESSION['CodCliente'];
    $passEncriptada=password_hash($contrasena, PASSWORD_BCRYPT);


    ManejoCliente::setConexionBD($conexion);
    $cliente=ManejoCliente::consultarCliente($codCliente);
    $cliente->setContrasena($passEncriptada);
    ManejoCliente::modificarCliente($cliente);


    header("location: /proyecto-final/parkeaColombia/presentacion/ModuloCLiente/index.php");
}elseif($tipoUsuario==2){
  $codFuncionario=$_SESSION['identificacion_funcionario'];
  $passEncriptada= password_hash($contrasena, PASSWORD_BCRYPT);

  ManejoFuncionario::setConexionBD($conexion);
  $funcionario=ManejoFuncionario::consultarFuncionario($codFuncionario);
  $funcionario->setContrasena($passEncriptada);
  ManejoFuncionario::modificarFuncionario($funcionario);

  header("location: /proyecto-final/parkeaColombia/presentacion/ModuloFuncionario/indexFuncionario.php");

}elseif($tipoUsuario==3){

  $codGerente=$_SESSION['identificacion_gerente'];
  $passEncriptada=password_hash($contrasena, PASSWORD_BCRYPT);
  ManejoGerente::setConexionBD($conexion);
  $gerente=ManejoGerente::consultarGerente($codGerente);
  $gerente->setContrasena($passEncriptada);
  ManejoGerente::modificarGerente($gerente);
  header("location: /proyecto-final/parkeaColombia/presentacion/gerente.php");

}
elseif($tipoUsuario==4){

  $idAdmin=$_SESSION['identificacion_admin'];
  $passEncriptada=password_hash($contrasena, PASSWORD_BCRYPT);
  ManejoAdministrador::setConexionBD($conexion);
  $admin=ManejoAdministrador::consultarAdministrador($idAdmin);
  $admin->setContrasena($passEncriptada);
  ManejoAdministrador::modificarAdministrador($admin);
  header("location: /proyecto-final/parkeaColombia/presentacion/administrador.php");
}
}


?>
