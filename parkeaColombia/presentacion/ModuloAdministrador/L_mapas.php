<?php
/**    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
*/
  
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';

  $idciudad = $_POST["idciudad"];
  $pieces = explode(",", $idciudad);
  $obj=new Conexion();
  $conexion=$obj->conectarBD();

  ManejoParqueadero::setConexionBD($conexion);

  ManejoZona::setConexionBD($conexion);
  $zonas= ManejoZona::ZonasConParqueadero($pieces[0]);
  $totalz=count($zonas);
 ?>

    <style>
        .navbar .navbar-nav li.activeMapas1 .menu-icon,
        .navbar .navbar-nav li:hover .toggle_nav_button:before,
        .navbar .navbar-nav li .toggle_nav_button.nav-open:before {
            color: #04dd1f;
        }
        
        .navbar .navbar-nav>.activeMapas1>a,
        .navbar .navbar-nav>.activeMapas1>a:focus,
        .navbar .navbar-nav>.activeMapas1>a:hover {
            color: #04dd1f;
        }
        
        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
        
        .mapas {
            height: 100%;
            position: absolute;
            top: 0;
            left: 0%;
            bottom: 0;
            width: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
    </style>
   
   
   <div class="col-lg-11">
            <div class="card-header" style="background-color: rgb(127, 255, 119);">
                <h4>Parqueaderos de las zonas de <?php echo $pieces[1]?></h4>
                <?php
                    $totalParqueaderos=0;
                      for ($i=0; $i <=$totalz-1; $i++) {
                        $parzona = ManejoParqueadero::parqueaderosEnEstaZona($zonas[$i]->getCodZona());
                        $totalParqueaderos+=count($parzona);
                      }
                      ?>
                <h5>Total Parqueaderos: <?php echo $totalParqueaderos?></h5>
                   
        </div>
    <br>
    <div class="row">

    

    </div>
    <div class="row">
        <?php
                      for ($i=0; $i <=$totalz-1; $i++) {
                        $parzona = ManejoParqueadero::parqueaderosEnEstaZona($zonas[$i]->getCodZona());

                          echo '
                          <div class="col-lg-6">
                              <div class="card">
                                  <div class="card-header" style="background-color: rgb(189, 255, 222);">
                                    <strong></strong><small>Zona: '.$zonas[$i]->getNomZona().' - No. Parqueaderos: '.count($parzona).'</small>
                                  </div>
                                  <div class="card-body card-block" style="padding: 10em;">
                        <div id="map'.$i.'" class="mapas"></div>
                        </div>
                        </div>
                    </div>
                ';
                        }
                ?>
    </div>
 </div>
    <script>
        var map2;

        function initMap() {
            <?php
							if(count($zonas) == 0  ){
              
              }else{
                
                for ($i=0; $i <= $totalz-1; $i++) {
                              
                $parzona = ManejoParqueadero::parqueaderosEnEstaZona($zonas[$i]->getCodZona());
  
                $mapss = 'map'.$i;
                ?>
            map2 = new google.maps.Map(document.getElementById(<?php echo json_encode($mapss); ?>), {
                center: {
                    lat: <?php echo $parzona[0]->getLatitud(); ?>,
                    lng: <?php echo $parzona[0]->getLongitud(); ?>
                },
                zoom: 15
            });

            var marker;
            <?php
								foreach($parzona as $p){
                  
              ?>
            var marker = new google.maps.Marker({
                position: {
                    lat: <?php echo $p->getLatitud(); ?>,
                    lng: <?php echo $p->getLongitud(); ?>
                },
                map: map2,
                title: <?php echo json_encode($p->getDireccion().' -- '.$p->getNomParqueadero()); ?>
            });

            <?php
              		
								}
              }	
            }
              ?>

        }
    </script>