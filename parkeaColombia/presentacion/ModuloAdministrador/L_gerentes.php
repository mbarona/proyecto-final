<?php
/**    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
*/
	require_once ('../persistencia/util/Conexion.php');
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoGerente.php';
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoFuncionario.php';
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Auditoria.php';
    $obj=new Conexion();
	$conexion=$obj->conectarBD();

	ManejoGerente::setConexionBD($conexion);
	$totalgerente= ManejoGerente::totalGerentes();

	if(isset($_POST['cambiar'])){
		$gerente= ManejoGerente::consultarGerente($_POST['cod']);
		$estado= $gerente->getEstado();
		if($estado==0){
			$gerente->setEstado(1);
		}else{
			$gerente->setEstado(0);
		}
		ManejoGerente::modificarEstado($gerente);
		$auditoria = new Auditoria();
		$auditoria->setCedula_usuario(0);
		$auditoria->setCod_afectado($gerente->getIdentificacion());
		$auditoria->setTabla("GERENTE");
		$auditoria->setTipo_operacion("Cambiar estado");
		ManejoAuditoria::setConexionBD($conexion);
		ManejoAuditoria::crearAuditoria($auditoria);

	}
	$gerentes= ManejoGerente::listarGerentes();

	ManejoFuncionario::setConexionBD($conexion);
	$totalfuncionario= ManejoFuncionario::totalFuncionarios();

	ManejoZona::setConexionBD($conexion);
?>
<style>
.navbar .navbar-nav li.activeGerente .menu-icon, .navbar .navbar-nav li:hover .toggle_nav_button:before, .navbar .navbar-nav li .toggle_nav_button.nav-open:before {
    color: #04dd1f;
}
.navbar .navbar-nav>.activeGerente>a, .navbar .navbar-nav>.activeGerente>a:focus, .navbar .navbar-nav>.activeGerente>a:hover {
    color: #04dd1f;
}
/*  Define the background color for all the ODD background rows  */
.TFtable tr:nth-child(odd){

	    background-color: rgba(105, 243, 128, 0.54)!important;

	}
	/*  Define the background color for all the EVEN background rows  */
	.TFtable tr:nth-child(even){
		background-color: #e3ffe1!important;

	}
	tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(105, 243, 128, 0.54);
}.table-striped tbody tr:nth-of-type(even) {
    background-color: #e3ffe1!important;
}
button.dt-button, a.dt-button {
    background-image: linear-gradient(to bottom, #5efa6a 0%, #e9e9e9 100%) !important;
	border-radius: 63px !important;
}
.dataTables_length{
    color: #333;
	margin-right:3em !important;
}
div.dt-button-collection {
    top: 0 !important;
	left: 150px !important;
	background-color: rgba(255, 255, 255, 0.8) !important;
}
</style>

<link rel="stylesheet" type="text/css" href="./diseño/admin/css/datatables.min.css"/>
<script src="./diseño/admin/js/datatables.min.js"></script>
<script src="./diseño/admin/js/pdfmake.min.js"></script>
<script src="./diseño/admin/js/vfs_fonts.js"></script>
<div class="animated fadeIn">
	<div class="row">
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<div class="card-body">
					<div class="stat-widget-five">
						<div class="stat-icon dib flat-color-5">
							<i class=" fa fa-users"></i>
						</div>
						<div class="stat-content">
							<div class="text-left dib">
								<div class="stat-text"><span class="count"><?php echo $totalfuncionario+$totalgerente; ?></span></div>
								<div class="stat-heading">Total Empleados</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<div class="card-body">
					<div class="stat-widget-five">
						<div class="stat-icon dib flat-color-5">
							<i class="pe-7s-users"></i>
						</div>
						<div class="stat-content">
							<div class="text-left dib">
								<div class="stat-text"><span class="count"><?php echo $totalgerente; ?></span></div>
								<div class="stat-heading" style="margin-right: 20px;">Gerentes</div>
								<form name="menu" method="get">
								<input id="menu" name="menu" type="hidden" value="registrarGerente">

							</div>
							<div class="text-right dib"><button type="submit" class="btn-warning btn-sm">Registrar</button></div>
								</form>
						</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-14">
			<div class="card">
				<div class="card-header">
					<strong class="card-title">Lista de Gerentes</strong>
				</div>
				<div class="card-body">
					<table id="example" class="table table-striped table-bordered TFtable">
						<thead>
							<tr style="background-color: rgb(255, 255, 255)!important;">
								<th><center>No.</center></th>
								<th><center>IDENTIFICACIÓN</center></th>
								<th><center>NOMBRE COMPLETO</center></th>
								<th><center>E-MAIL</center></th>
								<th><center>ESTADO</center></th>
								<th><center>INTENTOS</center></th>
								<th><center>ZONA</center></th>
								<th><center>ACCIONES</center></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$salida="";
							if(count($gerentes) == 0 ){
								echo '<tr><td colspan="7">No hay datos.</td></tr>';
							}else{
								$no = 1;
								foreach($gerentes as $g){
									$zona=ManejoZona::consultarZona($g->getZona());
									echo '
										<tr>
											<td><center>'.$no.'</center></td>
											<td><center>'.$g->getIdentificacion().'</center></td>
											<td><center>'.$g->getNombre().'</center></td>
											<td><center>'.$g->getEmail().'</center></td>';
											if($g->getEstado()==1){
												echo '<td><center>Activo</center></td>';
											}else{
												echo '<td><center>Inactivo</center></td>';
											}
											echo'
											<td><center>'.$g->getIntentosFallidos().'</center></td>
											<td><center>'.$zona->getNomZona().'</center></td>
											<td>
											<center>
												<form action="Administrador.php?menu=gerentes" method="POST">
													<button type="submit" style="background-color: Transparent;
													border: none;
													cursor:pointer;
													" title="Cambiar estado" name="cambiar">
														<input type="hidden" name="cod" value="'.$g->getIdentificacion().'">
														<i class="menu-icon fa fa-exchange"></i>
													</button>
												</form>
											</center>
										</td>
										</tr>';
									$no++;
								}
								$salida.="</tbody><tfoot>
							<tr>
								<th>No</th>
								<th>ID</th>
								<th>Nombre</th>
								<th>E-MAIL</th>
								<th>Estado</th>
								<th>Intentos</th>
								<th>Zona</th>
								<td></td>

							</tr>
						</tfoot>";
							$salida.="</table>";

						}

						echo $salida;
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$path = 'diseño/images/logo.jpg';
$type = pathinfo($path, PATHINFO_EXTENSION);
$data = file_get_contents($path);
$base64 = "'data:image/" . $type . ";base64," . base64_encode($data)."'";
?>
<script>
	$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="'+title+'" />' );
    } );

    // DataTable
    var table = $('#example').DataTable({
      		dom: 'lBfrtip',
			buttons: [{
				extend: 'collection',
				text: 'Exportar tabla',
				buttons: [
					{
					extend: 'copyHtml5',
					text: 'Copiar',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4,6 ]
					}},
						{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4,6 ]
					}},
					{
					extend: 'csvHtml5',
					text: 'CSV',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4,6 ]
					}} ,
						{
					extend: 'pdfHtml5',
					text: 'PDF',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4,6 ]
					},
					 customize: function(doc) {
						doc.styles.tableHeader.fillColor = 'green';

						doc.content.splice( 1, 0, {
							margin: [ 0, 0, 0, 6 ],
							alignment: 'center',
							image: <?php echo $base64;?>,
							width:50
						} );
						doc.content.splice(0, 1, {
								text: [
										   { text: 'Modulo Administrador \n',bold:true,fontSize:15 },
										   { text: 'Lista de Gerentes \n',italics:true,fontSize:12 },
										   { text: 'Par-Kea Colombia',italics:true,fontSize:9 }

								],
								margin: [0, 0, 0, 12],
								alignment: 'center'
						});
						doc['footer'] = (function(page, pages) {
                            return {
                            columns: [
                                {
                                alignment: 'center',
                                text: [
                                    { text: page.toString(), italics: true },
                                    ' of ',
                                    { text: pages.toString(), italics: true }
                                ]
                            }],
                            margin: [10, 0]
                            }
                        });
					 },
				title:'Gerentes'
				},
				'print'

				]}], initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;

                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );

        },
		"language": idioma_espanol
    });
} );
var idioma_espanol={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }
};
</script>
