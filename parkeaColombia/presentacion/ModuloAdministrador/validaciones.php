<?php
  function validaRequerido($valor){
      if(trim($valor) == ''){
        return false;
      }else{
         return true;
      }
    }

  function validarEntero($valor, $opciones=null){
      if(filter_var($valor, FILTER_VALIDATE_INT, $opciones) === FALSE){
        return false;
      }else{
        if($valor > 0){
          return true;
        }else{
          return false;
        }
      }
  }
  function validarCapacidad($valor, $opciones=null){
    if(filter_var($valor, FILTER_VALIDATE_INT, $opciones) === FALSE){
      return false;
    }else{
      if($valor > 0 AND $valor < 1001){
        return true;
      }else{
        return false;
      }
    }
}

  function validarLista($valor){
      if(trim($valor) == 0){
        return false;
      }else{
        return true;
      }
  }

  function is_valid_email($str)
  {
    $result = (false !== filter_var($str, FILTER_VALIDATE_EMAIL));
    
    if ($result)
    {
      list($user, $domain) = explode('@', $str);
      
      $result = checkdnsrr($domain, 'MX');
    }
    
    return $result;
  }
?>