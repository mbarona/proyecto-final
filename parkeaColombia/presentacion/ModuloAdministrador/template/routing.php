<?php 
	if(isset($_GET['menu'])){
		if ($_GET['menu']=='inicio'){
			include_once('ModuloAdministrador/inicio.php');
		}
		if ($_GET['menu']=='gerentes') {
			include_once('ModuloAdministrador/L_gerentes.php');
		}
		if ($_GET['menu']=='funcionarios') {
			include_once('ModuloAdministrador/L_funcionarios.php');
		}
		if ($_GET['menu']=='parqueaderos'){
			include_once('ModuloAdministrador/L_parqueaderos.php');
		}
		if ($_GET['menu']=='zonas'){
			include_once('ModuloAdministrador/L_zonas.php');
		}
		if ($_GET['menu']=='clientes'){
			include_once('ModuloAdministrador/L_clientes.php');
		}
		if ($_GET['menu']=='mapas'){
			include_once('ModuloAdministrador/L_mapas.php');
		}
		if ($_GET['menu']=='mapas0'){
			include_once('ModuloAdministrador/L_mapas0.php');
		}
		if ($_GET['menu']=='registrarGerente') {
			include_once('ModuloAdministrador/F_registrarGerente.php');
		}
		if ($_GET['menu']=='registrarFuncionario'){
			include_once('ModuloAdministrador/F_registrarFuncionario.php');
		}
		if ($_GET['menu']=='registrarParqueadero'){
			include_once('ModuloAdministrador/F_registrarParqueadero.php');
		}
		if ($_GET['menu']=='registrarZona'){
			include_once('ModuloAdministrador/F_registrarZona.php');
		}
		if ($_GET['menu']=='auditoria') {
			include_once('ModuloAdministrador/L_auditoria.php');
		}
		if ($_GET['menu']=='editarParametro'){
			include_once('ModuloAdministrador/F_update_parametro.php');
		}
	} else {
		include_once('ModuloAdministrador/inicio.php');
	}
 ?>

