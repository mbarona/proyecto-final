<style>
.navbar .navbar-nav li>a .menu-icon {
    width: 40px;
}
.right-panel {
            background: #f1f2f7;
            margin-left: 250px;
            margin-top: 0px;
}
         
.left-panel { 
        position: relative;
        float:left;
        width:250px; 
        top: 0px;
}
.navbar .navbar-nav>li {
    padding-left: 26px !important;
    padding-right: 26px !important;
}
.navbar .navbar-nav li>a .menu-icon {
    margin-top: 5px !important;
   
}
</style>
<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/presentacion/ModuloAdministrador/reportes/reportes.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoFactura.php';
$obj=new Conexion();
$conexion=$obj->conectarBD();

ManejoFactura::setConexionBD($conexion);

$años= ManejoFactura::añoFactura();

if(array_key_exists('button2', $_POST)) { 
        $Final = isset($_POST['fin']) ? $_POST['fin'] : null;
        $inicio = isset($_POST['start']) ? $_POST['start'] : null;
        
 ?>
        <script type="text/javascript">
        window.open(<?php reportefecha($inicio,$Final) ?>, '_blank');
        </script>
<?php    
}

if(array_key_exists('button3', $_POST)) { 
        $año = isset($_POST['año']) ? $_POST['año'] : null;
       
 ?>
        <script type="text/javascript">
        window.open(<?php reporteaño($año) ?>, '_blank');
        </script>
<?php    
}
    
if(array_key_exists('button4', $_POST)) { 
        $mes = isset($_POST['meses']) ? $_POST['meses'] : null;
        $año = isset($_POST['año2']) ? $_POST['año2'] : null;
        
        ?>
               <script type="text/javascript">
               window.open(<?php mesaño($año,$mes); ?>, '_blank');
               </script>
       <?php    
       }
           
?>

        <div id="abc">
            <!-- Popup Div Starts Here -->
            <div id="popupContact">
                <!-- Contact Us Form -->
                <div  id="form1" name="form1" align="center">
                    <img id="close" src="diseño/images/3.png" onclick="div_hide()">
                    <h2 id="h2">Reportes</h2>
                    <br>
                    <br>
                    <form action="ModuloAdministrador/reportes/facturaciontotal.php" target="_blank">
                        <input type="submit" name="export"id="ftotal" value="Facturacion total PDF" class="btn btn-success" />  
    </form>
                    <br>
                         <input type="button" name="boton2" id="faños"  onclick="div_hide(); div_show3();" value="Facturacion por año PDF" class="btn btn-success" />  
                    <br>
                    <br>
                         <input type="button" name="boton2" id="faños"  onclick="div_hide(); div_show4();" value="Facturacion por mes PDF" class="btn btn-success" />  
                    <br>
                    <br>
                        <input type="button" name="boton" id="fechas"  onclick="div_hide(); div_show2();" value="Facturacion por fechas PDF" class="btn btn-success" />  
                </div>
            </div>
            <!-- Popup Div Ends Here -->
        </div>
      
        <div class="abc2" id="abc2">
            <!-- Popup Div Starts Here -->
            <div class="popupContact2">
                <!-- Contact Us Form -->
                <div class="form2" name="form2" align="center">
                    <img class="close2" src="diseño/images/3.png" onclick="div_hide2()">
                    <h2 class="h22">Seleccione las fechas</h2>
                    <br>
                    <form method="post" target="_blank" >  
                      Fecha Inicial:  <input type="date" id="start" name="start" value=""  required>
                      <br><br>
                      Fecha Final:  <input type="date" id="fin" name="fin" value="" onblur="compare();" required>
                      <br>
                      <br><br>
                      <input type="submit" name="button2" id="fzonas"  value="Facturacion por fechas PDF" class="btn btn-success" />  
                    </form>  
	        </div>
            </div>
            <!-- Popup Div Ends Here -->
        </div>
     
        <div class="abc2" id="abc3">
            <!-- Popup Div Starts Here -->
            <div class="popupContact2">
                <!-- Contact Us Form -->
                <div class="form2" name="form2" align="center">
                    <img class="close2" src="diseño/images/3.png" onclick="div_hide3()">
                    <h2 class="h22">Seleccione el año</h2>
                    <br>
                    <form method="post" target="_blank" >  
                    <select name="año" id="año" class="form-control" required>
                        <option value="" selected hidden>Seleccione el año</option>
                                <?php 
                                while ($row = mysqli_fetch_array($años)) { 
                                        echo '<option value="'.$row["año"].'">'.$row["año"].'</option>';
                                }
                                ?>
                     </select>
                      <br>
                      <br><br>
                      <input type="submit" name="button3" id="faños"  value="Facturacion por año PDF" class="btn btn-success" />  
                    </form>  
	        </div>
            </div>
            <!-- Popup Div Ends Here -->
        </div>

        <div class="abc2" id="abc4">
            <!-- Popup Div Starts Here -->
            <div class="popupContact2">
                <!-- Contact Us Form -->
                <div class="form2" name="form2" align="center">
                    <img class="close2" src="diseño/images/3.png" onclick="div_hide4()">
                    <h2 class="h22">Seleccione el año y el mes</h2>
                    <br>
                    <form method="post" target="_blank" >  
                         <select name="año2" id="año2" class="form-control" required onchange="selectMeses()">
                        <option value="" selected hidden>Seleccione el año</option>
                                <?php 
                                
                                $años= ManejoFactura::añoFactura();
                                while ($row = mysqli_fetch_array($años)) { 
                                        echo '<option value='.$row["año"].'>'.$row["año"].'</option>';
                                }
                                ?>
                     </select>
                      <br>
                      <br>
                      <select class="form-control input-lg" id="meses" name="meses" disabled required>
                        <option value="">Seleccione el Mes</option>
                      </select>
                      <br>
                      <input type="submit" name="button4" id="faños"  value="Facturacion por mes PDF" class="btn btn-success" />  
                    </form>  
	        </div>
            </div>
        </div>

<nav class="navbar navbar-expand-sm navbar-default">
    <div id="main-menu" class="main-menu collapse navbar-collapse">
        <ul class="nav navbar-nav">
        <li class="activeInicio">
                <a href="?menu=inicio"><i class="menu-icon fa fa-laptop"></i>Inicio</a>
        </li>
        <li class="menu-title">Gestión</li>
        <li class="activeGerente">
                <a href="?menu=gerentes"><i class="menu-icon fa fa-users"></i>Gerentes</a>
        </li>
        <li class="activeFuncionario">
                <a href="?menu=funcionarios"><i class="menu-icon fa fa-users"></i>Funcionarios</a>
        </li>
        <li class="activeCliente">
                <a href="?menu=clientes"><i class="menu-icon fa fa-users"></i>Clientes</a>
        </li>
        <li class="activeZona">
                <a href="?menu=zonas"><i class="menu-icon  fa fa-bullseye"></i>Zonas</a>
        </li>
        <li class="activeParque">
                <a href="?menu=parqueaderos"><i class="menu-icon fa ti-car"></i>Parqueaderos</a>
        </li>
        <li class="menu-title">Mapas</li>
        <li class="activeMapas1">
                <a href="?menu=mapas0"><i class="menu-icon fa fa-map-marker"></i>Parqueaderos por Zona</a>
        </li>
        <li class="menu-title">Reportes</li>
        <li class="activeReporte">

        <a href="#" onclick="div_show()"><i  class="menu-icon fa  fa-bar-chart-o"></i>Generar Reportes </a>
        </li>
        <li class="menu-title">Auditoría</li>
        <li class="activeAudit">
                <a href="?menu=auditoria"><i class="menu-icon fa fa-eye"></i>Auditoría</a>
        </li>
        </ul>      
    </div>
</nav>
<style>

@import "http://fonts.googleapis.com/css?family=Raleway";

.abc2 {
    width: 100%;
    height: 100%;
    opacity: 0.95;
    top: 0;
    left: 0;
    display: none;
    position: fixed;
    background-color: #313131;
    overflow: auto;
    z-index: 5;
}

.close2 {
    width: 20px;
    position: absolute;
    right: -14px;
    top: -14px;
    cursor: pointer;
}


.popupContact2 {
    position: absolute;
    left: 50%;
    top: 17%;
    margin-left: -202px;
    font-family: 'Raleway', sans-serif
}

.form2 {
    max-width: 350px;
    min-width: 250px;
    position: absolute;
    left: 50px;
    padding: 30px 35px;
    border: 2px solid rgb(255, 255, 255);
    border-radius: 10px;
    font-family: raleway;
    background-color: #fff
}

.h22 {
    background-color: #FEFFED;
    padding: 20px 50px;
    margin: -10px 0px;
    text-align: center;
    border-radius: 10px 10px 0 0
}
</style>

<script>
  function selectMeses() {
  var año2 = $("#año2").val();
  $.ajax({
    url:"ModuloAdministrador/selectReporte.ajax.php",
    method: "POST",
    data: {
      "anio":año2
      },
      success: function(respuesta){
        $("#meses").attr("disabled", false);
       $("#meses").html(respuesta);
      }
    })
  }
</script>

<script>
function compare()
{
    var startDt = document.getElementById("start").value;
    var endDt = document.getElementById("fin").value;

    if( (new Date(startDt).getTime() > new Date(endDt).getTime()))
    {
        alert("La fecha final: "+ endDt + ", debe ser mayor a la fecha inicial: "+startDt);
    }
}
//Function To Display Popup
function div_show() {
    document.getElementById('abc').style.display = "block";
}
//Function to Hide Popup
function div_hide() {
    document.getElementById('abc').style.display = "none";
}

//Function To Display Popup
function div_show2() {
    document.getElementById('abc2').style.display = "block";
}

//Function to Hide Popup
function div_hide2() {
    document.getElementById('abc2').style.display = "none";
}
//Function To Display Popup
function div_show3() {
    document.getElementById('abc3').style.display = "block";
}

//Function to Hide Popup
function div_hide3() {
    document.getElementById('abc3').style.display = "none";
}
//Function To Display Popup
function div_show4() {
    document.getElementById('abc4').style.display = "block";
}

//Function to Hide Popup
function div_hide4() {
    document.getElementById('abc4').style.display = "none";
}
</script>

