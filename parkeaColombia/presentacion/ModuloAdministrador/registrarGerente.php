<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/GerenteDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ZonaDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoGerente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Auditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Gerente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Zona.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/mailer/mailer.php';

function registrarGerente($nombre, $identificacion, $correo, $zona){
	$conexion=new Conexion();
	$conexion=$conexion->conectarBD();
	$mail=new Mailer();

	ManejoGerente::setConexionBD($conexion);

	$password=ManejoGerente::contraseniaAleatoria();

	$gerente = new Gerente();
	$gerente->setNombre($nombre);
	$gerente->setEmail($correo);
	$gerente->setIdentificacion($identificacion);
	$gerente->setZona($zona);
	$gerente->setContrasena($password);

	ManejoGerente::crearGerente($gerente);

	$auditoria = new Auditoria();
	$auditoria->setCedula_usuario(0);
	$auditoria->setCod_afectado($identificacion);
	$auditoria->setTabla("GERENTE");
	$auditoria->setTipo_operacion("Registrar gerente");
	ManejoAuditoria::setConexionBD($conexion);
	ManejoAuditoria::crearAuditoria($auditoria);
	/**
	* Texto para el mensaje del correo
	*/
	$txt = "¡Hola ".$nombre."!, gracias por registrarte en Par-Kea Colombia.  Tu contraseña es: ".$password." .  Cuando inicies sesión por primera vez tendrás que cambiarla. <br> Cordialmente, <br> Par-Kea Colombia.";

        
	//Enviar correo electrónico
	$mail->enviarCorreo($correo,$txt,"Registro Par-kea Colombia");

	$redd = true;
	if ($redd) {
		header("Location: Administrador.php?menu=gerentes"); 
		exit();
	}
}
	
?>