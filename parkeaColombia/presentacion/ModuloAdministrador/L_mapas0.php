<?php
/**    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
*/
    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCiudad.php';
    $obj=new Conexion();
	$conexion=$obj->conectarBD();
    ManejoCiudad::setConexionBD($conexion);
    $ciudades=ManejoCiudad::listarCiudadesConZona();
 ?>
 <style>
        .navbar .navbar-nav li.activeMapas1 .menu-icon,
        .navbar .navbar-nav li:hover .toggle_nav_button:before,
        .navbar .navbar-nav li .toggle_nav_button.nav-open:before {
            color: #04dd1f;
        }
        
        .navbar .navbar-nav>.activeMapas1>a,
        .navbar .navbar-nav>.activeMapas1>a:focus,
        .navbar .navbar-nav>.activeMapas1>a:hover {
            color: #04dd1f;
        }
 </style>
 <div class="row" >
    <div class="col-lg-6" >
        <div class="card" style="left: 50%; top:10px;">
            <div class="card-header" style="background-color: rgb(189, 255, 222);">
                <strong></strong><h4>Seleccione la ciudad</h4>
            </div>
            <div class="card-body card-block" style="padding: 5em;">
            <form method="post" action="Administrador.php?menu=mapas" >  
                  
            <div class="form-group">
                    <label for="idciudad" class="form-control-label">Ciudad:</label>
                    <select id="idciudad" name="idciudad" class="form-control" required>
                        <option value="" disabled selected hidden>Seleccione la ciudad</option>           
                            <?php 
                                  foreach ($ciudades as $c) {
                                      echo'<option value="'.$c->getCodCiudad().','.$c->getNomCiudad().'">'.$c->getNomCiudad().'</option>';
                                      
                                  }
                            ?>
                    </select>
                         </div>
                        <input type="submit" name="export" id="input1" value="Continuar"  class="btn btn-success" />  
                   </form>  
            </div>
        </div>
    </div>
    </div>
