<?php
/**    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
*/
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';

	$obj=new Conexion();
	$conexion=$obj->conectarBD();

	ManejoAuditoria::setConexionBD($conexion);
    $auditorias=ManejoAuditoria::listarAuditorias();
	$salida = "";
	if(isset($_POST['fechas'])){
		$auditorias=ManejoAuditoria::consultarAuditoriaPorFechas($_POST["fecha1"],$_POST["fecha2"]);
	}
?>

<style>
.navbar .navbar-nav li.activeAudit .menu-icon, .navbar .navbar-nav li:hover .toggle_nav_button:before, .navbar .navbar-nav li .toggle_nav_button.nav-open:before {
    color: #04dd1f;
}
.navbar .navbar-nav>.activeAudit>a, .navbar .navbar-nav>.activeAudit>a:focus, .navbar .navbar-nav>.activeAudit>a:hover {
    color: #04dd1f;
}
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(105, 243, 128, 0.54);
}.table-striped tbody tr:nth-of-type(even) {
    background-color: #e3ffe1!important;
}
button.dt-button, a.dt-button {
    background-image: linear-gradient(to bottom, #5efa6a 0%, #e9e9e9 100%) !important;
	border-radius: 63px !important;
}
.dataTables_length{
    color: #333;
	margin-right:3em !important;
}
div.dt-button-collection {
    top: 0 !important;
	left: 150px !important;
	background-color: rgba(255, 255, 255, 0.8) !important;
}
</style>

<link rel="stylesheet" type="text/css" href="./diseño/admin/css/datatables.min.css"/>
<script src="./diseño/admin/js/datatables.min.js"></script>
<script src="./diseño/admin/js/pdfmake.min.js"></script>
<script src="./diseño/admin/js/vfs_fonts.js"></script>
<div class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<strong class="card-title">Lista de Auditorias</strong>			
				</div>
				<!--
				<div>
					<label for="caja_busqueda" style="margin-left: 5%; margin-top: 1%;">Buscar</label>
					<input type="text" name="caja_busqueda" id="caja_busqueda"></input>
				</div>-->
				<div id="datos" style="margin:3%;">
				<form method="POST" name="formFechas" id="formFechas" action="Administrador.php?menu=auditoria">
						<label>Buscar por fechas: </label>	
						<br>
						<label>Fecha inicial: </label>	
						<input type="date" id="fecha1" name="fecha1" required>
						<label>Fecha final: </label>	
						<input type="date" id="fecha2" name="fecha2" required>
						<button type="submit" id="fechas" name="fechas">Buscar</button>
					</form>
				<table id="example" class="table table-striped table-bordered" style="width:100%; margin:1%;">
				<?php
				if (count($auditorias) > 0) {
    	$salida.="
        <thead>
            <tr>
                <th><center>No.</center></th>	
                <th><center>CEDULA USUARIO</center></th>
                <th><center>TIPO OPERACIÓN</center></th>
                <th><center>TABLA AFECTADA</center></th>
                <th><center>CEDULA AFECTADO</center></th>
                <th><center>FECHA</center></th>
            </tr>
        </thead>
        <tbody>";

    	$no = 1;
		foreach($auditorias as $f){
            $salida.= '
                <tr>
				<td>'.$no.'</td>';
				if($f->getCedula_usuario() == 0){
					$salida .='<td>Administrador</td>';

                }else{
					$salida .='<td>'.$f->getCedula_usuario().'</td>';
				}
				$salida .= '
				<td>'.$f->getTipo_operacion().'</td>';
				if($f->getTabla()=='-'){
					$salida .= '<td>N/A</td>';
				}else{
					$salida .= '<td>'.$f->getTabla().'</td>';
				}
				if($f->getCod_afectado()==0){
					$salida .= '<td>N/A</td>';
					}else{
						$salida .= '<td>'.$f->getCod_afectado().'</td>';
					}
					$salida .= '<td>'.$f->getFecha().'</td>
                </tr>';
				$no++;
		}
		$salida.="</tbody><tfoot>
		<tr>
			<th>No</th>
			<th>Cedula</th>
			<th>Tipo</th>
			<th>Tabla</th>
			<th>Afectado</th>
			<th>Fecha</th>
		</tr>
	</tfoot>";
    	$salida.="</table>";
    }else{
    	$salida.="<center><h2>NO HAY DATOS </h2></center>";
    }
	echo $salida;
	?>
        
				</div>				
			</div>			
		</div>		
	</div>
</div>
<?php
$path = 'diseño/images/logo.jpg';
$type = pathinfo($path, PATHINFO_EXTENSION);
$data = file_get_contents($path);
$base64 = "'data:image/" . $type . ";base64," . base64_encode($data)."'";
?>
<script>
	$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#example').DataTable({
        dom: 'lBfrtip',
			buttons: [{
				extend: 'collection',
				text: 'Exportar tabla',
				buttons: [
					{
					extend: 'copyHtml5',
					text: 'Copiar',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4,5 ]
					}},
						{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4,5 ]
					}},
					{
					extend: 'csvHtml5',
					text: 'CSV',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4,5 ]
					}} ,
						{
					extend: 'pdfHtml5',
					text: 'PDF',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4,5 ]
					},
					 customize: function(doc) {
						doc.styles.tableHeader.fillColor = 'green';
						
						doc.content.splice( 1, 0, {
							margin: [ 0, 0, 0, 6 ],
							alignment: 'center',
							image: <?php echo $base64;?>,
							width:50
						} );
						doc.content.splice(0, 1, {
								text: [
										   { text: 'Modulo Administrador \n',bold:true,fontSize:15 },
										   { text: 'Lista de Auditorias \n',italics:true,fontSize:12 },
										   { text: 'Par-Kea Colombia',italics:true,fontSize:9 }
	
								],
								margin: [0, 0, 0, 12],
								alignment: 'center'
                        });
                        doc['footer'] = (function(page, pages) {
                            return {
                            columns: [
                                {
                                alignment: 'center',
                                text: [
                                    { text: page.toString(), italics: true },
                                    ' of ',
                                    { text: pages.toString(), italics: true }
                                ]
                            }],
                            margin: [10, 0]
                            }
                        });
					 },
				title:'Auditorias'
				},
				'print'
			   
				]}],initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
			
        },
		"language": idioma_espanol
    }); 
} );
var idioma_espanol={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }
};
</script>



