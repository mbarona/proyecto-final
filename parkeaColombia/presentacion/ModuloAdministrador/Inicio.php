<?php

	require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
  	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';



  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';

	$meses = ["enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre"];
  $obj=new Conexion();
  $conexion=$obj->conectarBD();

  ManejoCliente::setConexionBD($conexion);
  ManejoParqueadero::setConexionBD($conexion);
  ManejoZona::setConexionBD($conexion);
  ManejoReserva::setConexionBD($conexion);

	$anolineChartGeneral = isset($_GET['ano']) ? $_GET['ano'] : 0;
	$meslineChartGeneral = isset($_GET['mes']) ? $_GET['mes'] : 0;
	$contarReservarPorDia= ManejoReserva::contarReservasPorDia($meslineChartGeneral,$anolineChartGeneral);

  $anosReserva = ManejoReserva::ListarAnos();
  $dias = array();
  $cantidad = array();
  $totalParqueaderos= ManejoParqueadero::totalParqueaderos();
  $totacliente= ManejoCliente::totalClientes();
  $zonas= ManejoZona::ListarZonasConParqueadero();
  $totalz=count($zonas);
  $label= array();
  $numeros= array();

  for ($i=0; $i <=$totalz-1; $i++) {
     $parzona = ManejoParqueadero::parqueaderosEnEstaZona($zonas[$i]->getCodZona());
     $numeros[$i]=count($parzona);
  }

  $label = $zonas;
?>


    <script type="text/javascript" src="diseño/Admin/js/jquery.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.14.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-match-height@0.7.2/dist/jquery.matchHeight.min.js"></script>
    <script src="./diseño/Admin/js/main.js"></script>
    <!--  Chart js -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.bundle.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery.flot@0.8.3/jquery.flot.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flot-spline@0.0.1/js/jquery.flot.spline.min.js"></script>


    <style>
        .navbar .navbar-nav li.activeInicio .menu-icon,
        .navbar .navbar-nav li:hover .toggle_nav_button:before,
        .navbar .navbar-nav li .toggle_nav_button.nav-open:before {
            color: #04dd1f;
        }

        .navbar .navbar-nav>.activeInicio>a,
        .navbar .navbar-nav>.activeInicio>a:focus,
        .navbar .navbar-nav>.activeInicio>a:hover {
            color: #04dd1f;
        }

    </style>
    <div class="row">
        <div class="col-sm-12">
            <div class="page-title">
                <center>
                    <h3>Resumen General</h3>
                </center>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-sm-6 col-lg3">
            <div class="card text-white bg-flat-color-4">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                        </h3>
                        <p class="text-light mt-1 m-0">Total Parqueaderos</p>
                    </div>
                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-home"> <span class="count"><?php echo $totalParqueaderos?></span></i>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-sm-6 col-lg3">
            <div class="card text-white bg-flat-color-1">
                <div class="card-body">
                    <div class="card-left pt-1 float-left">
                        <h3 class="mb-0 fw-r">
                        </h3>
                        <p class="text-light mt-1 m-0">Total Clientes</p>
                    </div>
                    <div class="card-right float-right text-right">
                        <i class="icon fade-5 icon-lg pe-7s-users"> <span class="count"><?php echo $totacliente?></span></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-lg-12">
        <div class="card">
        <div class="card-body">
            	<form action="<?php echo $_SERVER['PHP_SELF'];?>" method="GET">
            		<div class="row form-group">
         			<select id="ano" name="ano" class="form-control" onchange="selectMes()" required>
            			<option value="">Seleccione el Año</option>           
                  		<?php 
                      	foreach ($anosReserva as $d) {
                         	echo'<option value="'.$d.'">'.$d.'</option>';
                       	} 
                  	?>
        			</select>
            		</div>
            		<div class="row form-group">
            			<select class="form-control input-lg" id="mes" name="mes" disabled required="Mes Requerido">
            				<option value="">Seleccione el Mes</option>
          				</select>
            		</div>
            		<div class="row form-group">
                		<button type="submit" class="btn btn-warning btn-lg btn-block"  name="btnGenerar" id="btnGenerar" >Generar Gráfica</button>
            		</div>
            	</form>
            </div>
            <div class="card-body">
                <h4 class="mb-3">Reservas por Día en General</h4>
                <canvas id="lineChartGeneral"></canvas>
            </div>
            
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">

        <div class="card">
            <div class="card-body">
                <h4 class="mb-3">Parqueaderos por Zona</h4>
                <canvas id="pieChart"></canvas>
            </div>
        </div>
    </div>
</div>

<script>
  function selectMes() {
  var ano = $("#ano").val();
  $.ajax({
    url:"ModuloAdministrador/selectMes.ajax.php",
    method: "POST",
    data: {
      "ano":ano
      },
      success: function(respuesta){
        $("#mes").attr("disabled", false);
       $("#mes").html(respuesta);
      }
    })
  }
</script>

<script>
  function cargarGrafica() {
  var mes = $("#mes").val();
  var ano = $("#ano").val();
  $.ajax({
    url:"<?php echo $_SERVER['PHP_SELF']; ?>",
    method:"POST",
    data: {
    	"mes":mes,
    	"ano":ano
    	}
    })
  }
</script>

<script type="text/javascript">
    ( function ($) {

    //Pie Chart
    var ctx = document.getElementById("pieChart");
    ctx.height =300;
    ctx.width=300;
    var myChart = new Chart( ctx, {
        type: 'pie',
        data: {
            datasets: [ {
                data: [
                <?php echo '"'.implode('","', $numeros).'"' ?>
                ],
            backgroundColor: [
                            "rgba(142, 194, 95, 0.47)",
                            "rgba(117, 195, 171, 0.47)",
                            "rgba(67, 153, 89, 0.47)",
                            "rgba(118, 145, 99, 0.47)",
                            "rgba(118, 202, 172, 0.47)",
                            "rgba(0,0,0,0.07)"
                                ],
            hoverBackgroundColor: [
                                "rgba(142, 194, 95, 0.7)",
                                "rgba(117, 195, 171, 0.7)",
                                "rgba(67, 153, 89, 0.7)",
                                "rgba(118, 145, 99, 0.7)",
                                "rgba(118, 202, 172, 0.7)",
                                "rgba(0,0,0,0.07)"
                                ]

                            } ],
            labels:[
        <?php 
            for ($i=0; $i <=$totalz-1; $i++) {
                echo '"'.$label[$i]->getNomZona().'",';
            }
        ?>]
         
        },
        options: {
            responsive: true
        }
    } );

} )( jQuery );
</script>

<script type="text/javascript">
( function ($) {
 //line chart
    var ctx = document.getElementById( "lineChartGeneral" );
    ctx.height = 150;
    var myChart = new Chart( ctx, {
        type: 'line',
        data: {
            labels: [<?php
	for($i=0; $i <= count($contarReservarPorDia)-1;$i++){
		$contarReservarPorDia= ManejoReserva::contarReservasPorDia($meslineChartGeneral,$anolineChartGeneral);
		if (($i % 2)==0) {
			$dias[$i]=$contarReservarPorDia[$i];
			echo '"'.$meses[($meslineChartGeneral)-1].' '.$dias[$i].'",';
		}
		
	}	
	           		?>],
            datasets: [
                {
                    label: "Reservas",
                    borderColor: "rgba(118, 202, 172, 0.7)",
                    borderWidth: "1",
                    backgroundColor: "rgba(118, 202, 172, 0.5)",
                    data: [ <?php 
	for($i=0; $i <= count($contarReservarPorDia)-1;$i++){
		$contarReservarPorDia= ManejoReserva::contarReservasPorDia($meslineChartGeneral,$anolineChartGeneral);
		if(($i % 2)==1){
			$cantidad[$i]=$contarReservarPorDia[$i];
			echo '"'.$cantidad[$i].'",';
		}
		
	}
                    		?>]
                }
                        ]
        	},
        options: {
            responsive: true,
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }

        }
    } );
} )( jQuery );
</script>
    
    	<div class="card-body">
    		<div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px;right: 0px;bottom: 0px; overflow: hidden;pointer-events: none;visibility: hidden; z-index: -1;">
    			<div class="chartjs-size-monitor-expand" style="position: absolute; left: 0; top: 0; right: 0;bottom: 0;overflow: hidden;pointer-events: none; visibility: hidden;z-index: -1;">
    				<div style="position: absolute;width: 1000000px;height: 1000000px;left: 0;top: 0">
    				</div>
    			</div>
    			<div class="chartjs-size-monitor-shrink" style="position: absolute;left: 0;top: 0;right: 0;bottom: 0;overflow: hidden;pointer-events: none;visibility: hidden;z-index: -1;">
    				<div style="position: absolute;width: 200%; height: 200%; left: 0;top: 0 "></div>
    			</div>
    			<h4 class="mb-3">Grafico 1</h4>
    			<canvas id="pieChart" height="300" width="300" class="chartjs-render-monitor" style="display: block; width: 300px;height: 300px">
    		</div>
    	</div>
    </div>

