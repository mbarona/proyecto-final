<?php
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoFactura.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

	$mesesN = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

	$año=$_POST["anio"];
	$obj=new Conexion();
	$conexion=$obj->conectarBD();
	ManejoFactura::setConexionBD($conexion);

	$meses = ManejoFactura::mesFactura($año);
	
	echo '<option value="">Seleccione el Mes</option>';
	while ($row = mysqli_fetch_array($meses)) { 
		echo '<option value='.$row['mes'].'>'.$mesesN[($row["mes"])-1].'</option>';
    }
?>