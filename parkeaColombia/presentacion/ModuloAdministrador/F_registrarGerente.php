<?php

	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
    require_once 'validaciones.php';
    require_once 'registrarGerente.php';

    $obj=new Conexion();
    $conexion=$obj->conectarBD();

    ManejoZona::setConexionBD($conexion);
    $zonasSinGerente= ManejoZona::zonasSinGerente();
    $numzonas = mysqli_num_rows($zonasSinGerente);

    $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : null;
    $identificacion = isset($_POST['identificacion']) ? $_POST['identificacion'] : null;
    $email = isset($_POST['email']) ? $_POST['email'] : null;
    $zona = isset($_POST['zona']) ? $_POST['zona'] : null;
    $errores = array();

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //Valida que el campo nombre no esté vacío.
    if (!validaRequerido($nombre)) {
        $errores[] = 'El Nombre es Obligatorio.';
    }
    if (!ctype_alpha(str_replace(array("\n", "\t", ' '), '', $nombre))) {
      $errores[] = 'El nombre solo debe contener letras y espacios.';
  }
    if (!validaRequerido($identificacion)) {
        $errores[] = 'El Número de Identificación es Obligatorio.';
    }
    if(!validarEntero($identificacion)){
        $errores[] = 'Solo deben Ingresarse Números.';
    }
    if (!validaRequerido($email)) {
        $errores[] = 'El Correo es Obligatorio.';
    }
    if(!is_valid_email($email)){
    	$errores[] = 'El Correo '.$email.' no es valido.';
    }
    if(!validarLista($zona)){
    	$errores[] = 'Seleccione una Zona.';
    }

    if(!$errores){
        registrarGerente($nombre, $identificacion, $email, $zona);
    }

}

?>
<?php if ($errores): ?>
      <?php foreach ($errores as $error): ?>
        <a href='#' style="cursor: initial;">
        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade-show">
          <span class="badge badge-pill badge-danger">Error</span>
          <?php echo $error ?>
          <img src="diseño/images/3.png" style="cursor: pointer;" width="10" title='Cerrar' alt="Close" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" />
        </div>
        </a>
      <?php endforeach; ?>
  <?php endif; ?>
  <?php if (!$errores): ?>
      <?php foreach ($errores as $error): ?>
          <div class="sufee-alert alert with-close alert-success alert-dismissible fade-show">
          <span class="badge badge-pill badge-success">Correcto</span>
          <?php echo $error ?>
          </div>
      <?php endforeach; ?>
  <?php endif; ?>
  <div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Registrar Gerente</strong><small> Gestión Empleados</small>
      </div>

      <div class="card-body card-block">
        <form class="form" role="form" autocomplete="off" id="formRegistroGerente" method="POST">
            <div class="form-group">
              <label for="nombre" class="form-control-label">Nombre Completo</label>
              <input type="text" id="nombre" name="nombre" placeholder="Ingrese Nombre Completo" maxlength="80" required class="form-control">
            </div>
            <div class="form-group">
              <label for="identificacion" class="form-control-label">Número Identificación</label>
              <input type="number" id="identificacion" name="identificacion" placeholder="Ingrese Número Identificación" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength = "15" min=1 max=999999999999999  required class="form-control" onkeypress="solonumeros(event);">
            </div>
            <div class="form-group">
              <label for="email" class="form-control-label">E-mail</label>
              <input type="text" id="email" name="email" placeholder="Ingrese E-mail" maxlength="80" required class="form-control" >
            </div>
            <div class="form-group">
              <label for="zona" class="form-control-label">Zona</label>
              <select name="zona" id="zona" class="form-control" required>
                  <?php
                  if( $numzonas == 0){
                    echo '<option  style="display:none" value="" >No hay zonas disponibles</option>';
                  }else{
                      echo '<option style="display:none" value="">Seleccione la Zona</option>';
                      while ($mostrar=mysqli_fetch_row($zonasSinGerente)){
                             echo '<option value="'.$mostrar[0].'">'.$mostrar[1].' -- '.$mostrar[2].'</option>';
                      }
                  }
                  ?>
              </select>
            </div>
            <div class="form-group">
            <button type="submit" class="btn btn-warning btn-lg btn-block">Registrar Gerente</button>
            </div>
        </form>

        <form name="menu" method="get">
          <div>
            <input id="menu" name="menu" type="hidden" value="gerentes">
            <button type="submit" class="btn btn-danger btn-lg btn-block">Regresar</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	function solonumeros(e)
                    {
         var key = window.event ? e.which : e.keyCode;
                        if(key < 48 || key > 57)
                            e.preventDefault();
                    }
</script>
