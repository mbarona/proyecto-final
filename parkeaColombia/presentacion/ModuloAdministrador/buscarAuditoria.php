<?php
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';

	$obj=new Conexion();
	$conexion=$obj->conectarBD();

	ManejoAuditoria::setConexionBD($conexion);
    $auditorias=ManejoAuditoria::listarAuditorias();
    $salida = "";

    if (isset($_POST['consulta'])) {
        $auditorias=ManejoAuditoria::listarAuditoriasPorParametro($_POST['consulta']);
    }
    
    if (count($auditorias) > 0) {
    	$salida.="<table id='bootstrap-data-table' class='table table-striped table-bordered'>
        <thead>
            <tr>
                <th><center>No.</center></th>	
                <th><center>CEDULA USUARIO</center></th>
                <th><center>TIPO OPERACIÓN</center></th>
                <th><center>TABLA AFECTADA</center></th>
                <th><center>CEDULA AFECTADO</center></th>
                <th><center>FECHA</center></th>
            </tr>
        </thead>
        <tbody>";

    	$no = 1;
		foreach($auditorias as $f){
            $salida.= '
                <tr class=tr-shadow">
                <td>'.$no.'</td>
                <td>'.$f->getCedula_usuario().'</td>
                <td>'.$f->getTipo_operacion().'</td>
                <td>'.$f->getTabla().'</td>
                <td>'.$f->getCod_afectado().'</td>										
                <td>'.$f->getFecha().'</td>
                </tr>
                <tr class="spacer"></tr>';
				$no++;
		}
    	$salida.="</tbody></table>";
    }else{
    	$salida.="<center><h2>NO HAY DATOS </h2></center>";
    }
    echo $salida;
?>