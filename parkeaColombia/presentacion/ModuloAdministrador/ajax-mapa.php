<?php 
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCiudad.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    $direccion=$_POST["direccion"];
    $arr= explode(",",$direccion);
    $obj=new Conexion();
	$conexion=$obj->conectarBD();
    ManejoCiudad::setConexionBD($conexion);
    ManejoZona::setConexionBD($conexion);

    if(strpos($arr[2], 'Bogotá') !== false){
        $zonas=ManejoZona::zonaCiudadDada(1);
        echo'<option value="">Seleccione la zona</option>';
        foreach ($zonas as $z) {
            echo'<option value="'.$z->getCodZona().'">'.$z->getNomZona().'</option>';
        }
    }else{
        $sinEspacios = str_replace(' ', '', $arr[1]);
        $ciudad= ManejoCiudad::consultarCiudadPorNombre($sinEspacios);
        $zonas=ManejoZona::zonaCiudadDada($ciudad->getCodCiudad());
        echo'<option value="">Seleccione la zona</option>';
        foreach ($zonas as $z) {
            echo'<option value="'.$z->getCodZona().'">'.$z->getNomZona().'</option>';
        }
    }
?>