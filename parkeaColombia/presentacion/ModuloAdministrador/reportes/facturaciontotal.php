<?php 
include('tcpdf/tcpdf.php');

// extend TCPF with custom functions
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoFactura.php';
$obj=new Conexion();
$conexion=$obj->conectarBD();

ManejoFactura::setConexionBD($conexion);

$mes="";
$facturas= ManejoFactura::facturasZona();
$zonaUlti="";
$ciuUlti="";
$valorUlti="";
$factUlti="";
$añoUlti="";
$no_of_rows = mysqli_num_rows($facturas);

while ($row = mysqli_fetch_array($facturas)) {
    $zonaUlti=$row["nom_zona"];
    $ciuUlti=$row["nom_ciudad"];
    $valorUlti=$row["total"];
    $factUlti=$row["facturas"];
    $añoUlti=$row["año"];
    if ($row["mes"]==1) {
        $mes= "Enero";
    } elseif ($row["mes"]==2) {
        $mes= "Febrero";
    } elseif ($row["mes"]==3) {
        $mes= "Marzo";
    } elseif ($row["mes"]==4) {
        $mes= "Abril";
    } elseif ($row["mes"]==5) {
        $mes= "Mayo";
    } elseif ($row["mes"]==6) {
        $mes= "Junio";
    } elseif ($row["mes"]==7) {
        $mes= "Julio";
    } elseif ($row["mes"]==8) {
        $mes= "Agosto";
    } elseif ($row["mes"]==9) {
        $mes= "Septiembre";
    } elseif ($row["mes"]==10) {
        $mes= "Octubre";
    } elseif ($row["mes"]==11) {
        $mes= "Noviembre";
    } elseif ($row["mes"]==12) {
        $mes= "Diciembre";
    }  
}

class MYPDF extends TCPDF {

    // Load table data from file
    public function LoadData() {
        // Read file lines
      
        $facturas= ManejoFactura::facturasZona();
        return $facturas;
    }
    
    // Colored table
    public function ColoredTable($header,$data) {
        // Colors, line width and bold font
        $this->SetFillColor(6, 196, 0);
        $this->SetTextColor(255);
        $this->SetDrawColor(0, 0, 0);
        $this->SetLineWidth(0.3);
        $this->SetFont('', 'B');
        // Header
        $w = array(26,26, 36, 20,23,23,20);
        $num_headers = count($header);
        for($i = 0; $i < $num_headers; ++$i) {
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
        }
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = 0;

        $mes="";
        while ($row = mysqli_fetch_array($data)) {
	     
            if($row["mes"]==1){
                $mes= "Enero";
            }elseif($row["mes"]==2){
                $mes= "Febrero";
            }elseif($row["mes"]==3){
                $mes= "Marzo";
            }elseif($row["mes"]==4){
                $mes= "Abril";
            }elseif($row["mes"]==5){
                $mes= "Mayo";
            }elseif($row["mes"]==6){
                $mes= "Junio";
            }elseif($row["mes"]==7){
                $mes= "Julio";
            }elseif($row["mes"]==8){
                $mes= "Agosto";
            }elseif($row["mes"]==9){
                $mes= "Septiembre";
            }elseif($row["mes"]==10){
                $mes= "Octubre";
            }elseif($row["mes"]==11){
                $mes= "Noviembre";
            }elseif($row["mes"]==12){
                $mes= "Diciembre";
            }
            
            $this->Cell($w[0], 6, $row["nom_ciudad"], 'LR', 0, 'C', $fill);
            $this->Cell($w[1], 6, $row["nom_zona"], 'LR', 0, 'C', $fill);
            $this->Cell($w[2], 6, $row["nom_gerente"], 'LR', 0, 'C', $fill);
            $this->Cell($w[3], 6, $row["facturas"], 'LR', 0, 'C', $fill);
            $this->Cell($w[4], 6, "$".$row["total"], 'LR', 0, 'C', $fill);
            $this->Cell($w[5], 6, $mes, 'LR', 0, 'C', $fill);
            $this->Cell($w[6], 6, $row["año"], 'LR', 0, 'C', $fill);
            $this->Ln();
            $fill=!$fill;
        }
        $this->Cell(array_sum($w), 0, '', 'T');
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Par-Kea Colombia');
$pdf->SetTitle('Facturación Total ');
$pdf->SetSubject('Ger');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
$PDF_HEADER_LOGO="diseño/images/logo.jpg";
$PDF_HEADER_TITLE="Reporte de Facturación";
$PDF_HEADER_STRING="by Par-Kea Colombia";
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $PDF_HEADER_TITLE, $PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', '', 10);

// add a page
$pdf->AddPage();

$msg="";
if($no_of_rows>0){
    // create some HTML content
   
    $facturas= ManejoFactura::facturasZona();
    $row = mysqli_fetch_array($facturas);
    
    $mes2="";
    if ($row["mes"]==1) {
        $mes2= "Enero";
    } elseif ($row["mes"]==2) {
        $mes2= "Febrero";
    } elseif ($row["mes"]==3) {
        $mes2= "Marzo";
    } elseif ($row["mes"]==4) {
        $mes2= "Abril";
    } elseif ($row["mes"]==5) {
        $mes2= "Mayo";
    } elseif ($row["mes"]==6) {
        $mes2= "Junio";
    } elseif ($row["mes"]==7) {
        $mes2= "Julio";
    } elseif ($row["mes"]==8) {
        $mes2= "Agosto";
    } elseif ($row["mes"]==9) {
        $mes2= "Septiembre";
    } elseif ($row["mes"]==10) {
        $mes2= "Octubre";
    } elseif ($row["mes"]==11) {
        $mes2= "Noviembre";
    } elseif ($row["mes"]==12) {
        $mes2= "Diciembre";
    }
    
    if($no_of_rows>1){
        $msg= '<h1>Facturación Total</h1>La zona con mayor número de facturas es '.$row["nom_zona"].
        ' en la ciudad de '.$row["nom_ciudad"].' con un total de '.$row["facturas"].' facturas las cuales tienen un valor total de $'.number_format($row["total"]).' durante el año '.$row["año"].' en el mes de '.$mes2.'.';
        $msg=$msg.'<br><br>La zona con menor número de facturas es '.$zonaUlti.' de la ciudad de '.$ciuUlti.
        ' con un total de '.$factUlti.' facturas las cuales tienen un valor total de $'.number_format($valorUlti).' durante el año '.$añoUlti.' en el mes de '.$mes.'.';
        
        $facturasdif=number_format($row["facturas"])-number_format($factUlti);
        $valordif=$row["total"]-$valorUlti;

        $msg=$msg.'<br><br>La diferencia entre estas zonas es de '.$facturasdif.' facturas y un valor de $'.number_format((abs($valordif))).'.<br>';
    }elseif($no_of_rows=1){
 
        $msg= '<h1>Facturación Total</h1>La zona con mayor número de facturas es '.$row["nom_zona"].
        ' en la ciudad de '.$row["nom_ciudad"].' con un total de '.$row["facturas"].' facturas la cuales tienen un valor total de $'
        .number_format($row["total"]).' durante el año '.$row["año"].' en el mes de '.$mes2.'.<br>';
    
    }
    $html =$msg;
    // output the HTML content
    $pdf->writeHTML($html, true, 0, true, 0);
    // column titles
    $header = array('Ciudad','Zona', 'Gerente', 'Facturas','Valor Total','Mes','Año');
    
    // data loading
    $data = $pdf->LoadData();
    //print_r($data);exit;
    
    // print colored table
    $pdf->ColoredTable($header, $data);
}else{
    
    $html = '<h1>No hay facturas registradas</h1><br>';
    
    // output the HTML content
    $pdf->writeHTML($html, true, 0, true, 0);
}


// ---------------------------------------------------------
ob_end_clean();
// close and output PDF document
$pdf->Output('FacturacionTotal__'.date("Y-m-d").'.pdf', 'I');
?>