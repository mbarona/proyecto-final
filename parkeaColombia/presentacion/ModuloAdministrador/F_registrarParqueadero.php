<?php
/**    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
*/
    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoT_Parqueadero.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';

   require_once 'validaciones.php';
   require_once 'registrarParqueadero.php';

   $obj=new Conexion();
   $conexion=$obj->conectarBD();
   
   $errores = array();
    ManejoZona::setConexionBD($conexion);
    $result = ManejoZona::zonaCiudad();
    
    ManejoT_Parqueadero::setConexionBD($conexion);
    $tipos = ManejoT_Parqueadero::listarT_Parqueaderos();
    
    $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : null;
    $zona = isset($_POST['zona']) ? $_POST['zona'] : null;
    $tipo = isset($_POST['tipo']) ? $_POST['tipo'] : null;
    $dirr = isset($_POST['direccion']) ? $_POST['direccion'] : null;
    $lat = isset($_POST['latitud']) ? $_POST['latitud'] : null;
    $lng = isset($_POST['longitud']) ? $_POST['longitud'] : null;
    $tarifa = isset($_POST['tarifa']) ? $_POST['tarifa'] : null;
    $capacidad = isset($_POST['capacidad']) ? $_POST['capacidad'] : null;
    $entreInicio = isset($_POST['entreInicio']) ? $_POST['entreInicio'] : null;
    $entreFinal = isset($_POST['entreFinal']) ? $_POST['entreFinal'] : null;
    $finesInicio = isset($_POST['finesInicio']) ? $_POST['finesInicio'] : null;
    $finesFinal = isset($_POST['finesFinal']) ? $_POST['finesFinal'] : null;

   if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //Valida que el campo nombre no esté vacío.
    if (!validaRequerido($nombre)) {
        $errores[] = 'El Nombre es Obligatorio.';
    }
    if (!validarLista($zona)) {
        $errores[] = 'Seleccione la Zona';
    }
   	if (!validarLista($tipo)) {
        $errores[] = 'Seleccione el Tipo de Parqueadero';
    }
    if (!validarEntero($tarifa)) {
        $errores[] = 'La Tarifa ingresada no es valida.';
    }
    if (!validarCapacidad($capacidad)) {
        $errores[] = 'La Capacidad del parqueadero no es valida.';
    }
    if(!$errores){
        registrarParqueadero($nombre, $zona, $tipo, $dirr, $lat, $lng, $tarifa, $capacidad, $entreInicio, $entreFinal, $finesInicio, $finesFinal);

    }

}
?>
    <style>
        #map {
            height: 100%;
            position: absolute;
            top: 0;
            left: 0%;
            bottom: 0;
            width: 100%;
        }
        
        #floating-panel {
            position: absolute;
            top: 10px;
            left: 25%;
            z-index: 3;
            background-color: #fff;
            padding: 5px;
            border: 1px solid #999;
            text-align: center;
            font-family: 'Roboto', 'sans-serif';
            line-height: 30px;
            padding-left: 10px;
        }
        
        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        .hora{
            width:300px;

        }
    </style>
    <?php if ($errores): ?>
        <?php foreach ($errores as $error): ?>
        <a href='#' style="cursor: initial;">
        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade-show">
          <span class="badge badge-pill badge-danger">Error</span>
          <?php echo $error ?>
          <img src="diseño/images/3.png" style="cursor: pointer;" width="10" title='Cerrar' alt="Close" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" />
        </div>
        </a>
      <?php endforeach; ?>
    <?php endif; ?>

    <?php if (!$errores): ?>
    <?php foreach ($errores as $error): ?>
    <div class="sufee-alert alert with-close alert-success alert-dismissible fade-show">
        <span class="badge badge-pill badge-success">Correcto</span>
        <?php echo $error ?>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <strong>Registrar Parqueadero</strong><small>  Gestión Parqueaderos</small>
                </div>

                <div class="card-body card-block">
                    <form class="form" role="form" autocomplete="off" id="formRegistroParqueadero" method="POST">
                        <div class="form-group">
                            <label for="nombre" class="form-control-label">Nombre del Parqueadero</label>
                            <input type="text" id="nombre" name="nombre" placeholder="Ingrese Nombre Completo" maxlength="80" required class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="tipo" class="form-control-label">Tipo de Parqueadero</label>
                            <select name="tipo" id="tipo" class="form-control" required>
                              <option value="" selected hidden>Seleccione el tipo de parqueadero</option>
                                  <?php 
                                    foreach($tipos as $t){
                                      echo '<option value="'.$t->getCodTParqueadero().'">'.$t->getNomTParqueadero().'</option>';
                                    }
                                    ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tarifa" class="form-control-label">Tarifa</label>
                            <input type="number" id="tarifa" name="tarifa" placeholder="Ingrese la Tarifa" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength = "7" required min="1" max="1000000" class="form-control" onkeypress="solonumeros(event);">
                        </div>
                      <center>
                        <div class="form-group">
                        <label class="form-control-label"><strong>Horario Entre Semana</strong></label>
                        
                        <div style="display: flex; ">
                            <div class="form-group" style="margin-right: -125px;">
                                <label for="entreInicio" class="form-control-label">Hora de apertura</label>
                                <input type="time" id="entreInicio" style=" margin-right: 150px; margin-left: 150px;" name="entreInicio" required class="form-control hora"  min="00:00:00" max="11:59:00">
                            </div>
                            <div class="form-group" >
                                <label for="entreFinal" class="form-control-label">Hora de cierre</label>
                                <input type="time" id="entreFinal" style=" margin-right: 150px; margin-left: 150px;" name="entreFinal"  required  class="form-control hora" min="12:00:00" max="23:59:00"> 
                            </div>
                        </div>
                        <label class="form-control-label"><strong>Horario Fines de Semana</strong></label>
                          
                        <div style="display: flex; ">
                            
                            <div class="form-group" style="margin-right: -125px;">
                                <label for="finesInicio" class="form-control-label">Hora de apertura</label>
                                <input type="time" id="finesInicio" style=" margin-right: 150px; margin-left: 150px;" name="finesInicio" required class="form-control hora"  min="00:00:00" max="11:59:00">
                            </div>
                            <div class="form-group" >
                                <label for="finesFinal" class="form-control-label">Hora de cierre</label>
                                <input type="time" id="finesFinal" style=" margin-right: 150px; margin-left: 150px;" name="finesFinal" required  class="form-control hora" min="12:00:00" max="23:59:00">
                            </div>
                        </div>
                        </div>
                      
                                </center>
                        <div class="form-group">
                            <label for="capacidad" class="form-control-label">Capacidad</label>
                            <input type="number" id="capacidad" name="capacidad" placeholder="Ingrese la Capacidad (min 1 - max 1000)" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"  maxlength = "4" min="1" max="1000" required class="form-control"  onkeypress="solonumeros(event);">
                        </div>
                        <div class="form-group">
                            <label for="direccion" class="form-control-label">Direccion</label>
                            <input type="text" id="direccion" name="direccion" placeholder="" class="form-control" required readonly>
                        </div>
                        <div class="form-group">
                            <label for="zona" class="form-control-label">Zona</label>
                            <select name="zona" id="zona" class="form-control" disabled required >
                              <option value="" selected hidden>Seleccione la Zona</option>
                            </select>
                        </div>
 
                        <label for="latitud" class="form-control-label">Latitud: </label>
                        <input id="latitud" type="text" name="latitud" placeholder="Longitud..." readonly>
                        <label for="longitud" class="form-control-label">Longitud: </label>
                        <input id="longitud" type="text" name="longitud" placeholder="Latitud..." readonly>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card">
                                    <div class="card-header">
                                        <strong></strong><small> Localizar el parqueadero</small>
                                    </div>
                                    <div class="card-body card-block" style="padding: 10em;">
                                        <div id="floating-panel">
                                            <input id="address" type="textbox" value="" required>
                                            <input id="submit1" type="button" value="Localizar">
                                        </div>
                                        <div id="map"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning btn-lg btn-block">Registrar Parqueadero</button>
                        </div>
                    </form>

                    <form name="menu" method="get">
                        <div>
                            <input id="menu" name="menu" type="hidden" value="parqueaderos">
                            <button type="submit" class="btn btn-danger btn-lg btn-block">Regresar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function initMap() {
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 14,
                center: {
                    lat: 4.6487827,
                    lng: -74.0652178,
                },
                streetViewControl: false,
                mapTypeControl: false
            });
            var geocoder = new google.maps.Geocoder();
            var infowindow = new google.maps.InfoWindow;

            document.getElementById('submit1').addEventListener('click', function() {

                geocodeAddress(geocoder, map, infowindow);
            });
        }

        function geocodeAddress(geocoder, resultsMap, infowindow) {
            var address = document.getElementById('address').value;
            geocoder.geocode({
                'address': address
            }, function(results, status) {
                if (status === 'OK') {
                    resultsMap.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location

                    });
                    document.getElementById("latitud").value = results[0].geometry.location.lat();
                    document.getElementById("longitud").value = results[0].geometry.location.lng();

                    geocodeLatLng(geocoder, resultsMap, infowindow, results[0].geometry.location.lat(), results[0].geometry.location.lng());
                    //   alert('latitud: ' + results[0].geometry.location.lat());
                    //  alert('latitud: ' + results[0].geometry.location.lng());
                } else {
                    alert('Geocode was not successful for the following reason: ' + status);
                }
            });

        }

        function geocodeLatLng(geocoder, map, infowindow, latt, long) {
            var latlng = {
                lat: parseFloat(latt),
                lng: parseFloat(long)
            };
            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        document.getElementById("direccion").value = results[0].formatted_address;
                        selectZona();
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
        }
    </script>
    <script type="text/javascript" src="./diseño/js/jquery1.min.js"></script>
    <script>
        function selectZona() {
            var direccion1 = $("#direccion").val();
            $.ajax({
                url: "ModuloAdministrador/ajax-mapa.php",
                method: "POST",
                data: {
                    "direccion": direccion1
                },
                success: function(respuesta) {
                    $("#zona").attr("disabled", false);
                    $("#zona").html(respuesta);
                }
            })
        }
    </script>
    <script type="text/javascript">
	function solonumeros(e)
                    {
         var key = window.event ? e.which : e.keyCode;
                        if(key < 48 || key > 57)
                            e.preventDefault();
                    }
</script>