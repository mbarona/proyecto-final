<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/FuncionarioDAO.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ZonaDAO.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoFuncionario.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Auditoria.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Funcionario.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Zona.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/mailer/mailer.php';

function registrarFuncionario($nombre, $identificacion, $correo, $parqueadero){
	$conexion=new Conexion();
	$conexion=$conexion->conectarBD();
	$mail=new Mailer();

	ManejoFuncionario::setConexionBD($conexion);

	$password=ManejoFuncionario::contraseniaAleatoria();

	$funcionario = new Funcionario();
	$funcionario->setNombre($nombre);
	$funcionario->setEmail($correo);
	$funcionario->setIdentificacion($identificacion);
	$funcionario->setParqueadero($parqueadero);
	$funcionario->setContrasena($password);

	ManejoFuncionario::crearFuncionario($funcionario);

	$auditoria = new Auditoria();
	$auditoria->setCedula_usuario(0);
	$auditoria->setCod_afectado($identificacion);
	$auditoria->setTabla("FUNCIONARIO");
	$auditoria->setTipo_operacion("Registrar funcionario");
	ManejoAuditoria::setConexionBD($conexion);
	ManejoAuditoria::crearAuditoria($auditoria);

	/**
	* Texto para el mensaje del correo
	*/
	 $txt = "¡Hola ".$nombre."!, gracias por registrarte en Par-Kea Colombia.  Tu contraseña es: ".$password." .  Cuando inicies sesión por primera vez tendrás que cambiarla. <br> Cordialmente, <br> Par-Kea Colombia.";

	//Enviar correo electrónico
	$mail->enviarCorreo($correo,$txt,"Registro Par-kea Colombia");

	$redd = true;
	if ($redd) {
 		header("Location: Administrador.php?menu=funcionarios");
		exit();
	}
}

?>
