<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Zona.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Auditoria.php';



function registrarZona($nombre, $ciudad){
	$conexion=new Conexion();
	$conexion=$conexion->conectarBD();
	ManejoZona::setConexionBD($conexion);
	$zona = new Zona();
	$zona->setNomZona($nombre);
	$zona->setCodCiudad($ciudad);

	ManejoZona::crearZona($zona);

	$auditoria = new Auditoria();
	$auditoria->setCedula_usuario(0);
	$auditoria->setCod_afectado(0);
	$auditoria->setTabla("ZONA");
	$auditoria->setTipo_operacion("Registrar zona");
	ManejoAuditoria::setConexionBD($conexion);
	ManejoAuditoria::crearAuditoria($auditoria);

 	$redd = true;
	if ($redd) {
		header('Location: Administrador.php?menu=zonas');
		exit();
	}
}

?>