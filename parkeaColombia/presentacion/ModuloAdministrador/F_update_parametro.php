<?php
  require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

	#require_once ('../persistencia/util/Conexion.php');
	
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParametro.php';
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoDetalle_horario.php';
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoDias.php';

    
  $obj=new Conexion();
	$conexion=$obj->conectarBD();
	$cod_parqueadero=$_POST['cod'];
	ManejoParqueadero::setConexionBD($conexion);
  $parqueadero= ManejoParqueadero::buscarParqueadero($cod_parqueadero);
  
  ManejoParametro::setConexionBD($conexion);
  $parametro=ManejoParametro::listarParametroDeParqueadero($cod_parqueadero);
  $cod_parametro=$parametro->getCodigo();

  ManejoDetalle_horario::setConexionBD($conexion);
  $horarios=ManejoDetalle_horario::consultarDetalle_horario($cod_parametro);

  ManejoDias::setConexionBD($conexion);

	

?>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Editar parametros</strong><small> Gestión parqueaderos</small>
      </div>

      <div class="card-body card-block">
        <form class="form" role="form" autocomplete="off" id="formRegistroParametro" method="post" action="ModuloAdministrador/updateParametro.php">
          <div class="form-group">
            <label for="nombre" class="form-control-label"><strong>Tarifa</strong></label>
            <input type="number" id="tarifa" name="tarifa" required min=1 max=1000000 placeholder="Ingrese la Tarifa" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeypress="solonumeros(event);" maxlength = "7" class="form-control" value="<?php echo $parametro->getTarifa();?>">
          </div>
          <div class="form-group">
            <label for="nombre" class="form-control-label"><strong>Capacidad (min=1 max=1000)</strong></label>
            <input type="number" id="cantidad" name="cantidad" required min=1 max=1000 class="form-control" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeypress="solonumeros(event);" maxlength = "4" value="<?php echo $parametro->getCapacidad();?>">
          </div>
          <h3 style="margin-bottom: 1%;"><strong>Horarios</strong></h3>
          <?php
            $i=1;
            foreach($horarios as $horario){
              $dia=ManejoDias::consultarDia($horario->getCod_Dia());
              $inicio= explode(" ",$horario->getHora_inicio());
              $fin= explode(" ",$horario->getHora_final());

              echo '<div class="form-group">
                      <input type="hidden" value="'.$cod_parqueadero.'" name="cod_parqueadero">
                      <input type="hidden" value="'.$cod_parametro.'" name="cod_parametro">
                      <input type="hidden" value="'.$cod_parametro.'" name="cod_parametro">
                      <input type="hidden" value="'.$parametro->getCapacidadDisponible().'" name="dis" id="dis">
                      <label for="nombre" class="form-control-label"><strong>'.$dia->getNomDia().'</strong></label><br>
                      <div style="display: inline-flex; margin-right:1%;">
                            <div class="form-group"  style="margin-right: 100px;">
                                <label for="entreInicio" class="form-control-label">Hora de apertura</label>
                                <input type="time" id="entreInicio'.$i.'" name="entreInicio'.$i.'" required  class="form-control hora" value="'. $inicio[0].'" min="00:00:00" max="11:59:00">
                            </div>
                            <div class="form-group">
                                <label for="entreFinal" class="form-control-label">Hora de cierre</label>
                                <input type="time" id="entreFinal'.$i.'" name="entreFinal'.$i.'"  required  class="form-control hora" value="'.$fin[0].'" min="12:00:00" max="23:59:00">
                            </div>
                        </div>
                    </div>'; 
              $i++;
            }
          ?>       
          <div class="form-group">
          <button type="submit" class="btn btn-warning btn-lg btn-block">Confirmar</button>
          </div>
        </form>

          <form name="menu" method="get">
            <div>
              <input id="menu" name="menu" type="hidden" value="parqueaderos">
              <button type="submit" class="btn btn-danger btn-lg btn-block">Regresar</button>
            </div>
          </form>        
      </div>  
    </div>    
  </div>
</div>
<script type="text/javascript">
	function solonumeros(e)
                    {
         var key = window.event ? e.which : e.keyCode;
                        if(key < 48 || key > 57)
                            e.preventDefault();
                    }
</script>