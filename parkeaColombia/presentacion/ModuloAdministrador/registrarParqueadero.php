<?php

require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/negocio/ManejoParametro.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/negocio/ManejoDetalle_Horario.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/negocio/Auditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/negocio/Parqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/negocio/Parametro.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/proyecto-final/parkeaColombia/negocio/Detalle_Horario.php';


function registrarParqueadero($nombre, $zona, $tipo, $dirr, $lat, $lng, $tarifa, $capacidad, $entreInicio, $entreFinal, $finesInicio, $finesFinal)
{
	$conexion = new Conexion();
	$conexion = $conexion->conectarBD();

	ManejoParqueadero::setConexionBD($conexion);
	$parqueadero = new Parqueadero();
	$parqueadero->setNomParqueadero($nombre);
	$parqueadero->setCodZona($zona);
	$parqueadero->setCodTipoParqueadero($tipo);
	$parqueadero->setDireccion($dirr);
	$parqueadero->setLatitud($lat);
	$parqueadero->setLongitud($lng);
	ManejoParqueadero::crearParqueadero($parqueadero);


	ManejoParametro::setConexionBD($conexion);
	$parqueaderoUl = ManejoParqueadero::ultimoParqueadero();
	$parametro = new Parametro();
	$parametro->setTarifa($tarifa);
	$parametro->setCodParqueadero($parqueaderoUl->getCodParqueadero());
	$parametro->setCapacidadDisponible($capacidad);
	$parametro->setCapacidad($capacidad);
	ManejoParametro::crearParametro($parametro);

	$parametroUl = ManejoParametro::ultimoParametro();

	ManejoDetalle_horario::setConexionBD($conexion);
	for ($i = 1; $i < 8; $i++) {
		$detalle = new Detalle_Horario();

		if ($i < 6) {
			$detalle->setCod_dia($i);

			$detalle->setHora_inicio($entreInicio);
			$detalle->setHora_final($entreFinal);
		} elseif ($i > 5) {
			$detalle->setCod_dia($i);

			$detalle->setHora_inicio($finesInicio);
			$detalle->setHora_final($finesFinal);
		}
		$detalle->setCod_parametro($parametroUl->getCodigo());
		ManejoDetalle_horario::crearDetalle_horario($detalle);
	}



	$auditoria = new Auditoria();
	$auditoria->setCedula_usuario(0);
	$auditoria->setCod_afectado(0);
	$auditoria->setTabla("PARQUEADERO");
	$auditoria->setTipo_operacion("Registrar parqueadero");
	ManejoAuditoria::setConexionBD($conexion);
	ManejoAuditoria::crearAuditoria($auditoria);

	$redd = true;
	if ($redd) {
		header("Location: Administrador.php?menu=parqueaderos");
		exit();
	}
}
