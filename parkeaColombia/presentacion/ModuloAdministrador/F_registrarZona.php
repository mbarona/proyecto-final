<?php
  
    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';   
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Zona.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCiudad.php'; 
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoDepartamento.php';

    require_once 'validaciones.php';
    require_once 'registrarZona.php';

    $obj=new Conexion();
    $conexion=$obj->conectarBD();
   
    ManejoDepartamento::setConexionBD($conexion);
    ManejoCiudad::setConexionBD($conexion);

    $departamentos=ManejoDepartamento::listarDepartamentos();

    $nombre = isset($_POST['nombre']) ? $_POST['nombre'] : null;
    $ciudad = isset($_POST['idCiudad']) ? $_POST['idCiudad'] : null;
    $errores = array();

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //Valida que el campo nombre no esté vacío.
    if (!validaRequerido($nombre)) {
        $errores[] = 'El Nombre es Obligatorio.';
    }
    if (!ctype_alpha(str_replace(array("\n", "\t", ' '), '', $nombre))) {
      $errores[] = 'El nombre solo debe contener letras y espacios.';
  }
    
    if(!$errores){
        registrarZona($nombre, $ciudad);
    }


}
?>

<?php if ($errores): ?>   
  <?php foreach ($errores as $error): ?>
        <a href='#' style="cursor: initial;">
        <div class="sufee-alert alert with-close alert-danger alert-dismissible fade-show">
          <span class="badge badge-pill badge-danger">Error</span>
          <?php echo $error ?>
          <img src="diseño/images/3.png" style="cursor: pointer;" width="10" title='Cerrar' alt="Close" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" />
        </div>
        </a>
      <?php endforeach; ?>
  <?php endif; ?>
  <?php if (!$errores): ?>   
      <?php foreach ($errores as $error): ?>
          <div class="sufee-alert alert with-close alert-success alert-dismissible fade-show"> 
          <span class="badge badge-pill badge-success">Correcto</span>  
          <?php echo $error ?>
          </div>
      <?php endforeach; ?>
  <?php endif; ?>
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <strong>Registrar Zona de Parqueaderos</strong><small>  Gestión Zonas</small>
      </div>

      <div class="card-body card-block">
      <form class="form" role="form" autocomplete="off" id="formRegistroParqueadero"  method="post">
       <div class="form-group">
          <label for="nombre" class="form-control-label">Nombre de la Zona</label>
          <input type="text" id="nombre" name="nombre" placeholder="Ingrese Nombre Zona" maxlength="80" class="form-control" required>
        </div>
        <div class="form-group">
          <label for="Zona" class="form-control-label">Ubicación Zona</label>
          <select id="idDepartamento" class="form-control" onchange="selectCiudad()" required>
            <option value="">Seleccione el Departamento</option>           
                  <?php 
                      foreach ($departamentos as $d) {
                         echo'<option value="'.$d->getCodDepartamento().'">'.$d->getNomDepartamento().'</option>';
                       } 
                  ?>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control input-lg" id="idCiudad" name="idCiudad" required disabled>
            <option value="">Seleccione la Ciudad</option>
          </select>
        </div>     
        <div class="form-group">
        <button type="submit" class="btn btn-warning btn-lg btn-block">Registrar Zona</button>
        </div>
      </form>
      <form name="menu" method="get">
        <div>
          <input id="menu" name="menu" type="hidden" value="zonas">
          <button type="submit" class="btn btn-danger btn-lg btn-block">Regresar</button>
        </div>
      </form>        
      </div>  
    </div>    
  </div>




<script>
  function selectCiudad() {
  var idDepartamento = $("#idDepartamento").val();
  $.ajax({
    url:"ModuloAdministrador/selectCiudad.ajax.php",
    method: "POST",
    data: {
      "idDepartamento":idDepartamento
      },
      success: function(respuesta){
        $("#idCiudad").attr("disabled", false);
       $("#idCiudad").html(respuesta);
      }
    })
  }
</script>
