<?php
	require('../script.php');
?>

<!DOCTYPE html>
<html lang="es">

<head>

	<title> Modulo Funcionario</title>

</head>
<body >
<header  class="site-header" >
	<?php
		require_once('header.php');
	?>
</header>

<section>
	<div class="container">
	<?php
			require_once('routing.php');
	 ?>

	</div>
</section>

<footer class="site-footer">
	<?php
		include_once('footer.php');
	?>
</footer>
</body>
</html>
