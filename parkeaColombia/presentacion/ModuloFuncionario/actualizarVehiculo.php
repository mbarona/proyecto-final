<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoVehiculo.php';

if(!isset($_SESSION['email'])){
	header('Location: ../login.php' );
}
$conexion=new Conexion();
$conexionUsar=$conexion->conectarBD();
$cedulaCliente=$_POST["numCedula"];

ManejoVehiculo::setConexionBD($conexionUsar);

$vehiculos=ManejoVehiculo::consultarVehiculoPorCliente($cedulaCliente);

echo'<option value="" disabled selected hidden>Seleccione vehiculo</option>';
foreach ($vehiculos as $vehiculo) {
     echo'<option value="'.$vehiculo->getPlaca().'">'.$vehiculo->getTipo_vehiculo().'</option>';
}
?>