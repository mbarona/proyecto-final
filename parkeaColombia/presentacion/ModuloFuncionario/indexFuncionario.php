	<?php
	session_start();
	if(!isset($_SESSION['email'])){
		header('Location: ../login.php' );
	}

	$nombreParqueadero=$_SESSION['nom_parqueadero'];
	$nombreFuncionario=$_SESSION['NombreFuncionario'];
	$codParqueadero=$_SESSION['cod_parqueadero'];
	$codFuncionario=$_SESSION['identificacion_funcionario'];


	?>
	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="../diseño/Funcionario/img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="codepixer">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Funcionario</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<!--
	CSS
	============================================= -->
	<link rel="stylesheet" href="../diseño/Funcionario/css/linearicons.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/font-awesome.min.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/bootstrap.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/magnific-popup.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/nice-select.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/animate.min.css">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/owl.carousel.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

	</head>
	<body background="diseño/images/Fondos/1.jpg" style="background-color:#b2db71; background-size:60% 80%; background-repeat: no-repeat; background-position: center 0;">
<!--  modal  -->

	<div id="myModal" class="modal fade"  role="dialog">
  <div class="modal-dialog modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 ></h4>
      </div>
      <div class="modal-body">
       <center> <p><?php echo $_SESSION["mensaje"] ?> </p> </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>





	<header id="header" id="home">
	<div class="container">
	<div class="row align-items-center justify-content-between d-flex">
	<div id="logo">
	<a href="index.html"></a>
	</div>
	<nav id="nav-menu-container">
	</nav><!-- #nav-menu-container -->
	</div>
	</div>
	</header><!-- #header -->


	<!-- start banner Area -->
	<section  id="home">
	<div class="overlay overlay-bg"></div>
	<div class="container">
	<div class="row fullscreen d-flex align-items-center justify-content-center header-right">
	<div class="banner-content col-lg-7 col-md-6 ">

	<center><img src="../diseño/Funcionario/img/logos/img02.png" width="200" height="210" alt="" title="" />
	<br><br><br>
	<h5 class="text-white text-uppercase">
	¡Bienvenido, <?php echo $nombreFuncionario."!"?>
	</h5>
	<h4 class="text-white text-uppercase">
		<?php echo $nombreParqueadero?>
	</h4>
	<p class="pt-20 pb-20 text-white">
	¿Deseas Registrar un Cliente?
	</p>
	<a href="formularioCliente.php" class="primary-btn text-uppercase">Registrar Cliente</a>
	<div class="">
<br>
	</div>
	<a href="../logout.php" class="primary-btn btn-block-10  text-uppercase">&nbsp &nbsp Cerrar  Sesión &nbsp &nbsp  </a>
	</center>
	</div>

	<div class="col-lg-5  col-md-6 header-right">

	<h4 class="text-white pb-30">Registro Ingreso/Salida	</h4>

	<form class="form" role="form" autocomplete="off" action="registrarIngresoSalida.php" method="post">

	<div class="from-group">
	<input class="form-control txt-field " type="text" name="placa" pattern="[A-Z]{3}\d{3}" title="AAA123"placeholder="Placa Vehiculo" minlength="6" maxlength="6" onKeyUp="this.value=this.value.toUpperCase();" required="true">
	</div>


	<div class="form-group">
	<div class="default-select" id="default-select">
	<select required="true" name="tipoAccion">
	<option value="" disabled selected hidden>Seleccione una opción</option>
	<option value="1">Ingreso</option>
	<option value="2">Salida</option>
	</select>
	</div>
	</div>


	<div class="form-group row">
	<div class="col-md-12">
	<button type="submit" class="btn btn-default btn-lg btn-block text-center text-uppercase">Registrar</button>
	</div>
	</div>
	</form>

	<hr >


	<h4 class="text-white pb-30">Registro de ingreso sin reserva</h4>
	<form class="form" role="form" autocomplete="off" action="formularioReserva.php" method="post">

	<div class="from-group">
	<input class="form-control txt-field" type="number"  min="1" name="cedulaCliente" placeholder="Cédula del cliente" required="true" id="numCedula"  autofocus>
	</div>



	<div class="form-group row">
	<div class="col-md-12">
	<button type="submit" class="btn btn-default btn-lg btn-block text-center text-uppercase">Confirmar</button>
	</div>
	</div>

	</form>

	</div>
	</div>

	</div>




	</div>




	</div>





	<!-- start footer Area -->
	<footer class="footer-area section-gap">
	<div class="container">
	<div class="row">

	<p class="mt-50 mx-auto footer-text col-lg-12">
	<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
	Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="#">SoftUp Enterprise</a>
	<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
	</p>
	</div>
	</div>
	</footer>
	<!-- End footer Area -->
	</section>
	<!-- End banner Area -->




	<script src="../diseño/Funcionario/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="../diseño/Funcionario/js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="../diseño/Funcionario/js/easing.min.js"></script>
	<script src="../diseño/Funcionario/js/hoverIntent.js"></script>
	<script src="../diseño/Funcionario/js/superfish.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.ajaxchimp.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.magnific-popup.min.js"></script>
	<script src="../diseño/Funcionario/js/owl.carousel.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.sticky.js"></script>
	<script src="../diseño/Funcionario/js/jquery.nice-select.min.js"></script>
	<script src="../diseño/Funcionario/js/waypoints.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.counterup.min.js"></script>
	<script src="../diseño/Funcionario/js/parallax.min.js"></script>
	<script src="../diseño/Funcionario/js/mail-script.js"></script>
	<script src="../diseño/Funcionario/js/main.js"></script>
	</body>
	</html>
<!--< ?php } ?> -->
<?php
if(isset($_SESSION["mensaje"])){
	
	echo '<script language="JavaScript">';
echo "$('#myModal').modal('show')";
echo '</script>';
}
unset($_SESSION["mensaje"]);

?>