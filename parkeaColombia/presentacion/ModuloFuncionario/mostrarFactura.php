<?php
session_start();
if(!isset($_SESSION['email'])){
	header('Location: ../login.php' );
}
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParametro.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';

$codReserva=$_SESSION["codReserva"];


$conexion=new Conexion();
$conexionBD=$conexion->conectarBD();

ManejoReserva::setConexionBD($conexionBD);
ManejoCliente::setConexionBD($conexionBD);
ManejoParametro::setConexionBD($conexionBD);
date_default_timezone_set("America/Bogota");
$reserva=ManejoReserva::consultarReserva($codReserva);
$placa=$reserva->getVehiculo()->getPlaca();
$parqueadero=$reserva->getParqueadero();
$codCliente=ManejoCliente::obtenerClienteDeUnaReserva($codReserva);
$cliente=ManejoCliente::consultarCliente($codCliente);
$puntos=$cliente->getPuntos();


$parametro=ManejoParametro::listarParametroDeParqueadero($parqueadero->getCodParqueadero());

$fechaFinal=$reserva->getHora_salida_real();
if($fechaFinal==""||$fechaFinal==null){
	$fechaFinal=date('Y-m-d H:i:s ');
}
$dateFinal = new DateTime($fechaFinal);
$fechaInicial=$reserva->getHora_ingreso_real();
$dateInicial = new DateTime($fechaInicial);
$diferencia=$dateFinal->diff($dateInicial);

$dias=$diferencia->format('%d');
$horas=$diferencia->format('%H');
$minutos=$diferencia->format('%i');
  



$valorTarifaPArametro=$parametro->getTarifa();
$diasAHoras=$dias*24;
$valorMiutos=($valorTarifaPArametro/60)*$minutos;
$valorHoras=($diasAHoras+$horas)*$valorTarifaPArametro;
$valorTotal=$valorHoras+$valorMiutos;



?>

<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="../diseño/Funcionario/img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="codepixer">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	
	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<!-- Site Title -->
	<title>Factura</title>

	<!--
	CSS
	============================================= -->
	<link rel="stylesheet" href="../diseño/Funcionario/css/linearicons.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/font-awesome.min.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/bootstrap.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/magnific-popup.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/nice-select.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/animate.min.css">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/owl.carousel.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/main.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>
	<body>

<!--  modal  -->

<div id="myModal" class="modal fade"  role="dialog">
  <div class="modal-dialog modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 ></h4>
      </div>
      <div class="modal-body">
       <center> <p><?php echo $_SESSION["mensaje"] ?> </p> </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>





	<header id="header" id="home">
	<div class="container">
	<div class="row align-items-center justify-content-between d-flex">
	<div id="logo">
	<a href="indexFuncionario.php"></a>
	</div>
	<nav id="nav-menu-container">
	</nav><!-- #nav-menu-container -->
	</div>
	</div>
	</header><!-- #header -->
	
	
	<!-- start banner Area -->
	<section class="banner-area relative" id="home">
	<div class="overlay overlay-bg"></div>
	<div class="container">
	<div class="row fullscreen d-flex align-items-center justify-content-center">
	<div class="banner-content col-lg-7 col-md-6 ">
	

	
	
    <center>
	<div class="col-lg-10 col-md-6 header-right">
	<h4 class="text-white pb-30">Descripción de Factura</h4>
	<form class="form" role="form" autocomplete="off" action="registrarFactura.php" method="post">
	
    
	<div class="from-group ">
    <label for="placa"class="text-white">Placa</label>
	<input class="form-control txt-field" type="text" name="placa"  id="placa"placeholder="Placa" required="true" readonly value="<?php echo $placa ?>">
    </div>
	<div class="from-group">
    <label for="horaInicio"class="text-white">Hora Ingreso</label>
	<input class="form-control txt-field" id="horaInicio"type="datetime" name="HoraInicio" placeholder="Hora Inicio" required="true" value="<?php echo $fechaInicial ?>" readonly>
	</div>
    <div class="from-group">
    <label for="horaSalida"class="text-white">Hora Salida</label>
	<input class="form-control txt-field" id="horaSalida"type="datetime" name="HoraSalida" placeholder="Hora Salida" required="true" value="<?php echo $fechaFinal ?>" readonly>
	</div>
    <div class="from-group">
    <label for="tiempoEstacionado"class="text-white">Tiempo en Parqueadero</label>
	<input class="form-control txt-field" id="tiempoEstacionado"type="datetime" name="tiempoEstacionado" placeholder="Tiempo En Parqueadero" required="true" value="<?php echo "Días: ".$dias.", horas: ".$horas.", minutos: ".$minutos; ?>" readonly>
	</div>
    <div class="from-group">
    <label for="valTotal"class="text-white">Valor Total</label>
	<input class="form-control txt-field" id="valTotal" type="text" name="valorTotal" placeholder="Valor Total" required="true" value="<?php echo "$".$valorTotal; ?>" readonly>
	</div>
    <div class="from-group">
    <label class="text-white">Seleccione forma de pago</label>
    <div>
	<input type="radio" id="efectivo" name="f_pago" value="1">
<label for="efectivo" class="text-white">Efectivo</label>
&nbsp
&nbsp
<input type="radio" id="tarjeta" name="f_pago" value="2">
<label for="tarjeta" class="text-white">Tarjeta</label>
&nbsp
&nbsp
<input type="radio" id="puntos" name="f_pago" value="3">
<label for="puntos" class="text-white">Puntos</label>
</div>
</div>
	<div class="form-group row">
	<div class="col-md-12">
	<button type="submit" class="btn btn-default btn-lg btn-block text-center text-uppercase" >Confirmar</button>
	</div>
	</div>
    
	<div class="form-group row">
	<div class="col-md-12">
	<a href="indexFuncionario.php"><input type="button" class="btn btn-default btn-lg btn-block text-center text-uppercase" value="Regresar"></a>

	</div>
	</div>

	</form>
	</div>
	</div>
	</div>
    </center>
	</div>
 <!-- Modal-->
	<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <p>Some text in the modal.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>


	<!-- start footer Area -->
	<footer class="footer-area section-gap">
	<div class="container">
	<div class="row">
	
	<p class="mt-50 mx-auto footer-text col-lg-12">
	<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
	Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="#">SoftUp Enterprise</a>
	<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
	</p>
	</div>
	</div>
	</footer>
	<!-- End footer Area -->
	</section>
	<!-- End banner Area -->
	
	
	
	
	<script src="../diseño/Funcionario/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="../diseño/Funcionario/js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="../diseño/Funcionario/js/easing.min.js"></script>
	<script src="../diseño/Funcionario/js/hoverIntent.js"></script>
	<script src="../diseño/Funcionario/js/superfish.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.ajaxchimp.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.magnific-popup.min.js"></script>
	<script src="../diseño/Funcionario/js/owl.carousel.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.sticky.js"></script>
	<script src="../diseño/Funcionario/js/jquery.nice-select.min.js"></script>
	<script src="../diseño/Funcionario/js/waypoints.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.counterup.min.js"></script>
	<script src="../diseño/Funcionario/js/parallax.min.js"></script>
	<script src="../diseño/Funcionario/js/mail-script.js"></script>
	<script src="../diseño/Funcionario/js/main.js"></script>
	</body>
	</html>
	<?php
if(isset($_SESSION["mensaje"])){
	
	echo '<script language="JavaScript">';
echo "$('#myModal').modal('show')";
echo '</script>';
}
unset($_SESSION["mensaje"]);

?>
