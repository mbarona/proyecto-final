<?php
	session_start();
	if(!isset($_SESSION['email'])){
		header('Location: ../login.php' );
	}

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoVehiculo.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

$codParqueadero=$_SESSION['cod_parqueadero'];
$nomParqueadero=$_SESSION['nom_parqueadero'];


$cedulaCliente=$_POST["cedulaCliente"];
$con= new Conexion();
$conexion=$con->conectarBD();
if($conexion->connect_error){
	die("Problema de conexión con la base de datos: ".$conexion->connect_error);
}
ManejoVehiculo::setConexionBD($conexion);
ManejoCliente::setConexionBD($conexion);
$cliente=ManejoCliente::consultarCliente($cedulaCliente);
if($cliente->getNombre()==null || $cliente->getNombre()==""){
	$_SESSION["mensaje"]="¡El cliente ingresado no está registrado!";
	header('Location: indexFuncionario.php');
}else{
$vehiculos=ManejoVehiculo::consultarVehiculoPorCliente($cedulaCliente);
$fecha= date('Y-m-j H:i:s');
$fechaMax = strtotime ( '+1 hour' , strtotime ($fecha) ) ;
}
	?>
	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
	<script type="text/javascript" src="./diseño/js//jquery.min.js"></script>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="../diseño/Funcionario/img/fav.png">
	<!-- Author Meta -->
	<meta name="author" content="codepixer">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Ingreso sin reserva</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
	<!--
	CSS
	============================================= -->
	<link rel="stylesheet" href="../diseño/Funcionario/css/linearicons.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/font-awesome.min.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/bootstrap.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/magnific-popup.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/nice-select.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/animate.min.css">
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/owl.carousel.css">
	<link rel="stylesheet" href="../diseño/Funcionario/css/main.css">
	
	</head>
	<body background="diseño/images/Fondos/1.jpg" style="background-color:#b2db71; background-size:60% 80%; background-repeat: no-repeat; background-position: center 0;">
	<header id="header" id="home">
	<div class="container">
	<div class="row align-items-center justify-content-between d-flex">
	<div id="logo">
	<a href="indexFuncionario.php"></a>
	</div>
	<nav id="nav-menu-container">
	</nav><!-- #nav-menu-container -->
	</div>
	</div>
	</header><!-- #header -->


	<!-- start banner Area -->
	<section  id="home">
	<div class="overlay overlay-bg"></div>
	<div class="container">
	<div class="row  d-flex align-items-center justify-content-center ">
	<div class="banner-content col-lg-10 col-md-6 ">




    <center>
	<div class="col-lg-6  col-md-6 header-right">
	<h4 class="text-white pb-30">Registro de ingreso</h4>
	<form class="form" role="form" autocomplete="off" action="registrarReserva.php" method="post">
    <div class="from-group">
	<input class="form-control txt-field" type="hidden" name="codParqueadero" value="<?php echo $codParqueadero?>">
	</div>

	<div class="from-group">
	<label for="Name" class="text-white pb-12">Número de identificación</label>

	<input class="form-control txt-field" type="text"  name="cedulaCliente" placeholder="Número de identificación" required="true" value="<?php echo $cedulaCliente?>" readonly>
	</div>
	<div class="from-group">

	<label for="Name" class="text-white pb-12">Nombre</label>
	<input class="form-control txt-field" type="text"  name="nombreCliente" placeholder="Nombre" required="true" value="<?php echo $cliente->getNombre()?>" readonly>
	</div>
	<div class="from-group">
	<label for="Name" class="text-white pb-12">Vehiculo</label>
    <div class="default-select" id="default-select">

				<select name="codVehiculo" id="idVehiculo" disable autofocus>
						<option value="" disabled selected hidden>Seleccione vehiculo</option>
						<?php foreach ($vehiculos as $vehiculo) {
		?>
		<option value="<?php echo $vehiculo->getPlaca(); ?>" ><?php echo $vehiculo->getPlaca()."-".$vehiculo->getTipo_vehiculo(); ?></option>
						<?php } ?>
				</select>

			</div>
    </div>

	<div class="from-group">

	<label for="Name" class="text-white pb-12">Parqueadero</label>
	<input class="form-control txt-field" type="text"  name="cedulaCliente" placeholder="Número de identificación" required="true" value="<?php echo $nomParqueadero?>" readonly>
	</div>
	<div class="from-group">
	
<label for="Name" class="text-white pb-12">Fecha y hora de salida</label>
<input class="form-control txt-field" type="datetime-local" value="now" id="fechaSalida" name="fechaFinal" value="" min="<?php echo date('Y-m-d\TH:i', $fechaMax); ?>"placeholder="" required="true" >

</div>
<div class="form-check text-white">
		  <input id="checkHora" class="form-check-input" type="checkbox" value="" onchange="chequeado();">
		  <small id="emailHelp" class="text-white">No especifica la hora de salida.</small>
		</div>


	<div class="form-group row">
	<div class="col-md-12">
	<button type="submit" class="btn btn-default btn-lg btn-block text-center text-uppercase">Confirmar</button>
	</div>
	</div>

	<div class="form-group row">
	<div class="col-md-12">
	<a href="indexFuncionario.php"><input type="button" class="btn btn-default btn-lg btn-block text-center text-uppercase" value="Regresar"></a>

	</div>
	</div>

	</form>
	</div>
	</div>
	</div>
    </center>
	</div>
	<!-- start footer Area -->
	<footer class="footer-area section-gap">
	<div class="container">
	<div class="row">

	<p class="mt-50 mx-auto footer-text col-lg-12">
	<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
	Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="#">SoftUp Enterprise</a>
	<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
	</p>
	</div>
	</div>
	</footer>
	<!-- End footer Area -->
	</section>
	<!-- End banner Area -->




	<script src="../diseño/Funcionario/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="../diseño/Funcionario/js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="../diseño/Funcionario/js/easing.min.js"></script>
	<script src="../diseño/Funcionario/js/hoverIntent.js"></script>
	<script src="../diseño/Funcionario/js/superfish.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.ajaxchimp.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.magnific-popup.min.js"></script>
	<script src="../diseño/Funcionario/js/owl.carousel.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.sticky.js"></script>
	<script src="../diseño/Funcionario/js/jquery.nice-select.min.js"></script>
	<script src="../diseño/Funcionario/js/waypoints.min.js"></script>
	<script src="../diseño/Funcionario/js/jquery.counterup.min.js"></script>
	<script src="../diseño/Funcionario/js/parallax.min.js"></script>
	<script src="../diseño/Funcionario/js/mail-script.js"></script>
	<script src="../diseño/Funcionario/js/main.js"></script>
	</body>
	</html>




<script>
  function selectVehiculos() {
  var numCedula = $("#numCedula").val();
  $.ajax({
    url:"ModuloFuncionario/actualizarVehiculo.php",
    method: "POST",
    data: {
      "numCedula":numCedula
      },
      success: function(respuesta){
        $("#idVehiculo").attr("disabled", false);
       $("#idVehiculo").html(respuesta);
      }
    })
  }

  function chequeado(){

if(document.getElementById("checkHora").checked)
	{
   document.getElementById('fechaSalida').type = 'hidden';
   }
else
	{
	document.getElementById('fechaSalida').type = 'datetime-local';
   }
	
}
</script>
