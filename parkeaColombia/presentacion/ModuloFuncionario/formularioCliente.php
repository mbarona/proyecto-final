	<?php
session_start();
//if(!isset($_SESSION['email'])){
//header('Location: ../login.php' );
//}
if(!isset($_SESSION['numTarjeta'])){
	$numTarjeta="";
}else{
	$numTarjeta=$_SESSION['numTarjeta'];
	
}
	?>
	<!DOCTYPE html>
	<html lang="zxx" class="no-js">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="../diseño/Funcionario/img/fav.png">
		<!-- Author Meta -->
		<meta name="author" content="codepixer">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Registro Cliente</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
			<!--
			CSS
			============================================= -->
			<link rel="stylesheet" href="../diseño/Funcionario/css/linearicons.css">
			<link rel="stylesheet" href="../diseño/Funcionario/css/font-awesome.min.css">
			<link rel="stylesheet" href="../diseño/Funcionario/css/bootstrap.css">
			<link rel="stylesheet" href="../diseño/Funcionario/css/magnific-popup.css">
			<link rel="stylesheet" href="../diseño/Funcionario/css/nice-select.css">
			<link rel="stylesheet" href="../diseño/Funcionario/css/animate.min.css">
			<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
			<link rel="stylesheet" href="../diseño/Funcionario/css/owl.carousel.css">
			<link rel="stylesheet" href="../diseño/Funcionario/css/main.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  			<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  		
		</head>
		<body background="diseño/images/Fondos/1.jpg" style="background-color:#b2db71; background-size:60% 80%; background-repeat: no-repeat; background-position: center 0;">

<!--  modal  -->

<div id="myModal" class="modal fade"  role="dialog">
  <div class="modal-dialog modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 ></h4>
      </div>
      <div class="modal-body">
       <center> <p><?php echo $_SESSION["mensaje"] ?> </p> </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>




			  <header id="header" id="home">
			    <div class="container">
			    	<div class="row align-items-center justify-content-between d-flex">
				      <div id="logo">
				        <a href="index.html"></a>
				      </div>
				      <nav id="nav-menu-container">

				      </nav><!-- #nav-menu-container -->
			    	</div>
			    </div>
			  </header><!-- #header -->


			<!-- start banner Area -->
			<section class="relative" id="home">
				<div class="overlay overlay-bg"></div>
				<div class="container">
					<div class="row d-flex align-items-center justify-content-center">
						<div class="banner-content col-lg-7 col-md-6 ">


						<div class="col-lg-11  col-md-6 header-right">
							<h4 class="text-white pb-30"> <center> Registro Cliente </center>	</h4>
							<form class="form" role="form" autocomplete="off" id="formRegistroCliente" method="post" action="registrarCliente.php">
								<div class="from-group">
									<input class="form-control txt-field" type="number" name="cedula" placeholder="Cedula Cliente" required="true">
									<input class="form-control txt-field" type="text" name="nombre" placeholder="Nombre Completo" required="true">
									<input class="form-control txt-field" id="email"type="email" name="email" pattern="/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3,4})+$/"placeholder="Correo Electronico" required="true">
									<input class="form-control txt-field" type="text" name="placa"  pattern="[A-Z]{3}\d{3}" title="AAA123"placeholder="Placa" minlength="6" maxlength="6" onKeyUp="this.value=this.value.toUpperCase();" required="true">
								   
											<select class="form-control" name="tipoVeh" required="true">
												<option value="">Tipo de Vehiculo</option>
												<option value="1">Automovil</option>
												<option value="2">Motocicleta</option>
											</select></p>
										
										
									<input class="form-control txt-field" type="text" name="numTarjeta" required="true" readonly <?php if($numTarjeta==""){
										echo 'placeholder="Número de Tarjeta "';
										}else {
											echo ' value="'.$numTarjeta.'"';
											}?> >
					   		</div>



							<center>
							
							    <div class="form-group row">
							        <div class="col-md-12">
							            <button type="submit" class="btn btn-default btn-lg btn-block text-center text-uppercase">Registrar</button>

							        </div>
							    </div>
								</center>
							</form>
							<div class="form-group row">

							        <div class="col-md-12">
							           <a href="../ModuloCliente/agregarTarjeta.php"><button <?php if ($numTarjeta!="") echo 'disabled="true"'?> onclick="" class="btn btn-default btn-lg btn-block text-center text-uppercase">Agregar Tarjeta</button></a>

							        </div>
							    </div>
								<div class="form-group row">
							<div class="col-md-12">
											<?php if(!isset($_SESSION['email'])){ ?>

								<a href="../login.php"><button onclick="" class="btn btn-default btn-lg btn-block text-center text-uppercase">Regresar</button></a>
											<?php unset($_SESSION['numTarjeta']);}else{?>
								<a href="indexFuncionario.php"><button onclick="" class="btn btn-default btn-lg btn-block text-center text-uppercase">Regresar</button></a>
											<?php unset($_SESSION['numTarjeta']);}?>
							</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End banner Area -->


			<!-- start footer Area -->
			<footer class="footer-area section-gap">
				<div class="container">
					<div class="row">


						<p class="mt-50 mx-auto footer-text col-lg-12">
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
							Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="#">SoftUp Enterprise</a>
								<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						</p>
					</div>
				</div>
			</footer>
			<!-- End footer Area -->

			<script src="../diseño/Funcionario/js/vendor/jquery-2.2.4.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="../diseño/Funcionario/js/vendor/bootstrap.min.js"></script>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  			<script src="../diseño/Funcionario/js/easing.min.js"></script>
			<script src="../diseño/Funcionario/js/hoverIntent.js"></script>
			<script src="../diseño/Funcionario/js/superfish.min.js"></script>
			<script src="../diseño/Funcionario/js/jquery.ajaxchimp.min.js"></script>
			<script src="../diseño/Funcionario/js/jquery.magnific-popup.min.js"></script>
			<script src="../diseño/Funcionario/js/owl.carousel.min.js"></script>
			<script src="../diseño/Funcionario/js/jquery.sticky.js"></script>
			<script src="../diseño/Funcionario/js/jquery.nice-select.min.js"></script>
			<script src="../diseño/Funcionario/js/waypoints.min.js"></script>
			<script src="../diseño/Funcionario/js/jquery.counterup.min.js"></script>
			<script src="../diseño/Funcionario/js/parallax.min.js"></script>
			<script src="../diseño/Funcionario/js/mail-script.js"></script>
			<script src="../diseño/Funcionario/js/main.js"></script>
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		</body>
	</html>
	<?php
if(isset($_SESSION["mensaje"])){
	
	echo '<script language="JavaScript">';
echo "$('#myModal').modal('show')";
echo '</script>';
}
unset($_SESSION["mensaje"]);

?>

<script>

$( "#email" ).blur(function() {
	var email=document.getElementById("email").value;
	validarEmail(email);
      
      // tu codigo ajax va dentro de esta function...
    });
function validarEmail(valor) {
  if (/^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i.test(valor)){
	 

  } else {
   alert("La dirección de email es incorrecta.");
  }
}
</script>