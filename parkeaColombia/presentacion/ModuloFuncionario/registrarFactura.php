<?php
session_start();

if(!isset($_SESSION['email'])){
	header('Location: ../login.php' );
}
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoFactura.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/mailer/mailer.php';

if(!isset($_POST["f_pago"])){
    $_SESSION["mensaje"]="Se debe seleccionar una forma de pago";
    header('Location: mostrarFactura.php');
}

$conexion=new Conexion();
$conexionBD=$conexion->conectarBD();

ManejoFactura::setConexionBD($conexionBD);
ManejoParqueadero::setConexionBD($conexionBD);
ManejoCliente::setConexionBD($conexionBD);
ManejoReserva::setConexionBD($conexionBD);


$valorTotal=$_POST["valorTotal"];
$codReserva=$_SESSION["codReserva"];
$formaPago=$_POST["f_pago"];
$registrarFactura=false;
 

$reserva=ManejoReserva::consultarReserva($codReserva);
$parqueadero=ManejoParqueadero::buscarParqueadero($reserva->getParqueadero()->getCodParqueadero());
$codCliente=ManejoCliente::obtenerClienteDeUnaReserva($codReserva);
$cliente=ManejoCliente::consultarCliente($codCliente);

if($formaPago==2){
//REdireccionar a pago con tarjeta

$nombre=$cliente->getNombre();
$numTarjeta=$cliente->getTarjeta(); //Se debe actualizar el dao, retorna el codigo más no la tarjeta
$ultimos4=substr($numTarjeta,-4);
$correo=$cliente->getEmail();
$mail=new Mailer();


/**
 * Texto para el mensaje del correo
 */
$txt = "¡Hola ".$nombre."!, se ha registrado un pago por ".$valorTotal." cargado a tu tarjeta número **** **** **** ".$ultimos4." en Par-Kea Colombia.  <br> Cordialmente, <br> Par-Kea Colombia.";

//Enviar correo electrónico
$subject="Pago de servicios";
$mail->enviarCorreo($correo,$txt,$subject);

$factura=new Factura();
$factura->setCod_reserva($codReserva);
$factura->setPrecio_total($valorNuevo);
$factura->setCod_f_pago($formaPago);

registrarFactura($codReserva,$factura);
$_SESSION["mensaje"]="¡Pago exitoso!";
header('Location: indexFuncionario.php');

}else if($formaPago==3){
//Disminuir puntos
$puntos=$cliente->getPuntos();
if($puntos<2){
    
    $_SESSION["mensaje"]="¡No es posible pagar con puntos! <br> Puntos insuficientes" ;
header('Location: mostrarFactura.php');

    
}else{

if($parqueadero->getCodTipoParqueadero()==1){
    $valorNuevo=$valorTotal*0.5;
    $valorDescuento=$valorTotal-$valorNuevo;
}else if($parqueadero->getCodTipoParqueadero()==2){
    $valorNuevo=$valorTotal*0.7;
    $valorDescuento=$valorTotal-$valorNuevo;
}else{
    $valorNuevo=0;
    $valorDescuento=$valorTotal;
}

$factura=new Factura();
$factura->setCod_reserva($codReserva);
$factura->setPrecio_total($valorNuevo);
$factura->setCod_f_pago($formaPago);
$cliente->setPuntos($puntos-2);
ManejoCliente::modificarCliente($cliente,$factura);
registrarFactura($codReserva);

$_SESSION["mensaje"]="El valor a pagar es : ".$valorNuevo.", tu ahorro fue de: ".$valorDescuento;
header('Location: indexFuncionario.php');
}

}else if($formaPago==1){

$factura=new Factura();
$factura->setCod_reserva($codReserva);
$factura->setPrecio_total(substr($valorTotal, 1));
$factura->setCod_f_pago($formaPago);

registrarFactura($codReserva,$factura);
$_SESSION["mensaje"]="¡Pago exitoso!";
header('Location: indexFuncionario.php');

}

 function registrarFactura($codReserva,$factura){
$reserva=ManejoReserva::consultarReserva($codReserva);
$reserva->setHora_salida_real(date('Y-m-d H:i:s '));
$reserva->setEstado(4);

ManejoReserva::modificarReserva($reserva);
ManejoFactura::crearFactura($factura);
unset($_SESSION["codReserva"]);
}

//header('Location: indexFuncionario.php');
?>

