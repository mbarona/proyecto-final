<?php
session_start();
if(!isset($_SESSION['email'])){
	header('Location: ../login.php' );
}
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Reserva.php';
date_default_timezone_set("America/Bogota");

$placa=$_POST["placa"];
$accion=$_POST["tipoAccion"];
$codParqueadero=$_SESSION['cod_parqueadero'];
$conexionBD=new Conexion();
$conexion=$conexionBD->conectarBD();

$fechaAbuscar=date('Y-m-d H:i');

ManejoReserva::setConexionBD($conexion);


if($accion==1){

$consultaHoraLLegada=ManejoReserva::consultarReservaDeUnVehiculoEnParqueaderoYFecha(strtoupper($placa),$codParqueadero,$fechaAbuscar);
$codReserva=$consultaHoraLLegada;
if($consultaHoraLLegada==null || $resultConsulta=""){
    $_SESSION["mensaje"]="No se encuentra reservas vigentes para el vehiculo con placa ".strtoupper($placa);
    header('Location: indexFuncionario.php');
}else{
    $reserva=ManejoReserva::consultarReserva($codReserva);
    

    if($reserva->getEstado()==1){
        $estado="Vigente";
    }else if($reserva->getEstado()==2){
        $estado="Extendida";
    }else if($reserva->getEstado()==3){
        $estado="Vencida";
    }else if($reserva->getEstado()==4){
        $estado="Cancelada";
    }else if($reserva->getEstado()==5){
        $estado="Usada";
    }

    $reserva->setHora_ingreso_real(date('Y-m-d H:i:s '));
    $reserva->setEstado(5);
    ManejoReserva::modificarReserva($reserva);


 $_SESSION["mensaje"]= "¡Se ha registrado el ingreso! <br /> Detalle de la reserva <br />"."Placa: ".$reserva->getVehiculo()->getPlaca()."<br /> "."Estado Reserva: ".$estado."<br />".
 "Fecha de ingreso reserva: ".$reserva->gethoraInicial()."<br /> Fecha actual: ".date('Y-m-d H:i:s ');
   header('Location: indexFuncionario.php');
    
}
}else if($accion==2){
$codReservaSalida=ManejoReserva::consultarVehiculoParaSalida($placa,$codParqueadero);
if($codReservaSalida==null ||$codReservaSalida==""){

    $_SESSION["mensaje"]="No se encuentra registro de ingreso para el vehiculo con placa ".$placa;
    header("location: indexFuncionario.php");
}else{
    
$_SESSION["codReserva"]=$codReservaSalida;
header("location: mostrarFactura.php");
}



}else{
    $_SESSION["mensaje"]="Se debe seleccionar ingreso o salida";
    header("location: indexFuncionario.php");
}


?>