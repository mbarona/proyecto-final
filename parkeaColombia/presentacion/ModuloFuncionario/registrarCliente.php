<?php
session_start();

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ClienteDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/TarjetaDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/VehiculoDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoVehiculo.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoTarjeta.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Auditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Cliente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Vehiculo.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Tarjeta.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/mailer/mailer.php';

$tipo=$_SESSION['tipoUsuario'];

$conexionInicial=new Conexion();
$mail=new Mailer();
$conexion=$conexionInicial->conectarBD();
ManejoCliente::setConexionBD($conexion);
ManejoTarjeta::setConexionBD($conexion);
ManejoVehiculo::setConexionBD($conexion);

$nombre=$_POST['nombre'];
$cedula=$_POST['cedula'];
$correo=$_POST['email'];
$placa=$_POST['placa'];
$t_vehiculo=$_POST['tipoVeh'];
$numTarjeta=$_POST['numTarjeta'];
$numTarjeta=str_replace(' ', '', $numTarjeta);
$estado=1;
$puntos=0;
$codivoVerificacion=000;
$fechaVencimiento='2020-10-2';


$clienteBuscar=ManejoCliente::consultarCliente($cedula);


if($clienteBuscar->getNombre()==null || $clienteBuscar->getNombre()==""){
//Genera la contraseña aleatoria
$password=ManejoCliente::contraseniaAleatoria();

if($numTarjeta==null || $numTarjeta==""){
$_SESSION["mensaje"]="Se debe ingresar una tarjeta";
header('Location: formularioCliente.php');
}else{
$tarjeta=ManejoTarjeta::buscarTarjeta($numTarjeta);


$cliente = new Cliente();
$cliente->setNombre($nombre);
$cliente->setEmail($correo);
$cliente->setIdentificacion($cedula);
$cliente->setContrasena($password);
$cliente->setTarjeta($tarjeta);

$vehiculo = new Vehiculo();
$vehiculo->setPlaca($placa);
$vehiculo->setCodCliente($cedula);
$vehiculo->setTipo_vehiculo($t_vehiculo);




ManejoCliente::crearCliente($cliente);
ManejoVehiculo::crearVehiculo($vehiculo);

if(!isset($_SESSION['email'])){
    $auditoria = new Auditoria();
$auditoria->setCedula_usuario($cedula);
$auditoria->setCod_afectado($cedula);
$auditoria->setTabla("CLIENTE");
$auditoria->setTipo_operacion("Registrar cliente");
ManejoAuditoria::setConexionBD($conexion);
ManejoAuditoria::crearAuditoria($auditoria);
unset($_SESSION['numTarjeta']);

}else{
$auditoria = new Auditoria();
$auditoria->setCedula_usuario($_SESSION['identificacion_funcionario']);
$auditoria->setCod_afectado($cedula);
$auditoria->setTabla("CLIENTE");
$auditoria->setTipo_operacion("Registrar cliente");
ManejoAuditoria::setConexionBD($conexion);
ManejoAuditoria::crearAuditoria($auditoria);
unset($_SESSION['numTarjeta']);
}
/**
 * Texto para el mensaje del correo
 */
 $txt = "¡Hola ".$nombre."!, gracias por registrarte en Par-Kea Colombia. <br> Tu contraseña es: ".$password." .  Cuando inicies sesión por primera vez tendrás que cambiarla. <br> Cordialmente, <br> Par-Kea Colombia.";

//Enviar correo electrónico
$subject="Registro Par-kea Colombia";
$mail->enviarCorreo($correo,$txt,$subject);



if(!isset($_SESSION['email'])){
   // $_SESSION["mensaje"]="¡Registro Exitoso!";
    header("location: ../login.php");
}else{
$_SESSION["mensaje"]="¡Registro Exitoso!";
header("location: /proyecto-final/parkeaColombia/presentacion/ModuloFuncionario/indexFuncionario.php");
    }


}
}else{
    $_SESSION["mensaje"]="Ya existe un usuario con ese número de identificación";
    header('Location: formularioCliente.php');
}




 ?>
