<?php
session_start();
if(!isset($_SESSION['email'])){
	header('Location: ../login.php' );
}
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoVehiculo.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Reserva.php';

date_default_timezone_set("America/Bogota");


$cedulaCliente=$_POST["cedulaCliente"];
$codParqueadero=$_POST["codParqueadero"];
$codVehiculo=$_POST["codVehiculo"];
$fechaActual=date('Y-m-d H:i:s');
$fechaHoraFinal= $_POST["fechaFinal"];
$fechayhorafinalReal=null;

$date_for_database = date ("Y-m-d H:i:s", strtotime($fechaHoraFinal));

$conexion=new Conexion();
$conexionBD=$conexion->conectarBD($conexion);

ManejoReserva::setConexionBD($conexionBD);
ManejoVehiculo::setConexionBD($conexionBD);
ManejoParqueadero::setConexionBD($conexionBD);

$vehiculo=ManejoVehiculo::consultarVehiculo($codVehiculo);
$parqueadero=ManejoParqueadero::buscarParqueadero($codParqueadero);

$reserva=new Reserva();

$reserva->setVehiculo($vehiculo); //Solo se pasa el código del vehiculo en este caso, ya que se tiene
$reserva->setParqueadero($parqueadero); //Solo se pasa el código del paraqueadero
$reserva->setEstado(1);//Vg-->vigente
$reserva->sethoraInicial($fechaActual);
$reserva->setHoraFinal($date_for_database);
$reserva->setHora_ingreso_real($fechaActual);
$reserva->setHora_salida_real($fechayhorafinalReal);




ManejoReserva::crearReserva($reserva);


header("location: /proyecto-final/parkeaColombia/presentacion/ModuloFuncionario/indexFuncionario.php");






?>