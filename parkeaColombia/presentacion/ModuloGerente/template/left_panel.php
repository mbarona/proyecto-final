<style>
.navbar .navbar-nav li>a .menu-icon {
    width: 40px;
}
.right-panel {
            background: #f1f2f7;
            margin-left: 250px;
            margin-top: 0px;
}
         
.left-panel { 
        position: relative;
        float:left;
        width:250px; 
        top: 0px;
}
.navbar .navbar-nav>li {
    padding-left: 26px !important;
    padding-right: 26px !important;
}
.navbar .navbar-nav li>a .menu-icon {
    margin-top: 5px !important;
   
}
</style>

      
<nav class="navbar navbar-expand-sm navbar-default">
    <div id="main-menu" class="main-menu collapse navbar-collapse">
        <ul class="nav navbar-nav">
        <li class="activeInicio">
                <a href="?menu=inicio"><i class="menu-icon fa fa-laptop"></i>Inicio</a>
        </li>
        <li class="menu-title">Gestión</li>
        <li class="activeFuncionario">
                <a href="?menu=funcionarios"><i class="menu-icon fa fa-users"></i>Funcionarios</a>
        </li>
        <li class="activeParque">
                <a href="?menu=parqueaderos"><i class="menu-icon fa ti-car"></i>Parqueaderos</a>
        </li>
        <li class="menu-title">Mapas</li>
        <li class="activeMapas1">
                <a href="?menu=mapas"><i class="menu-icon fa fa-map-marker"></i>Mapa parqueaderos</a>
        </li>
    </div>
</nav>
