<?php
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

	$mesesN = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

	$ano=$_POST["ano"];
	$cod_zona=$_POST["cod_zona"];
	
	$obj=new Conexion();
	$conexion=$obj->conectarBD();
	ManejoReserva::setConexionBD($conexion);

	$meses = ManejoReserva::listarMesesPorAnoPorZona($ano,$cod_zona);
	
	echo '<option value="">Seleccione el Mes</option>';
	foreach ($meses as $m){
    echo '<option value="'.$m.'">'.$mesesN[($m)-1].'</option>';
                       }
?>