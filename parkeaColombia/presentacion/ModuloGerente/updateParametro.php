<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Auditoria.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Parametro.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Detalle_horario.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Parqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Dias.php';

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParametro.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoDetalle_horario.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoDias.php';


$conexion=new Conexion();
$conexion=$conexion->conectarBD();


$cod_parametro=$_POST['cod_parametro'];
$cod_parqueadero=$_POST['cod_parqueadero'];
$tarifa =$_POST['tarifa'];
$capacidad=$_POST['cantidad'];
$disponible=$_POST['dis'];
$iden=$_POST['iden'];

$parametro=new Parametro();
$parametro->setCodigo($cod_parametro);
$parametro->setTarifa($tarifa);
$parametro->setCapacidad($capacidad);
$parametro->setCapacidadDisponible($disponible);
$parametro->setCodParqueadero($cod_parqueadero);


ManejoParametro::setConexionBD($conexion);
ManejoParametro::modificarParametro($parametro);


for($i=1;$i<8;$i++){
    ManejoDetalle_horario::setConexionBD($conexion);
    $detalle= new Detalle_Horario();
    $detalle->setCod_parametro($cod_parametro);
    $detalle->setHora_inicio($_POST['entreInicio'.$i]);
    $detalle->setHora_final($_POST['entreFinal'.$i]);
    $detalle->setCod_dia($i);
    ManejoDetalle_horario::modificarDetalle_horario($detalle);
}


$auditoria = new Auditoria();
$auditoria->setCedula_usuario($iden);
$auditoria->setCod_afectado(0);
$auditoria->setTabla("PARAMETRO ");
$auditoria->setTipo_operacion("Editar parametro");
ManejoAuditoria::setConexionBD($conexion);
ManejoAuditoria::crearAuditoria($auditoria);

$redd = true;
if ($redd) {
    $message= 'Se ha modificado de manera satisfactoria';
    echo "<SCRIPT> 
    alert('$message')
    window.location.replace('../gerente.php?menu=parqueaderos');
</SCRIPT>";
}
 ?>