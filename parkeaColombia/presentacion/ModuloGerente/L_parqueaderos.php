<?php
/**    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
*/
	require_once ('../persistencia/util/Conexion.php');
	
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
	require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
    
    $obj=new Conexion();
	$conexion=$obj->conectarBD();
	
	ManejoParqueadero::setConexionBD($conexion);
	$parqueaderos= ManejoParqueadero::parqueaderosEnEstaZona($_SESSION["cod_zona"]);

	$totalParqueaderos= ManejoParqueadero::totalParqueaderosZona($_SESSION['cod_zona']);
	
	ManejoZona::setConexionBD($conexion);

?>
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script>
	function openDialog(){
		$('#modalCreateList').modal('show');
	}
</script>
<?php
if(isset($_POST['updateDetails'])){
	echo 'sumama';
	echo '<script type="text/javascript">',
	'openDialog();',
	'</script>';
}
?>
<style>
.navbar .navbar-nav li.activeParque .menu-icon, .navbar .navbar-nav li:hover .toggle_nav_button:before, .navbar .navbar-nav li .toggle_nav_button.nav-open:before {
    color: #04dd1f;
}
.navbar .navbar-nav>.activeParque>a, .navbar .navbar-nav>.activeParque>a:focus, .navbar .navbar-nav>.activeParque>a:hover {
    color: #04dd1f;
}

	/*  Define the background color for all the ODD background rows  */
	.TFtable tr:nth-child(odd){ 
		
	    background-color: rgba(105, 243, 128, 0.54)!important;
		
	}
	/*  Define the background color for all the EVEN background rows  */
	.TFtable tr:nth-child(even){
		background-color: #e3ffe1!important;

	}
	tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(105, 243, 128, 0.54);
}.table-striped tbody tr:nth-of-type(even) {
    background-color: #e3ffe1!important;
}
button.dt-button, a.dt-button {
    background-image: linear-gradient(to bottom, #5efa6a 0%, #e9e9e9 100%) !important;
	border-radius: 63px !important;
}
.dataTables_length{
    color: #333;
	margin-right:3em !important;
}
div.dt-button-collection {
    top: 0 !important;
	left: 150px !important;
	background-color: rgba(255, 255, 255, 0.8) !important;
}
</style>

<link rel="stylesheet" type="text/css" href="./diseño/admin/css/datatables.min.css"/>
<script src="./diseño/admin/js/datatables.min.js"></script>
<script src="./diseño/admin/js/pdfmake.min.js"></script>
<script src="./diseño/admin/js/vfs_fonts.js"></script>
    
<div class="animated fadeIn">
	<div class="row">
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<div class="card-body">
					<div class="stat-widget-five">
						<div class="stat-icon dib flat-color-5">
							<i class="ti-car"></i>
						</div>
						<div class="stat-content">
							<div class="text-left dib">
								<div class="stat-text"><span class="count"><?php echo $totalParqueaderos; ?></span></div>
								<div class="stat-heading">Total Parqueaderos</div>
							</div>							
						</div>
					</div>
				</div>			
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<strong class="card-title">Lista de Parqueaderos</strong>			
				</div>
				<div class="card-body">
					<table id="example" class="table table-striped table-bordered TFtable">
						<thead>
							<tr style="background-color: rgb(255, 255, 255)!important;">
								<th><center>CODIGO</center></th>
								<th><center>NOMBRE</center></th>
								<th><center>ZONA</center></th>
								<th><center>TIPO DE PARQUEADERO</center></th>
								<th><center>DIRECCION</center></th>
								<th><center>ACCIONES</center></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$salida="";
							if(count($parqueaderos) == 0 ){
								echo '<tr><td colspan="7">No hay datos.</td></tr>';
							}else{
								$no = 1;
								foreach($parqueaderos as $p){
									$zona=ManejoZona::consultarZona($p->getCodZona());
									echo '
										<tr>
											<td><center>'.$p->getCodParqueadero().'</center></td>
											<td><center>'.$p->getNomParqueadero().'</center></td>
											<td><center>'.$zona->getNomZona().'</center></td>';
											if($p->getCodTipoParqueadero()==1){
												echo '<td><center>Cubierto</center></td>';
											}else if($p->getCodTipoParqueadero()==2){
												echo '<td><center>Descubierto</center></td>';
											}else{
												echo '<td><center>Semicubierto</center></td>';
											}
											echo'
											<td><center>'.$p->getDireccion().'</td>
											<td>
												<center>
													<form method="post" action="Gerente.php?menu=editarParametro">
														<input type="hidden" name="cod" value="'.$p->getCodParqueadero().'">
														<button type="submit" name="editar" style="background-color: Transparent;
														border: none;
														cursor:pointer;
														" title="Conf. parametros">
															<i class="menu-icon fa fa-pencil"></i>
														</button>
													</form>
												</center>
											</td>
																					
										</tr>
										';
									$no++;
								}
								$salida.="</tbody><tfoot>
							<tr>
								<th>Codigo</th>
								<th>Nombre</th>
								<th>Zona</th>
								<th>Tipo</th>
								<th>Direccion</th>
								<td></td>
								
							</tr>
						</tfoot>";
							$salida.="</table>";
						
						}
						
						echo $salida;
						?>
						</tbody>
					</table>
				</div>				
			</div>			
		</div>		
	</div>
</div>
<div class="modal fade" id="modalCreateList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form>
				<div class="modal-header">
					<h5 class="modal-title">Editar parametros</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="web">Tarifa:</label>
						<input type="number" class="form-control" id="tarifa" name="tarifa" required>
					</div>
					<div class="form-group">
						<label for="message-text" class="col-form-label">Capacidad:</label>
						<textarea class="form-control" id="capacidad" name="capacidad" required></textarea>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Guardar Cambios</button>
				</div>
			</form>
		</div>
	</div>
</div>



<?php
$path = 'diseño/images/logo.jpg';
$type = pathinfo($path, PATHINFO_EXTENSION);
$data = file_get_contents($path);
$base64 = "'data:image/" . $type . ";base64," . base64_encode($data)."'";
?>
<script>
	$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#example tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Buscar '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#example').DataTable({
		dom: 'lBfrtip',
			buttons: [{
				extend: 'collection',
				text: 'Exportar tabla',
				buttons: [
					{
					extend: 'copyHtml5',
					text: 'Copiar',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					}},
						{
					extend: 'excelHtml5',
					text: 'Excel',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					}},
					{
					extend: 'csvHtml5',
					text: 'CSV',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					}} ,
						{
					extend: 'pdfHtml5',
					text: 'PDF',
					exportOptions: {
						columns: [ 0, 1, 2, 3, 4 ]
					},
					 customize: function(doc) {
						doc.styles.tableHeader.fillColor = 'green';
						
						doc.content.splice( 1, 0, {
							margin: [ 0, 0, 0, 6 ],
							alignment: 'center',
							image: <?php echo $base64;?>,
							width:50
						} );
						doc.content.splice(0, 1, {
								text: [
										   { text: 'Modulo Gerente \n',bold:true,fontSize:15 },
										   { text: 'Lista de Parqueaderos \n',italics:true,fontSize:12 },
										   { text: 'Par-Kea Colombia',italics:true,fontSize:9 }
	
								],
								margin: [0, 0, 0, 12],
								alignment: 'center'
						});
						doc['footer'] = (function(page, pages) {
                            return {
                            columns: [
                                {
                                alignment: 'center',
                                text: [
                                    { text: page.toString(), italics: true },
                                    ' of ',
                                    { text: pages.toString(), italics: true }
                                ]
                            }],
                            margin: [10, 0]
                            }
                        });
					 },
				title:'Parqueaderos'
				},
				'print'
			   
				]}], initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
                var that = this;
 
                $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                        that
                            .search( this.value )
                            .draw();
                    }
                } );
            } );
			
        },
		"language": idioma_espanol
    }); 
} );
var idioma_espanol={
    "sProcessing":     "Procesando...",
    "sLengthMenu":     "Mostrar _MENU_ registros",
    "sZeroRecords":    "No se encontraron resultados",
    "sEmptyTable":     "Ningún dato disponible en esta tabla",
    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
    "sInfoPostFix":    "",
    "sSearch":         "Buscar:",
    "sUrl":            "",
    "sInfoThousands":  ",",
    "sLoadingRecords": "Cargando...",
    "oPaginate": {
        "sFirst":    "Primero",
        "sLast":     "Último",
        "sNext":     "Siguiente",
        "sPrevious": "Anterior"
    },
    "oAria": {
        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
    },
    "buttons": {
        "copy": "Copiar",
        "colvis": "Visibilidad"
    }
};
</script>



