<?php
/**    require_once $_SERVER["DOCUMENT_ROOT"].'/Proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
*/
  require_once ('../persistencia/util/Conexion.php');
  
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
  require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';

  $obj=new Conexion();
  $conexion=$obj->conectarBD();

  ManejoParqueadero::setConexionBD($conexion);
  ManejoZona::setConexionBD($conexion);
  
  $totalParqueaderos= ManejoParqueadero::totalParqueaderos();
  
  $parzona = ManejoParqueadero::parqueaderosEnEstaZona($_SESSION['cod_zona']);
  $totalz=count($parzona);
  $zona = ManejoZona::consultarZona(2);
  
 ?>

    <style>
        .navbar .navbar-nav li.activeMapas1 .menu-icon,
        .navbar .navbar-nav li:hover .toggle_nav_button:before,
        .navbar .navbar-nav li .toggle_nav_button.nav-open:before {
            color: #04dd1f;
        }
        
        .navbar .navbar-nav>.activeMapas1>a,
        .navbar .navbar-nav>.activeMapas1>a:focus,
        .navbar .navbar-nav>.activeMapas1>a:hover {
            color: #04dd1f;
        }
        
        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
        
        .mapas {
            height: 100%;
            position: absolute;
            top: 0;
            left: 0%;
            bottom: 0;
            width: 100%;
        }
        /* Optional: Makes the sample page fill the window. */
    </style>
   
   
   <div class="col-lg-11">
            <div class="card-header" style="background-color: rgb(127, 255, 119);">
                <h4>Parqueaderos en <?php echo $zona->getNomZona()?></h4>
                <h5>Total parqueaderos: <?php echo $totalz?></h5>
        
        </div>
    <br>
    <div class="row">
    </div>
    <div class="row">
        <?php
               echo '
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header" style="background-color: rgb(189, 255, 222);">
                            <strong></strong><small>Zona: '.$zona->getNomZona().'</small>
                            </div>
                            <div class="card-body card-block" style="padding: 15em;">
                <div id="map" class="mapas"></div>
                </div>
                </div>
            </div>';
        ?>
    </div>
 </div>
    <script>
        var map2;

        function initMap() {
            <?php
			if(count($parzona) == 0  ){
              
              }else{
               ?>
                map2 = new google.maps.Map(document.getElementById("map"), {
                center: {
                    lat: <?php echo $parzona[0]->getLatitud(); ?>,
                    lng: <?php echo $parzona[0]->getLongitud(); ?>
                },
                zoom: 15
            });

            var marker;
            <?php
				foreach($parzona as $p){
              ?>
            var marker = new google.maps.Marker({
                position: {
                    lat: <?php echo $p->getLatitud(); ?>,
                    lng: <?php echo $p->getLongitud(); ?>
                },
                map: map2,
                title: <?php echo json_encode($p->getDireccion().' -- '.$p->getNomParqueadero()); ?>
            });

            <?php
              	}	
            }
              ?>

        }
    </script>