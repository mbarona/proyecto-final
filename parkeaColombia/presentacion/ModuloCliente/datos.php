<?php
session_start();
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoVehiculo.php';
if($_SESSION['email']==null){
    header("Location: /proyecto-final/parkeaColombia/presentacion/login.php");
  }
    
$conexion=new Conexion();
$conexionBD=$conexion->conectarBD();

ManejoCliente::setConexionBD($conexionBD);
ManejoVehiculo::setConexionBD($conexionBD);

$codCliente=$_SESSION["CodCliente"];
$cliente=ManejoCliente::consultarCliente($codCliente);
$vehiculos=ManejoVehiculo::consultarVehiculoPorCliente($codCliente);

$num_tarjeta=$cliente->getTarjeta(); //Se debe actualizar el dao, retorna el codigo más no la tarjeta
$ultimos4=substr($num_tarjeta,-4);
$iden=$cliente->getIdentificacion();
$nombre=$cliente->getNombre();
$email=$cliente->getEMail();
?>

<!DOCTYPE html>
  <html lang="es">
  <head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../diseño/Funcionario/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->


    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Mis Datos</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
      <!--
      CSS
      ============================================= -->
      <link rel="stylesheet" href="../diseño/Funcionario/css/linearicons.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/font-awesome.min.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/bootstrap.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/magnific-popup.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/nice-select.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/animate.min.css">
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/owl.carousel.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/main.css">
      <link rel="stylesheet" type="text/css" href="../diseño/Cliente2/css/stylestarjeta.css">
        <link rel="stylesheet" type="text/css" href="../diseño/Cliente2/css/demotarjeta.css">
        <style type="text/css">
      #mapa {
            height: 50vh;
      }
      .gmnoprint,
      .gm-fullscreen-control {
        zoom: 0.75;
        .gmnoprint {
          zoom: 1;
        }
      }

    </style>
    </head>
    <body background="diseño/images/Fondos/1.jpg" style="background-color:#b2db71; background-size:60% 80%; background-repeat: no-repeat; background-position: center 0;" >

      <!-- start banner Area -->
    <section class=" relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
          <div class="row d-flex align-items-center justify-content-center header-right">
            <div class="banner-content col-lg-4 col-md-4 ">

              <img src="../diseño/Funcionario/img/logos/img02.png" width="200" height="210" alt="" title="" />
                <br><br><br>
              <h5 class="text-white text-uppercase">
                Bienvenido, <?php echo $nombre; ?>
              </h5><br>
              <div class="form-group">
              </div>
            
            </div>
            <div class="col-lg-5  col-md-4 ">
              <h4 class="text-white pb-10"> <center> Mis Datos </center> </h4>
              <form class="form" role="form" autocomplete="off" id="formRegistroCliente" method="post" action="guardarDatos.php">
                <div class="from-group">
                   <h4 class="text-white pb-10">Cedula</h4>
                  <input class="form-control txt-field" type="number" name="cedula" placeholder="" required="true" readonly value="<?php echo $iden ?>" />
                  <h4 class="text-white pb-10">Nombre</h4>
                  <input class="form-control txt-field" type="text" name="nombre" placeholder="Nombre Completo" required="true" value="<?php echo $nombre ?>" />
                  <h4 class="text-white pb-10">Correo</h4>
                  <input class="form-control txt-field" type="email" name="email" placeholder="Correo Electronico" required="true" value="<?php echo $email ?>" />
                  <h4 class="text-white pb-10">Tarjeta</h4>
                  <div class="form-group">
                    <div class="input-group">
                    <input class="form-control txt-field" type="text" name="numTarjeta" placeholder="Tarjeta" required="true" value="<?php echo '**** **** **** '.$ultimos4;?>" readonly>
                    <span class="input-group-addon">
                        <?php if (substr($num_tarjeta, 0, 1)==4 && strlen($num_tarjeta)==16) {
                          echo '<img src="../images/visa.jpg" id="visa" for="numTarjeta" height="40vh">';
                        }elseif (substr($num_tarjeta, 0, 1) ==3 && strlen($num_tarjeta)==15 ) {
                          echo '<img src="../images/amex.jpg" id="amex" height="40vh">';
                        }elseif (substr($num_tarjeta, 0, 1) ==5 &&strlen($num_tarjeta)==16) {
                          echo '<img src="../images/mastercard.jpg" id="mastercard" height="40vh">';
                        } ?>
                    </span>
                
                  </div>
                </div>
                  
                  <a href="agregarTarjeta.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Cambiar Tarjeta</a><br>
          <h4 class="text-white pb-10">Mis Autos</h4>
          <table id="example" class="table table-striped table-bordered TFtable">
        <a href="agregarVehiculo.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Agregar Vehiculo</a><br>
            <thead>
              <tr style="background-color: rgb(255, 255, 255)!important;">
                <th><center>#</center></th>
                <th><center>PLACA</center></th>
                <th><center>TIPO VEHICULO</center></th>
              </tr>
            </thead>
            <tbody class="text-white">
              <?php
              $salida="";
                $no = 1;
                foreach($vehiculos as $v){
                  echo '
                    <tr>
                      <td><center>'.$no.'</center></td>
                      <td><center>'.$v->getPlaca().'</center></td>
                      <td><center>'.$v->getTipo_vehiculo().'</center></td>   
                    </tr>';
                  $no++;
                }
                $salida.="</tbody><tfoot>
              
            </tfoot>";
              $salida.="</table>";

            echo $salida;
              ?>
            </tbody>
          </table>
                </div>
              <center>
                  <div class="form-group row">
                      <div class="col-md-12">
                          <button type="submit" class="btn btn-default btn-lg btn-block text-center text-uppercase">Guardar</button>
                          <a href="index.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Regresar</a>
                      </div>
                  </div>
                </center>
              </form>
            </div>
          </div>
          
        </div>
      </section>
      <!-- End banner Area -->


      <!-- start footer Area -->
      <footer class="footer-area section-gap">
        <div class="container">
          <div class="row">

            <p class="mt-50 mx-auto footer-text col-lg-12">
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="#">SoftUp Enterprise</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
        </div>
      </footer>
      <!-- End footer Area -->


      <script src="../diseño/Funcionario/js/vendor/jquery-2.2.4.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="../diseño/Funcionario/js/vendor/bootstrap.min.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="../diseño/Funcionario/js/easing.min.js"></script>
      <script src="../diseño/Funcionario/js/hoverIntent.js"></script>
      <script src="../diseño/Funcionario/js/superfish.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.ajaxchimp.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.magnific-popup.min.js"></script>
      <script src="../diseño/Funcionario/js/owl.carousel.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.sticky.js"></script>
      <script src="../diseño/Funcionario/js/jquery.nice-select.min.js"></script>
      <script src="../diseño/Funcionario/js/waypoints.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.counterup.min.js"></script>
      <script src="../diseño/Funcionario/js/parallax.min.js"></script>
      <script src="../diseño/Funcionario/js/mail-script.js"></script>
      <script src="../diseño/Funcionario/js/main.js"></script>
      
    </body>
</html>
