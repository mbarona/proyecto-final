<?php
session_start();
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoT_Parqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCiudad.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';


$obj=new Conexion();
$conexion=$obj->conectarBD();

ManejoT_Parqueadero::setConexionBD($conexion);
$tipos=ManejoT_Parqueadero::listarT_Parqueaderos();

ManejoCiudad::setConexionBD($conexion);
$ciudades=ManejoCiudad::listarCiudadesConZona();

ManejoZona::setConexionBD($conexion);

?>

  <html lang="zxx" class="no-js">
  <head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../diseño/Funcionario/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Menu Usuario</title>
 
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
      <!--
      CSS
      ============================================= -->
      <link rel="stylesheet" href="../diseño/Funcionario/css/linearicons.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/font-awesome.min.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/bootstrap.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/magnific-popup.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/nice-select.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/animate.min.css">
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/owl.carousel.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/main.css">
      <style type="text/css">
      #mapa {
                  height: 70vh;
            }
            .gmnoprint,
      .gm-fullscreen-control {
        zoom: 0.75;
        .gmnoprint {
          zoom: 1;
        }
      }

      </style>
    </head>
    <body background="diseño/images/Fondos/1.jpg" style="background-color:#b2db71; background-size:60% 80%; background-repeat: no-repeat; background-position: center 0;" >
       <?php
        
        if($_SESSION['email']==null){
        header("Location: /proyecto-final/parkeaColombia/presentacion/login.php");
      }
      ?>

      <div id="myModal" class="modal fade"  role="dialog">
  <div class="modal-dialog modal-dialog-centered">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 ></h4>
      </div>
      <div class="modal-body">
       <center> <p><?php echo $_SESSION["mensaje"] ?> </p> </center>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div>


      <!-- start banner Area -->
      <section class="relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
          <div class="row d-flex align-items-center justify-content-center header-right">

            <div class="banner-content col-lg-4 col-md-4">

              <center><img src="../diseño/Funcionario/img/logos/img02.png" width="200" height="210" alt="" title="" />
                <br><br><br>
              <h5 class="text-white text-uppercase">
                Bienvenido, <?php echo $_SESSION['NomCliente']; ?>
              </h5>
              </p>
              <div class="form-group">


              <a href="datos.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Mis Datos <i class="fa fa-pencil"></i></a></p>

              <a href="misReservas.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Mis Reservas   <i class="fa fa-archive"></i></a></p>
              <a href="misFacturas.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Mis Facturas <i class="fa fa-list-alt"></i></a></p>
              <a href="../logout.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Cerrar Sesión <i class="fa fa-sign-out"></i></a>
              </div>

            </center>
            </div>
            <div class="col-lg-8  col-md-6">



                <center>
                  <b>
                  <h5  class="pb-10 text-white">Filtro De Busqueda Parqueadero</h5>
                </b>
                </center>
                
                <form class="" action="index.php" method="post">

                  <input id="searchInput" class="form-control input" type="text" placeholder="Ingrese una dirección">
                    <br>
                  <div class="row">
                    
                    <div class="col-lg-4">
                        <select id="idTparqueadero" name="idTparqueadero" class="form-control" onchange="filterMarkersTipo(this.value);">
                            <option value="" >Tipo de Parqueadero </option>
                                <?php
                                      foreach ($tipos as $c) {
                                          echo'<option value="'.$c->getCodTParqueadero().'">'.$c->getNomTParqueadero().'</option>';
                                      }
                                ?>
                        </select>
                    </div>


                    <div class="col-lg-4">
                      <select id="estadoP" name="estadoP"  class="form-control" onchange="filterMarkersState(this.value);">
                          <option value="" >Tarifa X Hora </option>
                            <option value="0" > Menor $5.000</option>
                            <option value="1" > Entre $5.000 y $10.000</option>
                            <option value="2" > Mayor $10.000 </option>
                      </select>
                    </div>
                 
                 <!--
                    <div class="col-lg-4">
                      <select  id="estadoP" name="estadoP" class="form-control" onchange="filterMarkersState(this.value);">
                          <option value="">Estado </option>
                          <option value="0" > Abierto </option>
                          <option value="1" > Cerrado </option>
                      </select>
                    </div>
                    -->
                      <div class="col-lg-4">
                          <section class="card">
                            <select id="idciudad" name="idciudad" class="form-control" onchange="filterMarkersCity(this.value);">
                                <option value="" >Seleccione la ciudad</option>
                                    <?php
                                          foreach ($ciudades as $c) {
                                              echo'<option value="'.$c->getCodCiudad().'">'.$c->getNomCiudad().'</option>';

                                          }
                                    ?>
                            </select>
                          </section>
                      </div>


              </div>
              </form>

              
              <div id="mapa"></div>
            </div>
          </div>
          </div>
        </div>
      </section>
      <!-- End banner Area -->


      <!-- start footer Area -->
      <footer class="footer-area section-gap">
        <div class="container">
          <div class="row">

            <p class="mt-50 mx-auto footer-text col-lg-12">
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="#">SoftUp Enterprise</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
        </div>
      </footer>
      <!-- End footer Area -->

      <script src="../diseño/Funcionario/js/vendor/jquery-2.2.4.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="../diseño/Funcionario/js/vendor/bootstrap.min.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="diseño/Funcionario/js/easing.min.js"></script>
      <script src="../diseño/Funcionario/js/hoverIntent.js"></script>
      <script src="../diseño/Funcionario/js/superfish.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.ajaxchimp.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.magnific-popup.min.js"></script>
      <script src="../diseño/Funcionario/js/owl.carousel.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.sticky.js"></script>
      <script src="../diseño/Funcionario/js/jquery.nice-select.min.js"></script>
      <script src="../diseño/Funcionario/js/waypoints.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.counterup.min.js"></script>
      <script src="../diseño/Funcionario/js/parallax.min.js"></script>
      <script src="../diseño/Funcionario/js/mail-script.js"></script>
      <script src="../diseño/Funcionario/js/main.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA4LpI1kvBXluzzvoSQ1w-VYe9VHxDyjVY&libraries=places&callback=initMap"></script>

      <script>
        var gmarkers1 = [];
        var marcadoresAmostrar = [];
        var map;
          function initMap() {

            var geocoder = new google.maps.Geocoder();
            var bounds = new google.maps.LatLngBounds();
            var mapOptions = {
                streetViewControl: false,
                mapTypeId: 'roadmap',
            };

            map = new google.maps.Map(document.getElementById('mapa'), {
                mapOptions,
                center: {lat: 4.686918, lng: -74.057296},
            });
            var input = document.getElementById('searchInput');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.bindTo('bounds', map);
            autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(20);
        }
        });

            if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };

            map.setCenter(pos);
          });
          } else {
          map.setCenter(center);
          }

            map.setTilt(50);
             // Crear múltiples marcadores desde la Base de Datos
            marcadores = [
                <?php require('marcadores.php'); ?> 
            ];
            // Creamos la ventana de información para cada Marcador
            ventanaInfo = [
                <?php require('info_marcadores.php'); ?>
            ];

            // Creamos la ventana de información con los marcadores
            var mostrarMarcadores = new google.maps.InfoWindow(),
                marcadores, i;

            // Colocamos los marcadores en el Mapa de Google
            for (i = 0; i < marcadores.length; i++) {
                var position = new google.maps.LatLng(marcadores[i][2], marcadores[i][1]);
                bounds.extend(position);
                marker = new google.maps.Marker({
                    position: position,
                    map: map,
                    icon: {url:'../diseño/images/marcador.png', scaledSize: new google.maps.Size(70, 50)},
                    title: marcadores[i][0],
                    category: marcadores[i][3],
                    city: marcadores[i][4],
                    state: marcadores[i][5],
                });
                gmarkers1.push(marker);
                // Colocamos la ventana de información a cada Marcador del Mapa de Google
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        mostrarMarcadores.setContent(ventanaInfo[i][0]);
                        mostrarMarcadores.open(map, marker);
                    }
                })(marker, i));

                // Centramos el Mapa de Google para que todos los marcadores se puedan ver

            }

            // Aplicamos el evento 'bounds_changed' que detecta cambios en la ventana del Mapa de Google, también le configramos un zoom de 14
            var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
                this.setZoom(14);
                google.maps.event.removeListener(boundsListener);
            });
        }

        // Lanzamos la función 'initMap' para que muestre el Mapa con Los Marcadores y toda la configuración realizada
        google.maps.event.addDomListener(window, 'load', initMap);


        /**
         * Function to filter markers by category
         */
        function filterMarkersTipo(category) {
            for (i = 0; i < gmarkers1.length; i++) {
                marker1 = gmarkers1[i];
                // If is same category or category not picked
                if (marker1.category == category || category.length === 0) {
                    marker1.setMap(map);
                }
                // Categories don't match 
                else {
                    marker1.setMap(null);
                }
            }
        }


        /**
         * Function to filter markers by city
         */
        function filterMarkersCity(city) {
            for (i = 0; i < gmarkers1.length; i++) {
                marker1 = gmarkers1[i];
                // If is same category or category not picked
                if (marker1.city == city || city.length === 0) {
                    marker1.setMap(map);
                }
                // Categories don't match 
                else {
                    marker1.setMap(null);
                }
            }
        }

        /**
         * Function to filter markers by state
         */
        function filterMarkersState(state) {
            for (i = 0; i < gmarkers1.length; i++) {
                marker1 = gmarkers1[i];
                // If is same category or category not picked
                if (marker1.state == state || state.length === 0) {
                    marker1.setMap(map);
                }
                // Categories don't match 
                else {
                    marker1.setMap(null);
                }
            }
        }

        $('#idTparqueadero').change(function(){
        $('#estadoP').prop('selectedIndex',0);
        $('#idciudad').prop('selectedIndex',0);
      });


     $('#idciudad').change(function(){
        $('#estadoP').prop('selectedIndex',0);
        $('#idTparqueadero').prop('selectedIndex',0);
      });

     $('#estadoP').change(function(){
        $('#idTparqueadero').prop('selectedIndex',0);
        $('#idciudad').prop('selectedIndex',0);
      });

      </script>
    </body>
  </html>
<?php
if(isset($_SESSION["mensaje"])){

  echo '<script language="JavaScript">';
echo "$('#myModal').modal('show')";
echo '</script>';
}
unset($_SESSION["mensaje"]);

?>
