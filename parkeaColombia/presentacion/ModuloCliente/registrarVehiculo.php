<?php
session_start();
if($_SESSION['email']==null){
        header("Location: /proyecto-final/parkeaColombia/presentacion/login.php");
      }
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/VehiculoDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoVehiculo.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Vehiculo.php';

$conexion=new Conexion();
$conexionBD=$conexion->conectarBD();
ManejoVehiculo::setConexionBD($conexionBD);

$cedula=$_SESSION['CodCliente'];
$placa=$_POST['placa'];
$t_vehiculo=$_POST['tipVeh'];

$vehiculo = new Vehiculo();
$vehiculo->setPlaca($placa);
$vehiculo->setCodCliente($cedula);
$vehiculo->setTipo_vehiculo($t_vehiculo);

ManejoVehiculo::crearVehiculo($vehiculo);
header("location: /proyecto-final/parkeaColombia/presentacion/ModuloCliente/datos.php");
?>