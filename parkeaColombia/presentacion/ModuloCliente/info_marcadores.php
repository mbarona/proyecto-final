<?php

  // Agregamos el nombre y dirección a la ventana de información de cada marcador, puedes agregar la información que desees, nosotros agregaremos 'nombre' y 'direccion' 

  if(count($parqueaderos)>0){
    
    foreach($parqueaderos as $par){ 
		if($par->getCodTipoParqueadero()==1){
			$nomPar="Cubierto";
		}elseif($par->getCodTipoParqueadero()==2){
			$nomPar="Descubierto";
		}elseif($par->getCodTipoParqueadero()==3){
			$nomPar="Semicubierto";
		}
	$zona=ManejoZona::consultarZona($par->getCodZona());
	$parametro=ManejoParametro::listarParametroDeParqueadero($par->getCodParqueadero());
	$precio=$parametro->getTarifa();
	$capacidad=$parametro->getCapacidad();
	$capacidadD=$parametro->getCapacidadDisponible();
    	?>

    ['<div class="info_content">' + '<h3><?php echo $par->getNomParqueadero(); ?></h3>' + '<p><?php echo $par->getDireccion(); ?></p>'+ '<p><?php echo 'Tipo: '.$nomPar; ?></p>' + '<p><?php echo 'Zona: '.$zona->getNomZona(); ?></p>'+ '<p><?php echo 'Tarifa: $'.$precio; ?></p>' + '<p><?php echo 'Capacidad: '.$capacidadD.'/'.$capacidad; ?></p>' + '<a href="formularioReserva.php?codigo=<?php echo $par->getCodParqueadero() ?>"><button type="submit"  onclick="" class="btn btn-default btn-lg btn-block text-center text-uppercase">Reservar</button></a>' + '</div>'], 

    <?php }
  }
 
?>