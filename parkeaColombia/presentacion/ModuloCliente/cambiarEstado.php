<?php
session_start();
if($_SESSION['email']==null){
        header("Location: /proyecto-final/parkeaColombia/presentacion/login.php");
      }
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ReservaDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Reserva.php';

$conexionInicial=new Conexion();
$conexion=$conexionInicial->conectarBD();
ManejoReserva::setConexionBD($conexion);

$placa=$_POST['placaVeh'];
$estado=4;

ManejoReserva::modificarEstado($estado,$placa);

header("location: /proyecto-final/parkeaColombia/presentacion/ModuloCliente/misReservas.php");

?>