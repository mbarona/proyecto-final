<?php
      session_start();
      if($_SESSION['email']==null){
        header("Location: /proyecto-final/parkeaColombia/presentacion/login.php");
      }
      require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
      require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/FacturaDAO.php';
      require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Factura.php';
      require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoFactura.php';
      require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
      require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
      $con= new Conexion();
      $conexion=$con->conectarBD();
      if($conexion->connect_error){
        die("Problema de conexión con la base de datos: ".$conexion->connect_error);
      }
      ManejoFactura::setConexionBD($conexion);
      ManejoReserva::setConexionBD($conexion);
      ManejoParqueadero::setConexionBD($conexion);

      $cedulaCliente=$_SESSION['CodCliente'];
      $facturas= ManejoFactura::consultarFacturasCliente($cedulaCliente);

    ?>
<!DOCTYPE html>
  <html lang="zxx" class="no-js">
  <head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="../diseño/Funcionario/img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="codepixer">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Inicio de Sesión </title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
      <!--
      CSS
      ============================================= -->
      <link rel="stylesheet" href="../diseño/Funcionario/css/linearicons.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/font-awesome.min.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/bootstrap.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/magnific-popup.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/nice-select.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/animate.min.css">
      <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/owl.carousel.css">
      <link rel="stylesheet" href="../diseño/Funcionario/css/main.css">
      <style type="text/css">
     .TFtable tr:nth-child(odd){

      background-color: rgba(105, 243, 128, 0.54)!important;

  }
  /*  Define the background color for all the EVEN background rows  */
  .TFtable tr:nth-child(even){
    background-color: #e3ffe1!important;

  }
  tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }

.table-striped tbody tr:nth-of-type(odd) {
    background-color: rgba(105, 243, 128, 0.54);
}.table-striped tbody tr:nth-of-type(even) {
    background-color: #e3ffe1!important;
}
button.dt-button, a.dt-button {
    background-image: linear-gradient(to bottom, #5efa6a 0%, #e9e9e9 100%) !important;
  border-radius: 63px !important;
}
.dataTables_length{
    color: #333;
  margin-right:3em !important;
}

      </style>
    </head>
    <body background="diseño/images/Fondos/1.jpg" style="background-color:#b2db71; background-size:60% 80%; background-repeat: no-repeat; background-position: center 0;" >

      <!-- start banner Area -->
      <section class="relative" id="home">
        <div class="overlay overlay-bg"></div>
        <div class="container">
          <div class="row d-flex align-items-center justify-content-center  header-right">

            <div class="banner-content col-lg-4 col-md-4">

              <center><img src="../diseño/Funcionario/img/logos/img02.png" width="200" height="210" alt="" title="" />
                <br><br><br>
              <h5 class="text-white text-uppercase">
                Bienvenido, <?php echo $_SESSION['NomCliente']; ?>
              </h5>
              </p>
              <div class="form-group">
              <a href="index.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Regresar</a></p>
              <a href="../logout.php" class="btn btn-default btn-lg btn-block text-center text-uppercase">Cerrar Sesión</a>
              </div>
            </center>
            </div>
            <div class="col-lg-8  col-md-6">
              <h4 class="text-white pb-30">Mis Facturas</h4>
                  <table id="example" class="table table-striped table-bordered TFtable">
                <thead>
                  <tr style="background-color: rgb(255, 255, 255)!important;">
                    <th><center>PARQUEADERO</center></th>
                    <th><center>PRECIO TOTAL</center></th>
                    <th><center>FORMA DE PAGO</center></th>
                  </tr>
                </thead>
                <tbody class="text-black">
                  <?php
                  $salida="";
                  if(count($facturas) == 0 ){
                    echo '<tr><td colspan="7">No hay datos.</td></tr>';
                  }else{
                    $no = 1;
                    foreach($facturas as $f){
                      $reserva= ManejoReserva::consultarReserva($f->getCod_reserva());
                      $parqueadero= ManejoParqueadero::buscarParqueadero($reserva->getParqueadero()->getCodParqueadero());
                      echo '
                        <tr> 
                          <td><center>'.$parqueadero->getNomParqueadero().'</center></td>
                          <td><center>'.$f->getPrecio_total().'</center></td>';
                          if($f->getCod_f_pago()==1){
                          echo '<td><center>Efectivo</center></td>';
                          }elseif($f->getCod_f_pago()==2){
                            echo '<td><center>Tarjeta</center></td>';
                          }elseif($f->getCod_f_pago()==3){
                            echo '<td><center>Puntos</center></td>';
                          }
                          echo'
                          
                          
                        </tr>';
                      $no++;
                    }
                    $salida.="</tbody><tfoot>
                  
                </tfoot>";
                  $salida.="</table>";

                }

                echo $salida;
                  ?>
                </tbody>
              </table>
            </div>
          </div>
          </div>
        </div>
      </section>
      <!-- End banner Area -->


      <!-- start footer Area -->
      <footer class="footer-area section-gap">
        <div class="container">
          <div class="row">

            <p class="mt-50 mx-auto footer-text col-lg-12">
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Powered by <a href="#">SoftUp Enterprise</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
        </div>
      </footer>
      <!-- End footer Area -->

      <script src="../diseño/Funcionario/js/vendor/jquery-2.2.4.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="../diseño/Funcionario/js/vendor/bootstrap.min.js"></script>
      <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="diseño/Funcionario/js/easing.min.js"></script>
      <script src="../diseño/Funcionario/js/hoverIntent.js"></script>
      <script src="../diseño/Funcionario/js/superfish.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.ajaxchimp.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.magnific-popup.min.js"></script>
      <script src="../diseño/Funcionario/js/owl.carousel.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.sticky.js"></script>
      <script src="../diseño/Funcionario/js/jquery.nice-select.min.js"></script>
      <script src="../diseño/Funcionario/js/waypoints.min.js"></script>
      <script src="../diseño/Funcionario/js/jquery.counterup.min.js"></script>
      <script src="../diseño/Funcionario/js/parallax.min.js"></script>
      <script src="../diseño/Funcionario/js/mail-script.js"></script>
      <script src="../diseño/Funcionario/js/main.js"></script>
      
    </body>
</html>