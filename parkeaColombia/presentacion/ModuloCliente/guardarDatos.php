<?php
session_start();
if($_SESSION['email']==null){
        header("Location: /proyecto-final/parkeaColombia/presentacion/login.php");
      }
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ClienteDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoAuditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Auditoria.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Cliente.php';

$conexionInicial=new Conexion();
$conexion=$conexionInicial->conectarBD();
ManejoCliente::setConexionBD($conexion);

$nombre=$_POST['nombre'];
$cedula=$_POST['cedula'];
$correo=$_POST['email'];


$cliente = new Cliente();
$cliente->setNombre($nombre);
$cliente->setEmail($correo);
$cliente->setIdentificacion($cedula);

ManejoCliente::modificarDatos($cliente);

$auditoria = new Auditoria();
$auditoria->setCedula_usuario($cedula);
$auditoria->setCod_afectado($cedula);
$auditoria->setTabla("CLIENTE");
$auditoria->setTipo_operacion("Modificacion");
ManejoAuditoria::setConexionBD($conexion);
ManejoAuditoria::crearAuditoria($auditoria);

header("location: /proyecto-final/parkeaColombia/presentacion/ModuloCliente/datos.php");

?>