<?php
session_start();
if(isset($_SESSION['email'])){
$tipo=$_SESSION['tipoUsuario']; 

if ($tipo==1) {
    $linkRegresar="datos.php";
}
elseif ($tipo==2) {
    $linkRegresar="../ModuloFuncionario/formularioCliente.php";
}
}else{
    
    $linkRegresar="../ModuloFuncionario/formularioCliente.php";
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    
    <title>Agregar Tarjeta</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../diseño/Cliente2/css/stylestarjeta.css">
    <link rel="stylesheet" type="text/css" href="../diseño/Cliente2/css/demotarjeta.css">
    

    <style>
        input {text-transform: uppercase;}
    </style>
</head>

<body background="diseño/images/Fondos/1.jpg" style="background-color:#b2db71; background-size:60% 80%; background-repeat: no-repeat; background-position: center 0;">
    <div class="container-fluid fullscreen">

        <div class="creditCardForm">
            <div class="heading">
                <h1>Agregar Tarjeta</h1> 
            </div>
            <div class="payment">
                <form id="formA" name="formA"  action="ingresarTarjeta.php" method="POST" class="form" role="form" autocomplete="on">
                    <div class="form-group owner">
                        <label for="owner">Nombre Titular</label>
                        <!--<input type="text" class="form-control" class="uppercase" id="nom_titular">
                        --><input type="text" class="form-control" id="owner"name="owner1" rows="2" cols="60">
                    </div>
                    <div class="form-group CVV">
                        <label for="cvv">CVV</label>
                        <input type="text" class="form-control" id="cvv" name="codSeg" required="true">
                    </div>
                    <div class="form-group" id="card-number-field">
                        <label for="cardNumber">N&uacutemero de Tarjeta</label>
                        <input type="text" class="form-control" placeholder="XXXX-XXXX-XXXX-XXXX" id="cardNumber" required="true" name="numTar">
                    </div>
                    <div class="form-group" id="expiration-date" name="expiration_date1">
                        <label>Fecha de Vencimiento</label>
                        <select name="fecha_ven_mes">
                            <option value="01">Enero</option>
                            <option value="02">Febrero </option>
                            <option value="03">Marzo</option>
                            <option value="04">Abril</option>
                            <option value="05">Mayo</option>
                            <option value="06">Junio</option>
                            <option value="07">Julio</option>
                            <option value="08">Agosto</option>
                            <option value="09">Septiembre</option>
                            <option value="10">Octubre</option>
                            <option value="11">Noviembre</option>
                            <option value="12">Diciembre</option>
                        </select>
                        <select name="fecha_ven_year">
                            <option value="2020"> 2020</option>
                            <option value="2021"> 2021</option>
                            <option value="2022"> 2022</option>
                            <option value="2023"> 2023</option>
                            <option value="2024"> 2024</option>
                            <option value="2025"> 2025</option>
                            <option value="2026"> 2026</option>
                        </select>
                    </div>
                    <div class="form-group" id="credit_cards">
                        <img src="../images/visa.jpg" id="visa">
                        <img src="../images/mastercard.jpg" id="mastercard">
                        <img src="../images/amex.jpg" id="amex">
                    </div>
                    <div class="form-group" id="pay-now">
                        <button type="submit"  class="btn btn-default btn-block text-center text-uppercase" id="confirm-purchase">Registrar</button>

                    <form class="" action="miPerfil.php" method="post">
                      <div class="form-group" id="pay-now">
                          
                        <type="button" onclick="location='<?php echo $linkRegresar ?>'" ALIGN=RIGHT  id="confirm-purchase"
                           class="btn btn-default btn-block text-center text-uppercase" href name="Regresar">
                            Regresar
                            <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                    </form>
                </form>

            </div>
        </div>


    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="../diseño/js/jquery.payform.min.js" charset="utf-8"></script>
    <script src="../diseño/js/script.js"></script>
</body>

</html>