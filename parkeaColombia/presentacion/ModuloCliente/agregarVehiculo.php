<?php
session_start();
if($_SESSION['email']==null){
        header("Location: /proyecto-final/parkeaColombia/presentacion/login.php");
}
$tipo=$_SESSION['tipoUsuario']; 

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">

    <title>Agregar Tarjeta</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../diseño/Cliente2/css/stylestarjeta.css">
    <link rel="stylesheet" type="text/css" href="../diseño/Cliente2/css/demotarjeta.css">
    

    <style>
        input {text-transform: uppercase;}
    </style>
</head>

<body background="diseño/images/Fondos/1.jpg" style="background-color:#b2db71; background-size:60% 80%; background-repeat: no-repeat; background-position: center 0;">
    <div class="container-fluid">

        <div class="creditCardForm">
            <div class="heading">
                <h1>Agregar Vehiculo</h1>
            </div>
            <div class="payment">
                <form action="registrarVehiculo.php" method="POST" class="form" role="form" autocomplete="on">
                    <div class="form-group" id="card-number-field">
                        <label>Placa</label>
                        <input type="text" class="form-control" id="placa" name="placa"></input>
                    </div>
                    <div class="form-group">
                        <label>Tipo</label>
                        <select id="tipVeh" name="tipVeh" required="true" style="width: 272%;">
                                                <option value="">Tipo de Vehiculo</option>
                                                <option value="1">Automovil</option>
                                                <option value="2">Motocicleta</option>
                        </select></p>
                    </div>
                    
                    <div class="form-group" id="pay-now">
                        <button type="submit"  class="btn btn-default btn-lg btn-block text-center text-uppercase" id="confirm-purchase">Registrar</button>

                    <form class="" action="miPerfil.php" method="post">
                      <div class="form-group" id="pay-now">
                          
                        <type="button" onclick="location='datos.php'" ALIGN=RIGHT  id="confirm-purchase"
                           class="btn btn-default btn-block text-center text-uppercase" href name="Regresar">
                            Regresar
                            <i class="fa fa-refresh fa-spin" aria-hidden="true"></i>
                        </button>
                      </div>
                    </div>
                    </form>
                </form>

            </div>
        </div>


    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>

</html>