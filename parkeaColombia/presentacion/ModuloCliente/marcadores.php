<?php
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ParqueaderoDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoZona.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParametro.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Parqueadero.php';

$conexionInicial=new Conexion();
$conexion=$conexionInicial->conectarBD();
ManejoParqueadero::setConexionBD($conexion);
ManejoZona::setConexionBD($conexion);
ManejoParametro::setConexionBD($conexion);
$parqueaderos=ManejoParqueadero::listarParqueaderos();

foreach($parqueaderos as $par) {
	$zona=ManejoZona::consultarZona($par->getCodZona());
	$ciudad=$zona->getCodCiudad();
	$parametro=ManejoParametro::listarParametroDeParqueadero($par->getCodParqueadero());
	$precio=$parametro->getTarifa();
	$indicadorprecio;
	if($precio<5000) {
		$indicadorprecio=0;
	}elseif ($precio>=5000 && $precio<=10000) {
		$indicadorprecio=1;
	}
	else{
		$indicadorprecio=2;
	}
	echo '["' . $par->getDireccion() . '", ' . $par->getLongitud() . ', ' . $par->getLatitud(). ', '. $par->getCodTipoParqueadero().', '.$ciudad.', '.$indicadorprecio.'],';
}


?>