<?php
session_start();



require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoTarjeta.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Tarjeta.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

$usuario=$_SESSION['CodCliente'];
$tipo=$_SESSION['tipoUsuario'];
if(isset($_SESSION['email'])){
if ($tipo==1) {
    $link="datos.php";
}
elseif ($tipo==2) {
    $link="../ModuloFuncionario/formularioCliente.php";
}
}else{
	$link="../ModuloFuncionario/formularioCliente.php";
}

$conexion=new Conexion();
$conexionBD=$conexion->conectarBD();
ManejoTarjeta::setConexionBD($conexionBD);
ManejoCliente::setConexionBD($conexionBD);

$num_tarjeta=$_POST['numTar'];
$cvv=$_POST['codSeg'];
$fecha_vencimiento_m=$_POST['fecha_ven_mes'];
$fecha_vencimiento_y=$_POST['fecha_ven_year'];
$_SESSION['numTarjeta']=$num_tarjeta;


$fecha_vencimiento=$fecha_vencimiento_y."-".$fecha_vencimiento_m."-01 00:00:00";
$num_tarjeta=str_replace(' ', '', $num_tarjeta);


$tarjeta=new Tarjeta();
$tarjeta->setNumeroTarjeta($num_tarjeta);
$tarjeta->setCodigoVerificacion($cvv);
$tarjeta->setFechaVencimiento($fecha_vencimiento);

ManejoTarjeta::crearTarjeta($tarjeta);

if ($tipo==1) {
	$cliente = new Cliente();
	$cliente->setIdentificacion($usuario);
	$cliente->setTarjeta($tarjeta);
	ManejoCliente::modificarTarjeta($cliente);
}

if((substr($num_tarjeta, 0, 4)==4026 || substr($num_tarjeta, 0, 4)==4405 || substr($num_tarjeta, 0, 4)==4508  || substr($num_tarjeta, 0, 4)==4844|| substr($num_tarjeta, 0, 4)==4913|| substr($num_tarjeta, 0, 4)==4917 )&& strlen($num_tarjeta)==16){
	//Visa
	$franquicia=1;
	
}elseif ((substr($num_tarjeta, 0, 2) ==34 || substr($num_tarjeta, 0, 2) ==37)&& strlen($num_tarjeta)==15 ) {
	//American Express
	$franquicia=2;
	
}elseif((substr($num_tarjeta, 0, 2) ==51 || substr($num_tarjeta, 0, 2)==52 || substr($num_tarjeta, 0, 2)==53|| substr($num_tarjeta, 0, 2)==54|| substr($num_tarjeta, 0, 2)==55  ) &&strlen($num_tarjeta)==16){
	//Mastercard
	$franquicia=3;
	
}

header("Location: ".$link);



?>