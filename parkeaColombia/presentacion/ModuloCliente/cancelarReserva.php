<?php

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Reserva.php';

date_default_timezone_set("America/Bogota");

$fechaActual=strtotime(date('Y-m-d H:i:s'));

$conexion=new Conexion();
$conexionBD=$conexion->conectarBD();

ManejoReserva::setConexionBD($conexionBD);
$reservas=ManejoReserva::listarReservas();

for ($i=0; $i < count($reservas) ; $i++) { 
	$fechaReserva = strtotime($reservas[$i]->gethoraInicial());
	$interval  =  $fechaActual - $fechaReserva;
	$minutes   = $interval / 60;
	if ($reservas[$i]->getEstado()==1 && $minutes>15) {
		ManejoReserva::modificarEstado(4,$reservas[$i]->getVehiculo());
	}
}


?>