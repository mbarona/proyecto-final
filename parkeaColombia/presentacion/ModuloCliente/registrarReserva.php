<?php
session_start();
if($_SESSION['email']==null){
        header("Location: /proyecto-final/parkeaColombia/presentacion/login.php");
      }
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoReserva.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoVehiculo.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParametro.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoParqueadero.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/ManejoCliente.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Reserva.php';

date_default_timezone_set("America/Bogota");


$cedulaCliente=$_POST["cedulaCliente"];
$codParqueadero=$_POST["codParqueadero"];
$codVehiculo=$_POST["codVehiculo"];
$fechaActual= $_POST["fechaInicial"];
$fechaHoraFinal= $_POST["fechaFinal"];

$fechayhorafinalReal=null;

$date_for_database_inicial = date ("Y-m-d H:i:s", strtotime($fechaActual));
$date_for_database_final= "null";
if ($fechaHoraFinal != "") {
	$date_for_database_final = date ("Y-m-d H:i:s", strtotime($fechaHoraFinal));
}


$conexion=new Conexion();
$conexionBD=$conexion->conectarBD($conexion);

ManejoCliente::setConexionBD($conexionBD);
ManejoReserva::setConexionBD($conexionBD);
ManejoVehiculo::setConexionBD($conexionBD);
ManejoParqueadero::setConexionBD($conexionBD);
ManejoParametro::setConexionBD($conexionBD);

$vehiculo=ManejoVehiculo::consultarVehiculo($codVehiculo);
$parqueadero=ManejoParqueadero::buscarParqueadero($codParqueadero);
$cliente=ManejoCliente::consultarCliente($_SESSION["CodCliente"]);
$cliente->setPuntos($cliente->getPuntos()+5);
ManejoCliente::modificarCliente($cliente);

$parametro=ManejoParametro::listarParametroDeParqueadero($parqueadero->getCodParqueadero());
$precio=$parametro->setCapacidadDisponible($parametro->getCapacidadDisponible()-1);
ManejoParametro::modificarParametro($parametro);

$reserva=new Reserva();

$reserva->setVehiculo($vehiculo); //Solo se pasa el código del vehiculo en este caso, ya que se tiene
$reserva->setParqueadero($parqueadero); //Solo se pasa el código del paraqueadero
$reserva->setEstado(1);//Vg-->vigente
$reserva->sethoraInicial($date_for_database_inicial);
$reserva->setHoraFinal($date_for_database_final);
$reserva->setHora_ingreso_real($date_for_database_inicial);
$reserva->setHora_salida_real($fechayhorafinalReal);

ManejoReserva::crearReserva($reserva);

$_SESSION["mensaje"]="Se registro exitosamente la reserva";
header("Location: /proyecto-final/parkeaColombia/presentacion/ModuloCliente/index.php");


?>
