<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/ReservaDAO.php';

    class ManejoReserva{


        /**
         * Atributo para la conexión a la base de datos
         */
        private static $conexionBD;

        function __construct(){

        }

        /**
         * Obtiene un Reserva
        * @param  [int] $id [id de la Reserva a buscar]
        * @return [Reserva] Reserva encontrado
        */
        public static function consultarReserva($id){

            $ReservaDAO=ReservaDAO::obtenerReservaDAO(self::$conexionBD);
            $Reserva=$ReservaDAO->consultar($id);
            return $Reserva;
        }

        /**
         * Crea un nuevo Reserva 
         * @param Reserva Reserva a ingresar
         * @return void
         */
        public static function crearReserva($Reserva){
            $ReservaDAO=ReservaDAO::obtenerReservaDAO(self::$conexionBD);
            $ReservaDAO->crear($Reserva);

        }

        /**
         * Lista todos los Reservas
         * @return Reserva[] Lista de todos los Reserva de la base de datos
         */
        public  static function listarReservas(){
            $ReservaDAO=ReservaDao::obtenerReservaDAO(self::$conexionBD);
            $Reservas=$ReservaDAO->listarTodo();
            return $Reservas;
        }


        
        /**
         * Modifica un Reserva
         * @param Reserva Reserva a modificar
         * @return void
         */
        public static function modificarReserva($Reserva){
            $ReservaDAO=ReservaDAO::obtenerReservaDAO(self::$conexionBD);
            $ReservaDAO->modificar($Reserva);
        }

        /**
         * Modifica el estado de una reserva
         * @param estado
         * @param placa
         * @return void
         */
        public static function modificarEstado($estado,$placa){
            $ReservaDAO=ReservaDAO::obtenerReservaDAO(self::$conexionBD);
            $ReservaDAO->modificarEstado($estado,$placa);
        }

        /**
         * Consulta la reserva de un vehiculo, en un parqueadero especifico en una fecha espacifica
         *  @param $placaVehiculo Placa del vehiculo relacionado con la reserva
         * @param $codParqueadero Codigo del parqueadero que está realizando la consulta
         * @param $fechaActual Fecha en la que se está haciendo la consulta
         * @return $reserva Código de la reserva de ese vehiculo, en ese parqueadero a esa hora 
         */
        public static function consultarReservaDeUnVehiculoEnParqueaderoYFecha($placa,$codParqueadero,$fecha){
            $reservaDAO=ReservaDAO::obtenerReservaDAO(self::$conexionBD);
            $reserva=$reservaDAO->consultarReservadeUnVehiculoEnUnParqueaderoConFecha($placa,$codParqueadero,$fecha);
            return $reserva;
        }


         /**
         * Consulta la reserva de un vehiculo, en un parqueadero especifico 
         *  @param $placaVehiculo Placa del vehiculo relacionado con la reserva
         * @param $codParqueadero Codigo del parqueadero que está realizando la consulta
         * @return $reserva Código de la reserva de ese vehiculo, en ese parqueadero a esa hora 
         */
        public static function consultarReservadeUnVehiculoEnUnParqueadero($placa,$codParqueadero){
            $reservaDAO=ReservaDAO::obtenerReservaDAO(self::$conexionBD);
            $reserva=$reservaDAO->consultarReservadeUnVehiculoEnUnParqueadero($placa,$codParqueadero);
            return $reserva;
        }


        /**
         * Consulta el código de reserva de un vehiculo para la salida
         * @param Int Placa del vehiculo al que se le registra la salida
         * @param int Codigo del parqueadero en el que se encuentra el vehiculo
         * @return int codigo de la reserva encontrada
         */
        public static function consultarVehiculoParaSalida($placa, $codParqueadero){
            $reservaDAO=ReservaDAO::obtenerReservaDAO(self::$conexionBD);
            $reserva=$reservaDAO->consultarVehiculoParaSalida($placa,$codParqueadero);
            return $reserva;
        }

        /**
         * Consulta las reserva de un cliente
         * @param Int cedula del cliente
         * @return array de reservas
         */
        public static function consultarReservasCliente($cedula){
            $reservaDAO=ReservaDAO::obtenerReservaDAO(self::$conexionBD);
            $reservas=$reservaDAO->consultarReservasCliente($cedula);
            return $reservas;
        }

        /**
         * Lista todos los Reservas
         * @return Reserva[] Lista de todos los Reserva de la base de datos
         */
        public  static function contarReservasPorDia($mes, $ano){
            $ReservaDAO=ReservaDao::obtenerReservaDAO(self::$conexionBD);
            $reservasDia=$ReservaDAO->contarReservasPorDia($mes, $ano);
            return $reservasDia;
        }

        /**
         * Lista todos los Reservas
         * @return Reserva[] Lista de todos los Reserva de la base de datos
         */
        public  static function contarReservasPorDiaPorZona($mes, $ano, $cod_zona){
            $ReservaDAO=ReservaDao::obtenerReservaDAO(self::$conexionBD);
            $reservasDia=$ReservaDAO->contarReservasPorDiaPorZona($mes, $ano, $cod_zona);
            return $reservasDia;
        }

        /**
         * Lista todos los Reservas
         * @return Reserva[] Lista de todos los Reserva de la base de datos
         */
        public  static function ListarAnos(){
            $ReservaDAO=ReservaDao::obtenerReservaDAO(self::$conexionBD);
            $anosReserva=$ReservaDAO->listarAnos();
            return $anosReserva;
        }

        /**
         * Lista todos los Reservas
         * @return Reserva[] Lista de todos los Reserva de la base de datos
         */
        public  static function ListarMesesPorAno($ano){
            $ReservaDAO=ReservaDao::obtenerReservaDAO(self::$conexionBD);
            $mesesReserva=$ReservaDAO->listarMesesPorAno($ano);
            return $mesesReserva;
        }

        /**
         * Lista todos los Reservas
         * @return Reserva[] Lista de todos los Reserva de la base de datos
         */
        public  static function listarAnosPorZona($cod_zona){
            $ReservaDAO=ReservaDao::obtenerReservaDAO(self::$conexionBD);
            $anosReserva=$ReservaDAO->listarAnosPorZona($cod_zona);
            return $anosReserva;
        }

        /**
         * Lista todos los Reservas
         * @return Reserva[] Lista de todos los Reserva de la base de datos
         */
        public  static function listarMesesPorAnoPorZona($ano, $cod_zona){
            $ReservaDAO=ReservaDao::obtenerReservaDAO(self::$conexionBD);
            $mesesReserva=$ReservaDAO->listarMesesPorAnoPorZona($ano, $cod_zona);
            return $mesesReserva;
        }

        /**
         * Cambia la conexión 
         */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

?>