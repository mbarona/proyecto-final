<?php
/*
clase que representa a la entidad Funcionario
*/
require_once('Usuario.php');

class Funcionario extends Usuario{
	
//-------------------------
//Atributos
//-------------------------

	/*
	Representa el codigo del parqueadero al que pertenece el funcionario
	*/
	private $parqueadero;
	/*
	Representa el numero de intentos que ha hecho el funcionario para iniciar sesion
	*/
	private $intentos_fallidos;

//-------------------------
// Métodos
//-------------------------

	/**
 	* Método para obtener el codigo del parqueadero
 	* @return [int] codigo del parqueadero
 	*/
	public function getParqueadero(){
		return $this->parqueadero;
	}

	/**
	* Método para dar el codigo del parqueadero
	* @param [int] codigo del parqueadero
	 */
	public function setParqueadero($parqueadero){
		$this->parqueadero=$parqueadero;
		return $this;
	}
	/**
  * Método para obtener los intentos del funcionario
  * @return [Integer] intentos del funcionario
  */
  public function getIntentosFallidos(){
    return 	$this->intentos_fallidos;
  
  }
  
  /**
   * Método para cambiar los intentos fallidos del funcionario
   * @param [Integer] intentos del funcionario
   */
  public function setIntentosFallidos($intentos_fallidos){
    $this->intentos_fallidos=$intentos_fallidos;
    return $this;
  }
}
?>
