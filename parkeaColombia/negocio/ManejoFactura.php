<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/FacturaDAO.php';

    class ManejoFactura{


        /**
         * Atributo para la conexión a la base de datos
         */
        private static $conexionBD;

        function __construct(){

        }

        /**
         * Obtiene una Factura
        * @param  [String] $factura [codigo de factura a buscar]
        * @return [Factura] Factura encontrada
        */
        public static function consultarFactura($factura){

            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->consultar($factura);
            return $facturas;

        }


        /**
         * Crea un nueva Factura
         * @param Factura  a ingresar
         * @return void
         */
        public static function crearFactura($factura){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturaDAO->crear($factura);

        }

        /**
         * Lista todas las Facturas
         * @return Factura[] Lista de todas las Facturas de la base de datos
         */
        public  static function listarFactura(){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->listarTodo();
            return $facturas;
        }


        /**
         * Modifica una factura
         * @param Factura  a modificar
         * @return void
         */
        public static function modificarFactura($factura){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturaDAO->modificar($factura);
        }

        /**
         * Consulta las facturas de un cliente
         * @param Int cedula del cliente
         * @return array de facturas
         */
        public static function consultarFacturasCliente($cedula){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->consultarFacturasCliente($cedula);
            return $facturas;
        }

        /**
         * Consulta las facturas por zonas de un mes y un año dado
         * @return result resultado del msql query
         */
        public static function facturasZonaMesAño($año,$mes){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->facturasPorZonaPorMesPorAño($año,$mes);
            return $facturas;
        }

          /**
         * Consulta las facturas por zonas de un mes y un año dado
         * @return result resultado del msql query
         */
        public static function facturasZonaMesAño2($año,$mes){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->facturasPorZonaPorMesPorAño2($año,$mes);
            return $facturas;
        }
         /**
         * Consulta los años de las facturas
         * @return result resultado del msql query
         */
        public static function añoFactura(){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->añosFacturas();
            return $facturas;
        }
        /**
         * Consulta los meses de las facturas
         * @return result resultado del msql query
         */
        public static function mesFactura($año){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->mesesFacturas($año);
            return $facturas;
        }
        /**
         * Consulta las facturas por zonas del mes actual
         * @return result resultado del msql query
         */
        public static function facturasZonaFecha($in,$f){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->facturasPorZonaPorFechaDada($in,$f);
            return $facturas;
        }
         /**
         * Consulta las facturas por zonas del año actual
         * @return result resultado del msql query
         */
        public static function facturasZonaAñoDado($año){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->facturasPorZonaPorAñoDado($año);
            return $facturas;
        }

        /**
         * Consulta las facturas por zonas 
         * @return result resultado del msql query
         */
        public static function facturasZona(){
            $facturaDAO=FacturaDAO::obtenerFacturaDAO(self::$conexionBD);
            $facturas=$facturaDAO->facturasPorZona();
            return $facturas;
        }

        /**
         * Cambia la conexión
         */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

?>
