<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/CiudadDAO.php';

    class ManejoCiudad{


    /**
     * Atributo para la conexión a la base de datos
     */
        private static $conexionBD;

        function __construct(){

        }

    /**
     * Obtiene un ciudad
    * @param  [int] $codigo [Código del ciudad a consultar]
    * @return [Ciudad] Ciudad encontrada
    */
    public static function consultarCiudad($codigo){

        $ciudadDAO=CiudadDAO::obtenerCiudadDAO(self::$conexionBD);
        $ciudad=$ciudadDAO->consultar($codigo);
        return $ciudad;

    }
     /**
     * Obtiene un ciudad por nombre
    * @param  [String] $nombre [nombre de la ciudad a consultar]
    * @return [Ciudad] Ciudad encontrada
    */
    public static function consultarCiudadPorNombre($nombre){

        $ciudadDAO=CiudadDAO::obtenerCiudadDAO(self::$conexionBD);
        $ciudad=$ciudadDAO->consultarPorNombre($nombre);
        return $ciudad;

    }


    /**
     * Crea una nueva ciudad 
     * @param Ciudad Ciudad a ingresar
     * @return void
     */
        public static function crearCiudad($ciudad){
            $ciudadDAO=CiudadDAO::obtenerCiudadDAO(self::$conexionBD);
            $ciudadDAO->crear($ciudad);

        }

    /**
         * Lista todas las ciudades  
         * @return Ciudad[] Lista de todas las ciudades de la base de datos
         */
        public  static function listarCiudades(){
            $ciudadDAO=CiudadDAO::obtenerCiudadDAO(self::$conexionBD);
            $ciudades=$ciudadDAO->listarTodo();
            return $ciudades;
        }

    /**
         * Lista todas las ciudades por departamento dado
         * @return Ciudad[] Lista de todas las ciudades de la base de datos por departamento dado
         */
        public  static function listarCiudadesPorDepartamento($departamento){
            $ciudadDAO=CiudadDAO::obtenerCiudadDAO(self::$conexionBD);
            $ciudades=$ciudadDAO->listarPorDepartamento($departamento);
            return $ciudades;
        }     
    /**
         * Lista todas las ciudades con una zona registrada
         * @return Ciudad[] Lista de todas las ciudades de la base de datos con una zona registrada
         */
        public static function listarCiudadesConZona(){
            $ciudadDAO=CiudadDAO::obtenerCiudadDAO(self::$conexionBD);
            $ciudades=$ciudadDAO->listarConZona();
            return $ciudades;
        } 
    /**
     * Modifica una ciudad
     * @param Ciudad Ciudad a modificar
     * @return void
     */
    public static function modificarCiudad($ciudad){
        $ciudadDAO=CiudadDAO::obtenerCiudadDAO(self::$conexionBD);
        $ciudadDAO->modificar($ciudad);
    }

    /**
     * Cambia la conexión 
     */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

    ?>