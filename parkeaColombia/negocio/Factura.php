<?php
/*
clase que representa a la entidad Factura
*/
class Factura{
	
//-------------------------
//Atributos
//-------------------------


	/*
	Representa el código de la factura
	*/
	private $cod_factura;

	/*
	Representa el codigo de reserva asociado a la factura
	*/
	private $cod_reserva;

    /*
	Representa el precio o valor de la  factura
	*/
    private $precio_total;
    /*
	Representa el codigo de la forma de pago asocidada a la factura
	*/
    private $cod_f_pago;
    /*
	Representa el tiempo total de la reserva asocidada a la factura
	*/
    private $tiempo;
    

//----------------------------
//Constructor
//----------------------------

	public function __construct(){

	}

    /**
     * Método para obtener el codigo de la factura
    * @return [Integer] codigo de la factura
    */
    public function getCod_factura(){
		return $this->cod_factura;
	}

    /**
     * Método para cambiar el codigo de la factura
     * @param [Integer] codigo de la factura
     */
	public function setCod_factura($cod_factura){
		$this->cod_factura = $cod_factura;
	}

     /**
     * Método para obtener el codigo de reserva asocidado a la factura
    * @return [Integer] codigo de reserva de la factura
    */
	public function getCod_reserva(){
		return $this->cod_reserva;
	}

    /**
     * Método para cambiar el codigo de reserva asocidado a la factura
     * @param [Integer] codigo de reserva de la factura
     */
	public function setCod_reserva($cod_reserva){
		$this->cod_reserva = $cod_reserva;
	}

    /**
     * Método para obtener el valor de la factura
    * @return [Integer] valor de la factura
    */
	public function getPrecio_total(){
		return $this->precio_total;
	}

    /**
     * Método para cambiar el valor de la factura
     * @param [Integer] valor de la factura
     */
	public function setPrecio_total($precio_total){
		$this->precio_total = $precio_total;
	}

    /**
     * Método para obtener el codigo de la forma de pago de la factura
    * @return [Integer] forma de pago de la factura
    */
	public function getCod_f_pago(){
		return $this->cod_f_pago;
	}
    /**
     * Método para cambiar el codigo de la forma de pago de la factura
     * @param [Integer] codigo de la forma de pago de la factura
     */
	public function setCod_f_pago($cod_f_pago){
		$this->cod_f_pago = $cod_f_pago;
	}
    /**
     * Método para obtener el tiempo de la factura
    * @return [Integer] tiempo de la factura
    */
	public function getTiempo(){
		return $this->tiempo;
	}
    /**
     * Método para cambiar el tiempo de la factura
     * @param [Integer] tiempo de la facturara
     */
	public function setTiempo($tiempo){
		$this->tiempo = $tiempo;
	}

}
?>