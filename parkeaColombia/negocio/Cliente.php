<?php
/*
clase que representa a la entidad Cliente
*/
require_once('Usuario.php');

class Cliente extends Usuario{ 
//-------------------------
//Atributos
//-------------------------
/*
Representa el numero de la tarjeta de credito del cliente
*/
private $tarjeta;
/*
Representa los puntos acumulados por el cliente
*/
private $puntos;
/*
Representa el numero de intentos que ha hecho el cliente para iniciar sesion
*/
private $intentos_fallidos;


 /**
  * Método para obtener el numero de tarjeta del cliente
  * @return [Integer] numero de tarjeta del cliente
  */
  public function getTarjeta(){
	return 	$this->tarjeta;

}

/**
  * Método para cambiar el numero de tarjeta del cliente
 * @param [Integer] numero de tarjeta del cliente
 */
public function setTarjeta($tarjeta){
	$this->tarjeta=$tarjeta;
	return $this;
}

 /**
  * Método para obtener los puntos del cliente
  * @return [Integer] Puntos del cliente
  */
  public function getPuntos(){
	return 	$this->puntos;

}

/**
 * Método para cambiar los puntos del cliente
 * @param [Integer] Puntos del cliente
 */
public function setPuntos($puntos){
	$this->puntos=$puntos;
	return $this;
}
 /**
  * Método para obtener los intentos del cliente
  * @return [Integer] intentos del cliente
  */
  public function getIntentosFallidos(){
    return 	$this->intentos_fallidos;
  
  }
  
  /**
   * Método para cambiar los intentos fallidos del cliente
   * @param [Integer] intentos del cliente
   */
  public function setIntentosFallidos($intentos_fallidos){
    $this->intentos_fallidos=$intentos_fallidos;
    return $this;
  }

}
 ?>
