<?php

require_once  $SERVER["DOCUMENT_ROOT"].'/parkeaColombia/negocio/Estado_Reserva.php';



class EstadoExtendida extends Estado_Reserva{

    /**
     * Atributo con valor "E" haciendo referencia a Extendida
     */
private $estado="E";
/**
 * Actualiza el estado del parqueadero
 * @param Reserva reserva a cambiar el estado
 * @return Reserva reserva con el estado actualizado
 */
    public function actualizarEstado($reserva){
        $reserva->setEstado($estado);
        return $reserva;
    }
}

?>
