    <?php
    /**
     * Importe de clases
     */

    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ParqueaderoDAO.php';
    require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ParametroDAO.php';

    class ManejoParametro{


    /**
     * Atributo para la conexión a la base de datos
     */
        private static $conexionBD;

        function __construct(){

        }

    /**
     * Crea un nuevo parámetro para un parqueadero
     * @param Parametro Parámetro a ingresar
     * @return void
     */
    public static function crearParametro($parametro){
        $parametroDAO=ParametroDAO::obtenerParametroDAO(self::$conexionBD);
        $parametroDAO->crear($parametro);

    }
    /**
     * Modifica un parámetro
     * @param Parametro Parámetro a modificar
     * @return void
     */
    public static function modificarParametro($parametro){
        $parametroDAO=ParametroDAO::obtenerParametroDAO(self::$conexionBD);
        $parametroDAO->modificar($parametro);
    }
    /**
     * Obtiene un parametro 
     * @param Parqueadero Codigo del parametro a obtener
     * @return Parametro Parametro devuelto
     */
    public  static function consultarParametro($codigo){
        $parametroDAO=ParametroDAO::obtenerParametroDAO(self::$conexionBD);
        $parametros=$parametroDAO->consultar($codigo);
        return $parametros;
    }
    
    /**
     * Lista todos los parámetros 
     * @param Parqueadero Parqueadero que tiene el parámetro asignado
     * @return Parametro Lista de todos los parámetros de la base de datos
     */
    public  static function listarParametroDeParqueadero($codigo){
        $parametroDAO=ParametroDAO::obtenerParametroDAO(self::$conexionBD);
        $parametros=$parametroDAO->parametroDeUnParqueadero($codigo);
        return $parametros;
    }
    
    /**
     * Lista el parametro de un parqueadero
     * @return Parametro[] Lista el parametro de un parqueadero
     */
    public  static function listarParametros(){
        $parametroDAO=ParametroDAO::obtenerParametroDAO(self::$conexionBD);
        $parametros=$parametroDAO->listarTodo();
        return $parametros;
    }
    /**
     * Lista el ultimo parametro registrado
     * @return Parametro Ultimo parametro regitrado
     */
    public  static function ultimoParametro(){
        $parametroDAO=ParametroDAO::obtenerParametroDAO(self::$conexionBD);
        $parametros=$parametroDAO->listarUltimo();
        return $parametros;
    }

  



    /**
     * Cambia la conexión 
     */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }



    }

    ?>