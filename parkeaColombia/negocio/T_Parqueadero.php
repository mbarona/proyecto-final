<?php
/*
clase que representa a la entidad T_Parqueadero
*/
class T_Parqueadero{
//-------------------------
//Atributos
//-------------------------


/*
Representa el código de identificación del tipo de parqueadero
*/
private $cod_t_parqueadero;
/*
Representa el nombre del tipo de parqueadero
*/
private $nom_t_parqueadero;


//----------------------------
//Constructor
//----------------------------

/**
 * @param [integer]cod_t_parqueadero : Número de identificación del tipo de parqueadero
 * @param [String]nom_t_parqueadero : Nombre del tipo de parqueadero correspondiente
 */
public function __construct(){



}
 /**
  * Método para obtener  el codigo de identificación del tipo de parqueadero
  * @return [Integer] codigo del tipo de parqueadero
  */
public function getCodTParqueadero(){
	return 	$this->cod_t_parqueadero;

}

/**
 * Método para dar  el codigo de identificación del tipo de parqueadero
 * @param [Integer] codigo del tipo de parqueadero
 */
public function setCodTParqueadero($cod_t_parqueadero){
	$this->cod_t_parqueadero=$cod_t_parqueadero;
	return $this;
}

/**
 * Método para obtener el nombre del tipo de parqueadero
 * @return [String] nombre del tipo de parqueadero
 */
public function getNomTParqueadero(){
	return $this->nom_t_parqueadero;
}

/**
* Método para dar el nombre del tipo de parqueadero
* @param [String] nombre del tipo de parqueadero
 */
public function setNomTParqueadero($nom_t_parqueadero){
	$this->nom_t_parqueadero=$nom_t_parqueadero;
	return $this;
}

}
 ?>
