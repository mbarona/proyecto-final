<?php
    /**
     * Importe de clases
     */


    require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/T_ParqueaderoDAO.php';


    class ManejoT_Parqueadero{


    /**
     * Atributo para la conexión a la base de datos
     */
        private static $conexionBD;

        function __construct(){

        }

    /**
     * Obtiene un tipo de parqueadero
    * @param  [int] $codigo [Código del tipo de parqueadero a buscar]
    * @return [T_Parqueadero] tipo de Parqueadero encontrado
    */
    public static function buscarT_Parqueadero($codigo){

        $parqueaderoDAO=T_ParqueaderoDAO::obtenerT_ParqueaderoDAO(self::$conexionBD);
        $parqueadero=$parqueaderoDAO->consultar($codigo);
        return $parqueadero;

    }


    /**
     * Crea un nuevo tipo de parqueadero 
     * @param T_Parqueadero tipo de Parqueadero a ingresar
     * @return void
     */
        public static function crearT_Parqueadero($parqueadero){
            $parqueaderoDAO=T_ParqueaderoDAO::obtenerT_ParqueaderoDAO(self::$conexionBD);
            $parqueaderoDAO->crear($parqueadero);

        }

        /**
         * Lista todos los tipos de parqueaderos  
         * @return T_Parqueadero[] Lista de todos los tipos  de parqueaderos de la base de datos
         */
        public  static function listarT_Parqueaderos(){
            $parqueaderoDAO=T_ParqueaderoDAO::obtenerT_ParqueaderoDAO(self::$conexionBD);
            $parqueaderos=$parqueaderoDAO->listarTodo();
            return $parqueaderos;
        }
        

    /**
     * Modifica un tipo de parqueadero
     * @param T_Parqueadero tipo de parqueadero a modificar
     * @return void
     */
    public static function modificarT_Parqueadero($t_parqueadero){
        $t_parqueaderoDAO=T_ParqueaderoDAO::obtenerT_ParqueaderoDAO(self::$conexionBD);
        $t_parqueaderoDAO->modificar($t_parqueadero);
    }
    
  

    /**
     * Cambia la conexión 
     */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

    ?>