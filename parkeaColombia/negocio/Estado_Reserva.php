<?php
require_once $SERVER["DOCUMENT_ROOT"].'/parkeaColombia/negocio/Reserva.php';

class Estado_Reserva {


    /**
     * Actualiza el estado de la Reserva
     * @param Reserva Reserva a cambiar el estado
     * @return Reserva Reserva con el estado actualizado
     */
    public function actualizarEstado($reserva){
      return $reserva;
    }

    /**
     * Refresca el observador
     */
    public function  refrescar(){

    }

}

?>
