<?php
/*
clase que representa a la entidad Usuario
*/
abstract class Usuario{

//-------------------------
//Atributos
//-------------------------
/*
Representa la clave de acceso del Usuario
*/
private $contrasena;
/*
Representa el correo electronico del Usuario
*/
private $email;
/*
Representa el estado del Usuario: activo/inactivo
*/
private $estado;
/*
Representa la cedula del Usuario
*/
private $identificacion;
/*
Representa el nombre del Usuario
*/
private $nombre;

//----------------------------
//Constructor
//----------------------------
/*
Representa un constructor vacio para la clase Usuario
*/
function __construct(){

}
/**
* Método para obtener la clave de acceso del Usuario
 * @return [String] clave de acceso del Usuario
 */
public function getContrasena(){
    return $this->contrasena;
}
/**
* Método para cambiar la clave de acceso del Usuario
* @param [String] clave de acceso del Usuario
 */
public function setContrasena($contrasena){
    $this->contrasena = $contrasena;
}
/**
 * Método para obtener el correo electronico del Usuario
  * @return [String] correo del Usuario
  */
public function getEmail(){
    return $this->email;
}
/**
 * Método para obtener el correo electronico del Usuario
 * @param [String] correo del Usuario
 */
public function setEmail($email){
    $this->email = $email;
}
/**
 * Método para obtener el estado del Usuario (1:Activo/0:Inactivo)
 * @return [integer] estado del Usuario
 */
public function getEstado(){
    return $this->estado;
}
/**
 * Método para cambiar el estado del Usuario 
* @param [integer] estado del Usuario
 */
public function setEstado($estado){
    $this->estado = $estado;
}
/**
  * Método para obtener el número de identificación del Usuario
  * @return [Integer] cedula Usuario
  */
public function getIdentificacion(){
    return $this->identificacion;
}
/**
 * Método para cambiar el número de identificación del Usuario
 * @param [Integer] cedula Usuario
 */
public function setIdentificacion($identificacion){
    $this->identificacion = $identificacion;
}
/**
 * Método para obtener  el nombre del Usuario
 * @return [String] nombre del Usuario
 */
public function getNombre(){
    return $this->nombre;
}
/**
 * Método para cambiar  el nombre del Usuario
* @param [String] nombre del Usuario
 */
public function setNombre($nombre){
    $this->nombre = $nombre;
}
}
?>