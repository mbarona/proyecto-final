<?php
/*
clase que representa a la entidad Gerente
*/
require_once('Usuario.php');

class Gerente extends Usuario{
//-------------------------
//Atributos
//-------------------------
/*
Representa a la que fue asignado el gerente
*/
private $zona;
/*
Representa el numero de intentos que ha hecho el gerente para iniciar sesion
*/
private $intentos_fallidos;
/**
 	* Método para obtener el codigo de la zona
 	* @return [int] codigo de la zona
 	*/
public function getZona(){
    return $this->zona;
}
/**
	* Método para cambiar el codigo de la zona
	* @param [int] codigo de la zona
	 */
public function setZona($zona){
    $this->zona = $zona;
}
/**
  * Método para obtener los intentos del gerente
  * @return [Integer] intentos del gerente
  */
  public function getIntentosFallidos(){
    return 	$this->intentos_fallidos;
  
  }
  
  /**
   * Método para cambiar los intentos fallidos del gerente
   * @param [Integer] intentos del gerente
   */
  public function setIntentosFallidos($intentos_fallidos){
    $this->intentos_fallidos=$intentos_fallidos;
    return $this;
  }

}
?>
