<?php
/*
clase que representa a la entidad Dia
*/
class Dias{
	
//-------------------------
//Atributos
//-------------------------


	/*
	Representa el código de identificación del dia
	*/
	private $cod_dia;

	/*
	Representa el nombre del dia
	*/
	private $nom_dia;


//----------------------------
//Constructor
//----------------------------

	/**
 	* @param [integer]cod_dia : Número de identificación del dia
 	* @param [String]nom_dia : Nombre del dia correspondiente
 	*/
	public function __construct(){

	}

 	/**
 	 * Método para obtener  el codigo de identificación del dia
  	* @return [Integer] codigo del dia
  	*/
	public function getCodDia(){
		return 	$this->cod_dia;
	}

	/**
	 * Método para dar  el  el codigo de identificación del dia
	 * @param [Integer] codigo del dia
	 */
	public function setCodDia($cod_dia){
		$this->cod_dia=$cod_dia;
		return $this;
	}

	/**
 	* Método para obtener el nombre del dia
 	* @return [String] nombre del dia
 	*/
	public function getNomDia(){
		return $this->nom_dia;
	}

	/**
	* Método para dar el nombre del dia
	* @param [String] nombre del dia
	 */
	public function setNomDia($nom_dia){
		$this->nom_dia=$nom_dia;
		return $this;
	}

}
?>
