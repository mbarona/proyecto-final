<?php

require_once  $SERVER["DOCUMENT_ROOT"].'/parkeaColombia/negocio/Estado_Reserva.php';



class EstadoVencida extends Estado_Reserva{

    /**
     * Atributo con valor "Vc" haciendo referencia a Vencida
     */
private $estado="Vc";
/**
 * Actualiza el estado de la Reserva
 * @param Reserva reserva a cambiar el estado
 * @return Reserva reserva con el estado actualizado
 */
    public function actualizarEstado($reserva){
        $reserva->setEstado($estado);
        return $reserva;
    }
}

?>
