<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final//parkeaColombia/persistencia/AdministradorDAO.php';

    class ManejoAdministrador{


        /**
         * Atributo para la conexión a la base de datos
         */
        private static $conexionBD;

        function __construct(){

        }

        /**
         * Obtiene un administrador
        * @param  [String] $usuario [Nombre de usuario del administrador a buscar]
        * @return [Administrador] Administrador encontrado
        */
        public static function consultarAdministrador($usuario){

            $administradorDAO=AdministradorDAO::obteneradministradorDAO(self::$conexionBD);
            $administrador=$administradorDAO->consultar($usuario);
            return $administrador;

        }
        public static function consultarEmail($correo){

            $administradorDAO=AdministradorDAO::obteneradministradorDAO(self::$conexionBD);
            $administrador=$administradorDAO->consultarEmail($correo);

            return $administrador;

        }


        /**
         * Crea un nuevo administrador
         * @param Administrador Administrador a ingresar
         * @return void
         */
        public static function crearAdministrador($administrador){
            $administradorDAO=AdministradorDAO::obteneradministradorDAO(self::$conexionBD);
            $administradorDAO->crear($administrador);

        }

        public static function iniciarSesion($email, $contrasenia) {
            $administradorDAO = AdministradorDAO::obteneradministradorDAO(self::$conexionBD);
            $admin=$administradorDAO->consultarEmail($email);
            $pass=$admin->getContrasena();
            $identificadorContrasena = substr($pass, 0, 1);
            $arrayRta=array();

            if($identificadorContrasena=="#"){
                $password=substr($pass,1,strlen($pass)-1);
                $arrayRta[]=$admin;
                $arrayRta[]=1;
            }else{
                $password=$pass;
                $arrayRta[]=$admin;
                $arrayRta[]=0;
            }

            if($admin == null) {
                return null;
            } else {
                if (password_verify($contrasenia, $password)) {
                    return $arrayRta;
                } else {
                    return false;
                }
            }
        }
        /**
         * Lista todos los administradores
         * @return Administrador[] Lista de todos los administradores de la base de datos
         */
        public  static function listarAdministradores(){
            $administradorDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
            $administradores=$administradorDAO->listarTodo();
            return $administradores;
        }


        /**
         * Modifica un administrador
         * @param Administrador Administrador a modificar
         * @return void
         */
        public static function modificarAdministrador($administrador){
            $administradorDAO=AdministradorDAO::obteneradministradorDAO(self::$conexionBD);
            $administradorDAO->modificar($administrador);
        }



        /**
         * Cambia la conexión
         */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }
            public static function contraseniaAleatoria() {
                $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
                $pass = array(); //remember to declare $pass as an array
                $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
                for ($i = 0; $i < 8; $i++) {
                    $n = rand(0, $alphaLength);
                    $pass[] = $alphabet[$n];
                }
                return implode($pass); //turn the array into a string
            }
    }

?>
