<?php
    /**
     * Importe de clases
     */
    require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/GerenteDAO.php';

    class ManejoGerente{


    /**
     * Atributo para la conexión a la base de datos
     */
        private static $conexionBD;

        function __construct(){

        }

    /**
     * Obtiene los gerentes
     * @param identidicacion Gerente como referencia de consulta
     */
    public static function consultarGerente($identidicacion){

        $gerenteDAO=GerenteDAO::obtenerGerenteDAO(self::$conexionBD);
        $gerente=$gerenteDAO->consultar($identidicacion);

        return $gerente;

    }
    /**
     * Obtiene los gerentes
     * @param correo Gerente como referencia de consulta
     */
    public static function consultarEmail($correo){
        $GerenteDAO=GerenteDAO::obtenerGerenteDAO(self::$conexionBD);
        $gerente= $GerenteDAO->consultarEmail($correo);
        return $gerente;
    }

    /**
     * Crea un nuevo Gerente
     * @param Gerente Gerente a ingresar
     * @return void
     */
        public static function crearGerente($gerente){
            $GerenteDAO=GerenteDAO::obtenerGerenteDAO(self::$conexionBD);
            $GerenteDAO->crear($gerente);
        }

    /**
         * Lista todos los gerentes del sistema
         * @return gerentes[] Lista de todos los gerentes de la base de datos
         */
        public  static function listarGerentes(){
            $gerenteDAO=GerenteDAO::obtenerGerenteDAO(self::$conexionBD);
            $gerentes=$gerenteDAO->listarTodo();
            return $gerentes;
        }
        /**
         * Lista el numero total de los gerentes del sistema
         * @return [int] numero total gerentes
         */
        public  static function totalGerentes(){
            $gerenteDAO=GerenteDAO::obtenerGerenteDAO(self::$conexionBD);
            $gerentes=$gerenteDAO->totalGerentes();
            return $gerentes;
        }


    /**
     * Modifica un gerente
     * @param Gerente Gerente ha modificar
     * @return void
     */
    public static function modificarGerente($gerente){
        $gerenteDAO=GerenteDAO::obtenerGerenteDAO(self::$conexionBD);
        $gerenteDAO->modificar($gerente);
    }
    /**
     * Modifica el estado de un Gerente
     * @param Gerente Gerente ha modificar
     * @return void
     */
    public static function modificarEstado($gerente){
        $GerenteDAO=GerenteDAO::obtenerGerenteDAO(self::$conexionBD);
        $GerenteDAO->cambiarEstado($gerente);
    }

    /**
     * Cambia la conexión
     */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }
    public static function iniciarSesion($email, $contrasenia) {
            $gerenteDAO = GerenteDAO::obtenerGerenteDAO(self::$conexionBD);
            $gerente=$gerenteDAO->consultarEmail($email);
            $pass=$gerente->getContrasena();
            $identificadorContrasena = substr($pass, 0, 1);

            $arrayRta=array();

            if($identificadorContrasena=="#"){
                $password=substr($pass,1,strlen($pass)-1);
                $arrayRta[]=$gerente;
                $arrayRta[]=1;
            }else{
                $password=$pass;
                $arrayRta[]=$gerente;
                $arrayRta[]=0;
            }

            if($gerente == null) {
                return null;
            } else {
                if (password_verify($contrasenia, $password)) {
                    return $arrayRta;
                } else {

                    return false;
                }
            }
        }
/**
* Genera  una contraseña aleatoria
*/
    public static function contraseniaAleatoria() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    }

?>
