<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/DetalleHorarioDAO.php';

    class ManejoDetalle_horario{


    /**
     * Atributo para la conexión a la base de datos
     */
        private static $conexionBD;

        function __construct(){

        }

    /**
     * Obtiene un Detalle_horario
    * @param  [int] $codigo [Código del Detalle_horario a consultar]
    * @return [Detalle_horario] Detalle_horario encontrado
    */
    public static function consultarDetalle_horario($codigo){

        $Detalle_horarioDAO=DetalleHorarioDAO::obtenerDetalleHorarioDAO(self::$conexionBD);
        $Detalle_horario=$Detalle_horarioDAO->consultar($codigo);
        return $Detalle_horario;

    }


    /**
     * Crea una nueva Detalle_horario 
     * @param Detalle_horario Detalle_horario a ingresar
     * @return void
     */
        public static function crearDetalle_horario($Detalle_horario){
            $Detalle_horarioDAO=DetalleHorarioDAO::obtenerDetalleHorarioDAO(self::$conexionBD);
            $Detalle_horarioDAO->crear($Detalle_horario);

        }

    /**
         * Lista todas los Detalle_horarios  
         * @return Detalle_horario[] Lista de todas las ciudades de la base de datos
         */
        public  static function listarDetalle_horarios(){
            $Detalle_horarioDAO=DetalleHorarioDAO::obtenerDetalleHorarioDAO(self::$conexionBD);
            $detalle_horarios=$Detalle_horarioDAO->listarTodo();
            return $detalle_horarios;
        }
        

    /**
     * Modifica una Detalle_horario
     * @param Detalle_horario Detalle_horario a modificar
     * @return void
     */
    public static function modificarDetalle_horario($Detalle_horario){
        $Detalle_horarioDAO=DetalleHorarioDAO::obtenerDetalleHorarioDAO(self::$conexionBD);
        $Detalle_horarioDAO->modificar($Detalle_horario);
    }

    /**
     * Cambia la conexión 
     */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

    ?>