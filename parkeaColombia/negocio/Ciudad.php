<?php
/*
clase que representa a la entidad Ciudad
*/
class Ciudad{
//-------------------------
//Atributos
//-------------------------


/*
Representa el código de identificación de la ciudad
*/
private $cod_ciudad;
/*
Representa el nombre de la ciudad
*/
private $nom_ciudad;


//----------------------------
//Constructor
//----------------------------

function __construct(){

}
 /**
  * Método para obtener  el codigo de identificación de la ciudad
  * @return [Integer] codigo de la ciudad
  */
public function getCodCiudad(){
	return 	$this->cod_ciudad;

}

/**
 * Método para dar  el codigo de identificación de la ciudad
 * @param [Integer] codigo de la ciudad
 */
public function setCodCiudad($cod_ciudad){
	$this->cod_ciudad=$cod_ciudad;
	return $this;
}

/**
 * Método para obtener el nombre de la ciudad
 * @return [String] nombre de la ciudad
 */
public function getNomCiudad(){
	return $this->nom_ciudad;
}

/**
* Método para dar el nombre de la ciudad
* @param [String] nombre de la ciudad
 */
public function setNomCiudad($nom_ciudad){
	$this->nom_ciudad=$nom_ciudad;
	return $this;
}

}
 ?>
