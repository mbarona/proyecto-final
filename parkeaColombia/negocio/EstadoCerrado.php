    <?php

    require_once  $SERVER["DOCUMENT_ROOT"].'/parkeaColombia/negocio/Estado_Parqueadero.php';



    class EstadoCerrado extends Estado_Parqueadero{

        /**
         * Atributo con valor "C" haciendo referencia a cerrado
         */
    private $estado="C";
    /**
     * Actualiza el estado del parqueadero
     * @param Parqueadero Parqueadero a cambiar el estado
     * @return Parqueadero Parqueadero con el estado actualizado
     */
        public function actualizarEstado($parqueadero){
            $parqueadero->setEstado($estado);
        
            return $parqueadero;
            
        }


    }

    ?>