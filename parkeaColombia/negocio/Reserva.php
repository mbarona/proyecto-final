<?php
/*
clase que representa a la entidad Reserva
*/
class Reserva{
//-------------------------
//Atributos
//-------------------------

 

/*
Representa el cliente de la reserva
*/
private $vehiculo;
/*
Representa el código de identificacion del parqueadero de la reserva
*/
private $cod_parqueadero;
/*
Representa el estado de la reserva
*/
private $estado;
/*
Representa el la hora inicial de la reserva
*/
private $horainicial;
/*
Representa el la hora final de la reserva
*/
private $horaFinal;
/*
Representa el la hora real en que entra el vehiculo al parqueadero
*/
private $hora_ingreso_real;
/*
Representa el la hora real en que sale el vehiculo del parqueadero
*/
private $hora_salida_real;




//----------------------------
//Constructor
//----------------------------


public function __construct(){


}

 /**
  * Método para obtener el vehiculo de la reserva
  * @return Vehiculo código parqueadero
  */
public function getVehiculo(){
	return 	$this->vehiculo;
}

/**
 * Método para cambiar el vehiculo de la reserva
 * @param [Cliente] vehiculo
 */
public function setVehiculo($vehiculo){
	$this->vehiculo=$vehiculo;
	return $this;
}

 /**
  * Método para obtener el código del parqueadero de la reserva
  * @return [Integer] código parqueadero
  */
public function getParqueadero(){
	return 	$this->cod_parqueadero;
}

/**
 * Método para cambiar el código del parqueadero de la reserva
 * @param [Integer] código parqueadero
 */
public function setParqueadero($cod_parqueadero){
	$this->cod_parqueadero=$cod_parqueadero;
	return $this;
}

/**
 * Método para obtener el estado de la reserva
 * @return [String] estado de la reserva
 */
public function getEstado(){
	return $this->estado;
}

/**
 * Método para cambiar el estado de la reserva
* @param [String] estado de la reserva
 */
public function setEstado($estado){
	$this->estado=$estado;
	return $this;
}

 /**
  * Método para obtener la hora inicial de la reserva
  * @return [date] hora inicial
  */
public function gethoraInicial(){
	return 	$this->horainicial;
}

/**
 * Método para cambiar la hora inicial de la reserva
 * @param [date] hora inicial
 */
public function sethoraInicial($horainicial){
	$this->horainicial=$horainicial;
	return $this;
}

 /**
  * Método para obtener la hora final de la reserva
  * @return [date] hora final
  */
public function getHoraFinal(){
	return 	$this->horaFinal;
}

/**
 * Método para cambiar la hora final de la reserva
 * @param [date] hora final
 */
public function setHoraFinal($horaFinal){
	$this->horaFinal=$horaFinal;
	return $this;
}

/**
 * Registra el observador 
 *
 */ 
public function registrarObservador($observador)
{
		array_push($this->observerList, $observador);
}

/**
 * Elimina el observador 
 *
 */ 
public function eliminarObservador($observador)
{
	$i = 0;
	foreach($this->observerList as $observadorTemp)
	{
		if($observadorTemp->getcodigo() == $observador->getcodigo())
		{
			unset($this->observerList[$i]);
		}

		$i++;
	}
}

/**
 * Envía una notificación a todos los observadores 
 *
 */ 
public function notificarObservadores()
{
	foreach($this->observerList as $observadorTemp)
	{
		$observadorTemp->refrescar($this);
	}
}
 /**
  * Método para obtener la hora de ingreso real del vehiculo
  * @return [date] hora final
  */
public function getHora_ingreso_real(){
	return $this->hora_ingreso_real;
}
/**
 * Método para cambiar la hora de ingreso  real del vehiculo
 * @param [date] hora final
 */
public function setHora_ingreso_real($hora_ingreso_real){
	$this->hora_ingreso_real = $hora_ingreso_real;
}
 /**
  * Método para obtener la hora de salida  real del vehiculo
  * @return [date] hora final
  */
public function getHora_salida_real(){
	return $this->hora_salida_real;
}
/**
 * Método para cambiar la hora de salida real del vehiculo
 * @param [date] hora final
 */
public function setHora_salida_real($hora_salida_real){
	$this->hora_salida_real = $hora_salida_real;
}

}
 ?>
