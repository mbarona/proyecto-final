<?php


/**
 * Clase Vehiculo
 */
class Vehiculo {

/**
 * Placa del Vehiculo
 * @var [String]
 */
	private $placa;

	/**
	 * Cliente del Vehiculo
	 * @var [int]
	 */
		private $cliente;


/**
 * Tipo Vehiculo
 * @var [int]
 */
	private $tipo_vehiculo;



  /**
  *--------------------
  *   	CONSTRUCTOR
  *	-------------------
  */


/**
*--------------------
*   	MÉTODOS
*	-------------------
*/

/**
 * Obtiene la placa del Vehiculo
 * @return [int] [Placa Vehiculo]
 */
public function getPlaca(){
	return $this->placa;
}

/**
 * Obtiene el código del Cliente
 * @return [int] [Cliente]
 */
public function getCodCliente(){
	return $this->cliente;
}

/**
 * Obtiene el código del tipo de Vehiculo
 * @return [int] [Tipo Vehiculo]
 */
public function getTipo_vehiculo(){
	return $this->tipo_vehiculo;
}

/**
 * Método para dar el tipo de Vehiculo
 * @return [int]
 */
public function setPlaca($placa){
	$this->placa=$placa;
  return $this;
}

/**
 * Método para dar el código del Cliente
 * @return [int]
 */
public function setCodCliente($cliente){
	$this->cliente=$cliente;
  return $this;
}


/**
 * Método para dar el código del tipo de Vehiculo
* @param [int] tipo de Vehiculo
 */
public function setTipo_vehiculo($tipo_vehiculo){
	$this->tipo_vehiculo=$tipo_vehiculo;
  return $this;
}

}

?>
