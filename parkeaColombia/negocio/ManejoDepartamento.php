<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/DepartamentoDAO.php';

    class ManejoDepartamento{


    /**
     * Atributo para la conexión a la base de datos
     */
        private static $conexionBD;

        function __construct(){

        }

    /**
     * Obtiene un Departamento
    * @param  [int] $codigo [Código del Departamento a consultar]
    * @return [Departamento] Departamento encontrado
    */
    public static function consultarDepartamento($codigo){

        $DepartamentoDAO=DepartamentoDAO::obtenerDepartamentoDAO(self::$conexionBD);
        $Departamento=$DepartamentoDAO->consultar($codigo);
        return $Departamento;

    }


    /**
     * Crea una nueva Departamento 
     * @param Departamento Departamento a ingresar
     * @return void
     */
        public static function crearDepartamento($Departamento){
            $DepartamentoDAO=DepartamentoDAO::obtenerDepartamentoDAO(self::$conexionBD);
            $DepartamentoDAO->crear($Departamento);

        }

    /**
         * Lista todas los Departamentos  
         * @return Departamento[] Lista de todas las ciudades de la base de datos
         */
        public  static function listarDepartamentos(){
            $DepartamentoDAO=DepartamentoDAO::obtenerDepartamentoDAO(self::$conexionBD);
            $departamentos=$DepartamentoDAO->listarTodo();
            return $departamentos;
        }
        

    /**
     * Modifica una Departamento
     * @param Departamento Departamento a modificar
     * @return void
     */
    public static function modificarDepartamento($Departamento){
        $DepartamentoDAO=DepartamentoDAO::obtenerDepartamentoDAO(self::$conexionBD);
        $DepartamentoDAO->modificar($Departamento);
    }

    /**
     * Cambia la conexión 
     */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

    ?>