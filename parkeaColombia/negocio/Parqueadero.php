<?php
/*
clase que representa a la entidad Parqueadero
*/
class Parqueadero{
//-------------------------
//Atributos
//-------------------------


/*
Representa el código de identificacion del parqueadero
*/
private $cod_parqueadero;
/*
Representa el nombre del parqueadero
*/
private $nom_parqueadero;
/*
Representa el código de identificaicon de la zona del parqueadero
*/
private $cod_zona;
/*
Representa el código de identificacion del tipo de parqueadero
*/
private $cod_tipo_parqueadero;
/*
Representa la direccion del parqueadero
*/
private $direccion;
/*
Representa la longitud del parqueadero
*/
private $longitud;
/*
Representa la latitud del parqueadero
*/
private $latitud;
/*
Representa el estado del parqueadero
*/
private $estado;


//----------------------------
//Constructor
//----------------------------


/**
 * @param [integer]cod_parqueadero : Número de identificación del parqueadero
 * @param [String]nom_parqueadero : Nombre del parqueadero
 * @param [integer]cod_zona : Código de la zona del parqueadero
 * @param [integer]cod_parametro : Código del parametro del parqueadero
 * @param [integer]cod_tipo_parqueadero : Código del tipo del parqueadero
 * @param [String]direccion : Direccion del parqueadero
 * @param [String]longitud : Longitud del parqueadero
 * @param [String]latitud : Latitud del parqueadero
 * @param [String]estado : Estado del parqueadero
 */

public function __construct(){

}
 /**
  * Método para obtener el código del parqueadero
  * @return [Integer] código parqueadero
  */
public function getCodParqueadero(){
	return 	$this->cod_parqueadero;
}

/**
 * Método para cambiar el código del parqueadero
 * @param [Integer] código parqueadero
 */
public function setCodParqueadero($cod_parqueadero){
	$this->cod_parqueadero=$cod_parqueadero;
	return $this;
}

/**
 * Método para obtener el nombre del parqueadero
 * @return [String] nombre del parqueadero
 */
public function getNomParqueadero(){
	return $this->nom_parqueadero;
}

/**
 * Método para cambiar el nombre del parqueadero
* @param [String] nombre del parqueadero
 */
public function setNomParqueadero($nom_parqueadero){
	$this->nom_parqueadero=$nom_parqueadero;
	return $this;
}

 /**
 * Método para obtener el código de la zona del parqueadero
  * @return [String] correo del parqueadero
  */
  public function getCodZona(){
	return 	$this->cod_zona;

}

/**
 * Método para obtener el código de la zona del parqueadero
 * @param [integer]código de la zona del parqueadero
 */
public function setCodZona($cod_zona){
	$this->cod_zona=$cod_zona;
	return $this;
}

 /**
  * Método para obtener el código del tipo del parqueadero
  * @return [Integer] código del tipo del parqueadero
  */
  public function getCodTipoParqueadero(){
	return 	$this->cod_tipo_parqueadero;

}

/**
  * Método para cambiar el código del tipo del parqueadero
 * @param [Integer] código del tipo del parqueadero
 */
public function setCodTipoParqueadero($cod_tipo_parqueadero){
	$this->cod_tipo_parqueadero=$cod_tipo_parqueadero;
	return $this;
}

/**
 * Método para obtener la direccion del parqueadero
 * @return [String] direccion del parqueadero
 */
public function getDireccion(){
	return $this->direccion;
}

/**
 * Método para cambiar la direccion del parqueadero
* @param [String] nombre del parqueadero
 */
public function setDireccion($direccion){
	$this->direccion=$direccion;
	return $this;
}

/**
 * Método para obtener la longitud del parqueadero
 * @return [Decimal] longitud del parqueadero
 */
public function getLongitud(){
	return $this->longitud;
}

/**
 * Método para cambiar la longitud del parqueadero
* @param [Decimal] longitud del parqueadero
 */
public function setLongitud($longitud){
	$this->longitud=$longitud;
	return $this;
}
/**
 * Método para obtener la latitud del parqueadero
 * @return [Decimal] latitud del parqueadero
 */
public function getLatitud(){
	return $this->latitud;
}

/**
 * Método para cambiar la latitud del parqueadero
* @param [Decimal] latitud del parqueadero
 */
public function setLatitud($latitud){
	$this->latitud=$latitud;
	return $this;
}

/**
 * Método para obtener el estado del parqueadero
 * @return [String] estado del parqueadero
 */
public function getEstado(){
	return $this->estado;
}

/**
 * Método para cambiar el estado del parqueadero
* @param [String] estado del parqueadero
 */
public function setEstado($estado){
	$this->estado=$estado;
	return $this;
}

/**
 * Registra el observador 
 *
 */ 
public function registrarObservador($observador)
{
		array_push($this->observerList, $observador);
}

/**
 * Elimina el observador 
 *
 */ 
public function eliminarObservador($observador)
{
	$i = 0;
	foreach($this->observerList as $observadorTemp)
	{
		if($observadorTemp->getcodigo() == $observador->getcodigo())
		{
			unset($this->observerList[$i]);
		}

		$i++;
	}
}

/**
 * Envía una notificación a todos los observadores 
 *
 */ 
public function notificarObservadores()
{
	foreach($this->observerList as $observadorTemp)
	{
		$observadorTemp->refrescar($this);
	}
}


}
 ?>
