<?php
    /**
     * Importe de clases
     */

    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/ParqueaderoDAO.php';

    require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/ParqueaderoDAO.php';


    class ManejoParqueadero{


    /**
     * Atributo para la conexión a la base de datos
     */
        private static $conexionBD;

        function __construct(){

        }

    /**
     * Obtiene un parqueadero
    * @param  [int] $codigo [Código del parqueadero a buscar]
    * @return [Parqueadero] Parqueadero encontrado
    */
    public static function buscarParqueadero($codigo){

        $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
        $parqueadero=$parqueaderoDAO->consultar($codigo);
        return $parqueadero;

    }


    /**
     * Crea un nuevo parqueadero 
     * @param Parqueadero Parqueadero a ingresar
     * @return void
     */
        public static function crearParqueadero($parqueadero){
            $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
            $parqueaderoDAO->crear($parqueadero);

        }

        /**
         * Lista todos los parqueaderos  
         * @return Parqueadero[] Lista de todos los parqueaderos de la base de datos
         */
        public  static function listarParqueaderos(){
            $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
            $parqueaderos=$parqueaderoDAO->listarTodo();
            return $parqueaderos;
        }

    /**
    * Lista el numero total de los Parqueaderos del sistema 
    * @return [int] numero total Parqueaderos
    */
    public  static function totalParqueaderos(){
        $parqueaderoDAO=parqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
        $parqueaderos=$parqueaderoDAO->totalParqueaderos();
        return $parqueaderos;
    }
        

    /**
     * Modifica un parqueadero
     * @param Parqueadero Parqueadero a modificar
     * @return void
     */
    public static function modificarParqueadero($parqueadero){
        $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
        $parqueaderoDAO->modificar($parqueadero);
    }
    
    /**
    * Lista todos los parqueaderos disponibles
    * @return Parqueadero[] Lista de todos los parqueaderos disponibles de la base de datos
    */
    public static function parqueaderosDisponibles(){
        $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
        $parqueaderos=$parqueaderoDAO->listarDisponibles();
        return $parqueaderos;
    }
    
    /**
    * Lista todos los parqueaderos de una zona pasada por parametro
    * @param codzona codigo de lazona en la que se buscaran los parqueaderos 
    * @return Parqueadero[] Lista de todos los parqueaderos disponibles de la base de datos
    */
    public static function parqueaderosEnEstaZona($codzona){
        $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
        $parqueaderos=$parqueaderoDAO->listarParqueaderosZona($codzona);
        return $parqueaderos;
    }

    /**
     * Lista el numero total de los parqueaderos de una zona
     * @param codzona codigo de la zona en la que se buscaran los parqueaderos 
     * @return [int] numero total parqueaderos
     */
    public  static function totalParqueaderosZona($codzona){
        $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
        $parqueaderos=$parqueaderoDAO->cantidadParqueaderosZona($codzona);
        return $parqueaderos;
    }

    /**
    * Lista todos los parqueaderos con la zona y ciudad donde esta
    * @return padqueaderos Lista de todos los parqueaderos con la zona y ciudad
    */
    public static function parqueaderosZonaCiudad(){
        $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
        $parqueaderos=$parqueaderoDAO->listarParqueaderosZonaCiudad();
        return $parqueaderos;
    }
    /**
    * Lista el ultimo parqueadero registrado
    * @return padqueadero Ultimo parqueadero regitrado
    */
    public static function ultimoParqueadero(){
        $parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO(self::$conexionBD);
        $parqueaderos=$parqueaderoDAO->listarUltimo();
        return $parqueaderos;
    }
    /**
     * Cambia la conexión 
     */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

    ?>