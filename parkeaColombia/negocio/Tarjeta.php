<?php

/**
 * 
 */
class Tarjeta
{

	/**
	 * Número de la tarjeta
	 * @var int
	 */
	private $numeroTarjeta;

/**
 * Códio de verificación de la tarjeta
 * @var [type]
 */
private $codigoVerificacion;

/**
 * Fecha de vencimiento de la tarjeta
 * @var [type]
 */
private $fechaVencimiento;

function __construct()
{
}

/////////////////////////////
///MÉTODOS
/////////////////////////////


/**
 * Obtiene el número de la tarjeta
 * @return [int] [Número de la tarjeta]
 */
public function getNumeroTarjeta(){
	return $this->numeroTarjeta;
}

/** 
 * Obtiene el código de verificación de la tarjeta
 * @return [int] [Código de verificación]
 */
public function getCodigoVerificacion(){
	return $this->codigoVerificacion;
}

/**
 * [getFechaVencimiento description]
 * @return [type] [description]
 */
public function getFechaVencimiento(){
	return $this->fechaVencimiento;
}

/**
 * Modifica el número de la tarjeta
 * @param int  $numeroTarjeta 
 */
public function setNumeroTarjeta($numeroTarjeta){
	$this->numeroTarjeta=$numeroTarjeta;
}

/**
 * Modifica el código de verifiación de la tarjeta
 * @param int $codigoVerificacion 
 */
public function setCodigoVerificacion($codigoVerificacion){
	$this->codigoVerificacion=$codigoVerificacion;
}

/**
 * Modifica la fecha de vencimiento de la tarjeta
 * @param String 
 */
public function setFechaVencimiento($fechaVencimiento){
	$this->fechaVencimiento=$fechaVencimiento;
}

}

?>