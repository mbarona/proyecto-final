<?php


/**
 * Clase Parámetro
 */
class Parametro 
{

/**
 * Código del parámetro
 * @var [int]
 */
private $codigo;

/**
 * Código del parqueadero
 * @var [int]
 */
private $codParqueadero;

/**
 * Valor de la capacidad que tendrá el parqueadero
 * @var [int]
 */
private $capacidad;


/**
 * Valor de la capacidad disponible que tiene el parqueadero
 * @var [int]
 */
private $capacidadDisponible;

/**
 * Valor de la tarifa que tendrá el parqueadero
 * @var [int]
 */
private $tarifa;



function __construct()
{

}


	//////////////
	///MÉTODOS///
	/////////////
	///

/**
 * Obtiene el cóidgo del parámetro
 * @return [int] 
 */
public function getCodigo(){
	return $this->codigo;
}

/**
 * Obtiene el cóidgo del parámetro
 * @return [int] 
 */
public function getCodParqueadero(){
	return $this->codParqueadero;
}
/**
 * Obtiene la tarifa asiganda como parámetro al parqueadero
 * @return [int] [Tarifa del parqueadero]
 */
public function getTarifa(){
	return $this->tarifa;
}

/**
 * Obtiene la capacidad en cupos asignada por parámetro al parqueadero
 * @return [int] 
 */
public function getCapacidad(){
	return $this->capacidad;

}
/**
 * Obtiene la capacidad en cupos asignada por parámetro al parqueadero
 * @return [int] 
 */
public function getCapacidadDisponible(){
	return $this->capacidadDisponible;

}

 /**
  * modifica el código del parámetro
  * @param [int] $codigo 
  */
  public function setCodigo($codigo){
	$this->codigo=$codigo;
}

 /**
  * modifica el código del parámetro
  * @param [int] $codigo 
  */
  public function setCodParqueadero($codParqueadero){
	$this->codParqueadero=$codParqueadero;
}

/**
 * Modifica la tarifa que tiene el parámetro
 * @param [int] $tarifa 
 */
public function setTarifa($tarifa){
	$this->tarifa=$tarifa;
}

/**
 * Modifica la capacidad asignada al parqueadero a través del parámetro
 * @param [int] $capacidad nueva capacidad
 */
public function setCapacidad($capacidad){
	$this->capacidad=$capacidad;
}
/**
 * Modifica la capacidad asignada al parqueadero a través del parámetro
 * @param [int] $capacidad nueva capacidad
 */
public function setCapacidadDisponible($capacidadDisponible){
	$this->capacidadDisponible=$capacidadDisponible;
}

}



?>