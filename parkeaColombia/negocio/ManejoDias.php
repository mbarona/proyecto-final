<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/DiaDAO.php';

    class ManejoDias{


    /**
     * Atributo para la conexión a la base de datos
     */
        private static $conexionBD;

        function __construct(){

        }

    /**
     * Obtiene un Dia
    * @param  [int] $codigo [Código del Dia a consultar]
    * @return [Dia] Dia encontrado
    */
    public static function consultarDia($codigo){

        $DiaDAO=DiaDAO::obtenerDiasDAO(self::$conexionBD);
        $dia=$DiaDAO->consultar($codigo);
        return $dia;

    }


    /**
     * Crea una nueva Dia 
     * @param Dia Dia a ingresar
     * @return void
     */
        public static function crearDia($Dia){
            $DiaDAO=DiaDAO::obtenerDiasDAO(self::$conexionBD);
            $DiaDAO->crear($Dia);

        }

    /**
         * Lista todas los Dias  
         * @return Dia[] Lista de todas las ciudades de la base de datos
         */
        public  static function listarDias(){
            $DiaDAO=DiaDAO::obtenerDiasDAO(self::$conexionBD);
            $detalle_horarios=$DiaDAO->listarTodo();
            return $detalle_horarios;
        }
        

    /**
     * Modifica una Dia
     * @param Dia Dia a modificar
     * @return void
     */
    public static function modificarDia($Dia){
        $DiaDAO=DiaDAO::obtenerDiasDAO(self::$conexionBD);
        $DiaDAO->modificar($Dia);
    }

    /**
     * Cambia la conexión 
     */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

    ?>