<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/ZonaDAO.php';

    class ManejoZona{


        /**
         * Atributo para la conexión a la base de datos
         */
        private static $conexionBD;

        function __construct(){

        }

        /**
         * Obtiene una Zona
        * @param  [int] $idZona [codigo de la zona a buscar]
        * @return [Zona] Zona encontrada
        */
        public static function consultarZona($idZona){

            $ZonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $zona=$ZonaDAO->consultar($idZona);
            return $zona;

        }


        /**
         * Crea un nueva Zona
         * @param Zona  a ingresar
         * @return void
         */
        public static function crearZona($zona){
            $ZonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $ZonaDAO->crear($zona);

        }

        /**
         * Lista todas las Zonas
         * @return Zonas[] Lista de todas las Zonas de la base de datos
         */
        public  static function listarZonas(){
            $ZonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $zonas=$ZonaDAO->listarTodo();
            return $zonas;
        }

        /**
         * Lista el numero total de los zonas del sistema 
         * @return [int] numero total zonas
         */
        public  static function totalZonas(){
            $ZonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $zonas=$ZonaDAO->totalZonas();
            return $zonas;
        }
        
        /**
         * Modifica una Zona
         * @param Zona a modificar
         * @return void
         */
        public static function modificarZona($zona){
            $ZonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $ZonaDAO->modificar($zona);
        }
        /**
        * Lista todas las zonas con la ciudad donde esta
        * @return zonas Lista de todas las zonas con la ciudad
        */
        public static function zonaCiudad(){
            $zonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $zonas=$zonaDAO->listarZonaCiudad();
            return $zonas;
        }
/**
        * Lista todas las zonas con la ciudad pasada por parametro
        * @return zonas Lista de todas las zonas con la ciudad
        */
        public static function zonaCiudadDada($cod_ciudad){
            $zonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $zonas=$zonaDAO->listarZonaPorCiudadDada($cod_ciudad);
            return $zonas;
        }
        /**
        * Lista todas las zonas que no tengan un gerente asignado
        * @return zonas Lista de todas las zonas sin gerente
        */
        public static function zonasSinGerente(){
            $zonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $zonas=$zonaDAO->listarZonasSinGerente();
            return $zonas;
        }
        
        /**
        * Lista todas las zonas que tengan al menos un parqueadero
        * @return zonas Lista de todas las zonas que tengan al menos un parqueadero
        */
        public static function zonasConParqueadero($ciudad){
            $zonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $zonas=$zonaDAO->listarZonasParqueadero($ciudad);
            return $zonas;
        }

        /**
        * Lista todas las zonas que tengan al menos un parqueadero
        * @return zonas Lista de todas las zonas que tengan al menos un parqueadero
        */
        public static function ListarZonasConParqueadero(){
            $zonaDAO=ZonaDAO::obtenerZonaDAO(self::$conexionBD);
            $zonas=$zonaDAO->listarZonasParqueaderoSinCiudad();
            return $zonas;
        }

        /**
         * Cambia la conexión
         */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

?>
