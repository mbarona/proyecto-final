<?php
	
	
	/**
	 * Representa la interface de Observable
	 */
	interface Observable {

		//------------------
		// Métodos
		//-----------------
		
		/**
		 *
		 * Método abstracto para eliminar observador 
		 *
		 */
		
		public function eliminarObservador($observador);

		/**
		 *
		 * Método abstracto para notificar obsevadores 
		 *
		 */
		
		public function notificarObservadores();

		/**
		 *
		 * Método abstracto para registrar observador
		 *
		 */
		
		public function registrarObservador($observador);

	}



 ?>