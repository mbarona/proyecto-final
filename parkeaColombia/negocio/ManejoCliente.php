<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/ClienteDAO.php';
    require_once 'Usuario.php';
    require_once 'Cliente.php';

    class ManejoCliente{


        /**
         * Atributo para la conexión a la base de datos
         */
        private static $conexionBD;

        function __construct(){

        }

        /**
         * Obtiene un cliente
        * @param  [int] $id [id del cliente a buscar]
        * @return [Cliente] Cliente encontrado
        */
        public static function consultarCliente($id){

            $clienteDAO=clienteDAO::obtenerclienteDAO(self::$conexionBD);
            $cliente=$clienteDAO->consultar($id);
            return $cliente;
        }

        public static function consultarEmail($correo){

            $clienteDAO=clienteDAO::obtenerclienteDAO(self::$conexionBD);
            $cliente=$clienteDAO->consultarEmail($correo);
            return $cliente;
        }


        /**
         * Crea un nuevo cliente
         * @param Cliente Cliente a ingresar
         * @return void
         */
        public static function crearCliente($cliente){
            $ClienteDAO=ClienteDAO::obtenerClienteDAO(self::$conexionBD);
            $ClienteDAO->crear($cliente);

        }


        public static function iniciarSesion($email, $contrasenia) {
            $clienteDAO = ClienteDAO::obtenerClienteDAO(self::$conexionBD);
            $cliente=$clienteDAO->consultarEmail($email);
            $pass=$cliente->getContrasena();
            $identificadorContrasena = substr($pass, 0, 1);

            $arrayRta=array();
        
            if($identificadorContrasena=="#"){
                $password=substr($pass,1,strlen($pass)-1);
                $arrayRta[]=$cliente;
                $arrayRta[]=1;
            }else{
                $password=$pass;
                $arrayRta[]=$cliente;
                $arrayRta[]=0;
            }

            if($cliente == null) {
                return null;
            } else {
                if (password_verify($contrasenia, $password)) {
                    return $arrayRta;
                } else {
                    return false;
                }
            }
        }

        /**
         * Lista todos los clientes
         * @return Cliente[] Lista de todos los cliente de la base de datos
         */
        public  static function listarClientes(){
            $clienteDAO=clienteDao::obtenerClienteDAO(self::$conexionBD);
            $clientes=$clienteDAO->listarTodo();
            return $clientes;
        }

        /**
         * Modifica un cliente
         * @param Cliente Cliente a modificar
         * @return void
         */
        public static function modificarCliente($cliente){
            $clienteDAO=clienteDAO::obtenerClienteDAO(self::$conexionBD);
            $clienteDAO->modificar($cliente);
        }
        /**
         * Modifica un cliente
         * @param Cliente Cliente a modificar
         * @return void
         */
        public static function modificarDatos($cliente){
            $clienteDAO=clienteDAO::obtenerClienteDAO(self::$conexionBD);
            $clienteDAO->modificarDatos($cliente);
        }
        /**
         * Modifica el estado de un Cliente
         * @param Cliente Cliente ha modificar
         * @return void
         */
        public static function modificarEstado($cliente){
            $ClienteDAO=ClienteDAO::obtenerClienteDAO(self::$conexionBD);
            $ClienteDAO->cambiarEstado($cliente);
        }
        /**
         * Modifica la tarjeta de un Cliente
         * @param Cliente Cliente ha modificar
         * @return void
         */
        public static function modificarTarjeta($cliente){
            $ClienteDAO=ClienteDAO::obtenerClienteDAO(self::$conexionBD);
            $ClienteDAO->cambiarTarjeta($cliente);
        }
        /**
         * Lista el numero total de los clientes del sistema
         * @return [int] numero total clientes
         */
        public  static function totalClientes(){
            $clienteDAO=ClienteDAO::obtenerClienteDAO(self::$conexionBD);
            $clientes=$clienteDAO->totalClientes();
            return $clientes;
        }

        /**
         * Obtiene el código del cliente de la reserva ingresada por parámetro
         * @param int $codReserva Código de la reserva
         * @return int código del cliente
         */
        public static function obtenerClienteDeUnaReserva($codReserva){
            $clienteDAO=ClienteDAO::obtenerClienteDAO(self::$conexionBD);
            $codCliente=$clienteDAO->obtenerClienteDeUnaReserva($codReserva);
            return $codCliente;
        }


        /**
         * Cambia la conexión
         */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }





/**
 * Genera  una contraseña aleatoria
 */
    public static function contraseniaAleatoria() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    }



?>
