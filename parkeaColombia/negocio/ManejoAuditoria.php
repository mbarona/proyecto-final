<?php
/**
 * Importe de clases
 */
 require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
 require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/AuditoriaDAO.php';

class ManejoAuditoria{


/**
 * Atributo para la conexión a la base de datos
 */
    private static $conexionBD;

    function __construct(){

    }

/**
 * Obtiene los auditorias
 * @param cod_usuario codigo del usuario del que se quiere consultar las auditorias
 * @return Auditoria[] Arreglo con los datos del Auditoria del usuario
 */
public static function consultarAuditoriaPorUsuario($cod_usuario){

    $AuditoriaDAO=AuditoriaDAO::obtenerAuditoriaDAO(self::$conexionBD);
    $Auditoria=$AuditoriaDAO->consultar($cod_usuario);
    return $Auditoria;

}
/**
 * Obtiene los auditorias
 * @param cod_usuario codigo del usuario del que se quiere consultar las auditorias
 * @return Auditoria[] Arreglo con los datos del Auditoria del usuario
 */
public static function consultarAuditoriaPorFechas($fecha_inicial,$fecha_final){

    $AuditoriaDAO=AuditoriaDAO::obtenerAuditoriaDAO(self::$conexionBD);
    $Auditoria=$AuditoriaDAO->consultarPorFechas($fecha_inicial,$fecha_final);
    return $Auditoria;

}
/**
 * Crea un nuevo Auditoria
 * @param auditoria datos del nuevo Auditoria
 * @return void
 */
    public static function crearAuditoria($auditoria){
        $AuditoriaDAO=AuditoriaDAO::obtenerAuditoriaDAO(self::$conexionBD);
        $AuditoriaDAO->crear($auditoria);
    }

/**
     * Lista todos los auditorias del sistema
     * @return Auditoria[] Lista de todos los auditorias de la base de datos
     */
    public  static function listarAuditorias(){
        $AuditoriaDAO=AuditoriaDAO::obtenerAuditoriaDAO(self::$conexionBD);
        $auditorias=$AuditoriaDAO->listarTodo();
        return $auditorias;
    }
    /**
     * Lista todos los auditorias de un parametro
     * @return Auditoria[] Lista de todos los auditorias de que contengan un parametro
     */
    public  static function listarAuditoriasPorParametro($consulta){
        $AuditoriaDAO=AuditoriaDAO::obtenerAuditoriaDAO(self::$conexionBD);
        $auditorias=$AuditoriaDAO->buscarJS($consulta);
        return $auditorias;
    }
/**
 * Cambia la conexión
 */
    public static function setConexionBD($conexionBD)
        {
            self::$conexionBD = $conexionBD;
        }

/**
* Genera  una contraseña aleatoria 
*/
}
?>
