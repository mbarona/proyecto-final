<?php
	
	
	/**
	 * Representa la interface de Observable
	 */
	interface Observador {

		//------------------
		// Métodos
		//-----------------
		
		/**
		 *
		 * Método abstracto para refrescar
		 *
		 */
		
		public function refrescar($observador);
	}
 ?>