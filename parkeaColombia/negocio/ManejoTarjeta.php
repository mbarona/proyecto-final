<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/TarjetaDAO.php';

    class ManejoTarjeta{


        /**
         * Atributo para la conexión a la base de datos
         */
        private static $conexionBD;

        function __construct(){

        }

        /**
         * Obtiene una Tarjeta
        * @param  [int] $idTarjeta [codigo de la Tarjeta a buscar]
        * @return [Tarjeta] Tarjeta encontrada
        */
        public static function buscarTarjeta($idTarjeta){

            $TarjetaDAO=TarjetaDAO::obtenerTarjetaDAO(self::$conexionBD);
            $tarjeta=$TarjetaDAO->consultar($idTarjeta);
            return $tarjeta;

        }


        /**
         * Crea un nueva Tarjeta
         * @param Tarjeta  a ingresar
         * @return void
         */
        public static function crearTarjeta($tarjeta){
            $TarjetaDAO=TarjetaDAO::obtenerTarjetaDAO(self::$conexionBD);
            $TarjetaDAO->crear($tarjeta);

        }

        /**
         * Lista todas las Tarjetas
         * @return Tarjetas[] Lista de todas las Tarjetas de la base de datos
         */
        public  static function listarTarjetas(){
            $TarjetaDAO=TarjetaDAO::obtenerTarjetaDAO(self::$conexionBD);
            $tarjetas=$TarjetaDAO->listarTodo();
            return $tarjetas;
        }


        /**
         * Modifica una Tarjeta
         * @param Tarjeta a modificar
         * @return void
         */
        public static function modificarTarjeta($tarjeta){
            $TarjetaDAO=TarjetaDAO::obtenerTarjetaDAO(self::$conexionBD);
            $TarjetaDAO->modificar($tarjeta);
        }

        /**
         * Cambia la conexión
         */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

?>
