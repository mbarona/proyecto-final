<?php

/**
 * Clase Tipo de Vehiculo
 */
class T_Vehiculo {

/**
 * Código del Vehiculo
 * @var [int]
 */
	private $cod_tipo;

/**
 * Nombre del Tipo Vehiculo
 * @var [String]
 */
	private $nom_tipo;

  /**
  *--------------------
  *   	CONSTRUCTOR
  *	-------------------
  */

	

/**
*--------------------
*   	MÉTODOS
*	-------------------
*/

/**
 * Obtiene la placa del Vehiculo
 * @return [int] [Placa Vehiculo]
 */
public function getCod_t_vehiculo(){
	return $this->cod_tipo;
}

/**
 * Obtiene el nombre del tipo de Vehiculo
 * @return [String] [Tipo Vehiculo]
 */
public function getNom_t_vehiculo(){
	return $this->nom_tipo;
}



/**
 * Método para dar el código de tipo de Vehiculo
* @param [int] Código tipo de Vehiculo
 */
public function setCod_t_vehiculo($cod_tipo){
	$this->cod_tipo=$cod_tipo;
  return $this;
}


/**
 * Método para dar el nombre del tipo de Vehiculo
 * @return [String]
 */
public function setNom_t_vehiculo($nom_tipo){
	$this->placa=$nom_tipo;
  return $this;
}

}

?>
