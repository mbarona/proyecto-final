<?php
/*
clase que representa a la entidad Zona
*/
class Zona{
//-------------------------
//Atributos
//-------------------------


/*
Representa el código de identificación de la zona
*/
private $cod_zona;
/*
Representa el nombre de la zona
*/
private $nom_zona;

/*
Representa el código de identificación de la ciudad
*/
private $cod_ciudad;
/*

//----------------------------
//Constructor
//----------------------------

/**
 * @param [integer]cod_zona : Número de identificación de la zona
 * @param [String]nom_zona : Nombre de la zona correspondiente
 * @param [integer]cod_ciudad: Número de identificación de la ciudad
 */
public function __construct(){



}
 /**
  * Método para obtener  el codigo de identificación de la zona
  * @return [Integer] codigo de la zona
  */
public function getCodZona(){
	return 	$this->cod_zona;

}

/**
 * Método para dar  el codigo de identificación de la zona
 * @param [Integer] codigo de la zona
 */
public function setCodZona($cod_zona){
	$this->cod_zona=$cod_zona;
}

/**
 * Método para obtener el nombre de la zona
 * @return [String] nombre de la zona
 */
public function getNomZona(){
	return $this->nom_zona;
}

/**
* Método para dar el nombre de la zona
* @param [String] nombre de la zona
 */
public function setNomZona($nom_zona){
	$this->nom_zona=$nom_zona;
}

/**
 * Método para obtener  el codigo de identificación de la ciudad
 * @return [Integer] codigo de la ciudad
 */
public function getCodCiudad(){
   return 	$this->cod_ciudad;

}

/**
* Método para dar  el codigo de identificación de la ciudad
* @param [Integer] codigo de la ciudad
*/
public function setCodCiudad($cod_ciudad){
   $this->cod_ciudad=$cod_ciudad;
}


}
 ?>
