	<?php
		require_once $_SERVER["DOCUMENT_ROOT"].'/parkeaColombia/negocio/Parqueadero.php';
								
							
							class EstadoDisponible extends Estado_Parqueadero{
								
								
								/**
								* Atributo con valor "D" haciendo referencia a DISPONIBLE
								*/
								private $estado="D";
								/**
								* Actualiza el estado del parqueadero
								* @param Parqueadero Parqueadero a cambiar el estado
								* @return Parqueadero Parqueadero con el estado actualizado
								*/
								public function actualizarEstado($parqueadero){
									$parqueadero->setEstado($estado);
									
									return $parqueadero;
									
								}
								
								
							}
							
							
							?>