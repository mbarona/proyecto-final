<?php
/*
clase que representa a la entidad Departamento
*/
class Departamento{
//-------------------------
//Atributos
//-------------------------


/*
Representa el código de identificación de la departamento
*/
private $cod_departamento;
/*
Representa el nombre del departamento
*/
private $nom_departamento;


//----------------------------
//Constructor
//----------------------------

function __construct(){

}
 /**
  * Método para obtener  el codigo de identificación del departamento
  * @return [Integer] codigo del departamento
  */
public function getCodDepartamento(){
	return 	$this->cod_departamento;

}

/**
 * Método para dar  el codigo de identificación del departamento
 * @param [Integer] codigo del departamento
 */
public function setCodDepartamento($cod_departamento){
	$this->cod_departamento=$cod_departamento;
	return $this;
}

/**
 * Método para obtener el nombre del departamento
 * @return [String] nombre del departamento
 */
public function getNomDepartamento(){
	return $this->nom_departamento;
}

/**
* Método para dar el nombre del departamento
* @param [String] nombre del departamento
 */
public function setNomDepartamento($nom_departamento){
	$this->nom_departamento=$nom_departamento;
	return $this;
}

}
 ?>
