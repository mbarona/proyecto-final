<?php
/*
clase que representa a la entidad Auditoria
*/
class Auditoria{
//-------------------------
//Atributos
//-------------------------
	/*
	Representa el código de identificación de la auditoria
	*/
	private $cod_auditoria;
	/*
	Representa la cedula del usuario al que se le hara la auditoria
	*/
	private $cedula_usuario;
	/*
	Representa la tabla que fue afectada
	*/
	private $tabla;
	/*
	Representa el código del afectado 
	*/
	private $cod_afectado;
	/*
	Representa el tipo de operacion que se hace ['Insert','Delete','Update','login']
	*/
	private $tipo_operacion;
	/*
	Representa la fecha de la auditoria
	*/
    private $fecha;
    //----------------------------
	//Constructor
	//----------------------------

	function __construct(){

	}
	/**
	 * Método para obtener  el codigo de la auditoria
	* @return [Integer] codigo de la auditoria
	*/
    public function getCod_auditoria(){
		return $this->cod_auditoria;
	}

	public function setCod_auditoria($cod_auditoria){
		$this->cod_auditoria = $cod_auditoria;
	}
	/**
	 * Método para obtener la cedula del usuario
	* @return [Integer] cedula del usuario
	*/
	public function getCedula_usuario(){
		return $this->cedula_usuario;
	}

	public function setCedula_usuario($cedula_usuario){
		$this->cedula_usuario = $cedula_usuario;
	}
	/**
	 * Método para obtener la tabla afectada
	* @return [String] tabla afectada
	*/
	public function getTabla(){
		return $this->tabla;
	}
	/**
	 * Método para cambiar la tabla afectada
	 * @param [String] Tabla afectada
	 */
	public function setTabla($tabla){
		$this->tabla = $tabla;
	}
	/**
	 * Método para obtener el codigo del afectado
	* @return [String] codigo afectado
	*/
	public function getCod_afectado(){
		return $this->cod_afectado;
	}
	/**
	 * Método para cambiar el codigo afectado
	 * @param [String] Tipo de operacion
	 */
	public function setCod_afectado($cod_afectado){
		$this->cod_afectado = $cod_afectado;
	}
	/**
	 * Método para obtener el tipo operacion 
	* @return [String] Tipo operacion 
	*/
	public function getTipo_operacion(){
		return $this->tipo_operacion;
	}
	/**
	 * Método para cambiar el tipo de operacion 
	 * @param [String] Tipo de operacion
	 */
	public function setTipo_operacion($tipo_operacion){
		$this->tipo_operacion = $tipo_operacion;
	}
	/**
	 * Método para obtener la fecha 
	* @return [Date] Fecha
	*/
	public function getFecha(){
		return $this->fecha;
	}
	/**
	 * Método para cambiar la fecha 
	 * @param [Date] fecha
	 */
	public function setFecha($fecha){
		$this->fecha = $fecha;
	}
}
?>