<?php
/**
 * Importe de clases
 */
 require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
 require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/FuncionarioDAO.php';

class ManejoFuncionario{


/**
 * Atributo para la conexión a la base de datos
 */
    private static $conexionBD;

    function __construct(){

    }

/**
 * Obtiene los funcionarios
 * @param cod_funcionario codigo de Funcionario como referencia de consulta
 * @return Funcionario  los datos del Funcionario
 */
public static function consultarFuncionario($cod_funcionario){

    $FuncionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
    $Funcionario=$FuncionarioDAO->consultar($cod_funcionario);

    return $Funcionario;
 
}
public static function consultarEmail($correo){

    $FuncionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
    $Funcionario=$FuncionarioDAO->consultarEmail($correo);

    return $Funcionario;

}

/**
 * Crea un nuevo Funcionario
 * @param funcionario datos del nuevo Funcionario
 * @return void
 */
    public static function crearFuncionario($funcionario){
        $FuncionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
        $FuncionarioDAO->crear($funcionario);
    }

/**
     * Lista todos los funcionarios del sistema
     * @return funcionarios[] Lista de todos los funcionarios de la base de datos
     */
    public  static function listarFuncionario(){
        $FuncionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
        $funcionarios=$FuncionarioDAO->listarTodo();
        return $funcionarios;
    }

    public static function iniciarSesion($email, $contrasenia) {
        $funcionarioDAO = FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
        $funcionario=$funcionarioDAO->consultarEmail($email);
        $pass=$funcionario->getContrasena();
        $identificadorContrasena = substr($pass, 0, 1);

        $arrayRta=array();

        if($identificadorContrasena=="#"){
            $password=substr($pass,1,strlen($pass)-1);
            $arrayRta[]=$funcionario;
            $arrayRta[]=1;
        }else{
            $password=$pass;
            $arrayRta[]=$funcionario;
            $arrayRta[]=0;

        }
        if($funcionario == null) {
            return null;
        } else {
            if (password_verify($contrasenia, $password)) {
                return $arrayRta;
            } else {
                return false;
            }
        }
    }

/**
 * Modifica un Funcionario
 * @param Funcionario Funcionario ha modificar
 * @return void
 */
public static function modificarFuncionario($funcionario){
    $FuncionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
    $FuncionarioDAO->modificar($funcionario);
}
/**
 * Modifica el estado de un Funcionario
 * @param Funcionario Funcionario ha modificar
 * @return void
 */
public static function modificarEstado($funcionario){
    $FuncionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
    $FuncionarioDAO->cambiarEstado($funcionario);
}

/**
    * Lista todos los funcionarios de una zona pasada por parametro
    * @param codzona codigo de la zona en la que se buscaran los funcionarios
    * @return Funcionario[] Lista de todos los funcionarios disponibles de la base de datos
    */
    public static function funcionariosEnEstaZona($codzona){
        $funcionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
        $funcionarios=$funcionarioDAO->listarFuncionariosZona($codzona);
        return $funcionarios;
    }



/**
 * Lista el numero total de los funcionarios del sistema
 * @return [int] numero total funcionarios
 */
public  static function totalFuncionarios(){
    $funcionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
    $funcionarios=$funcionarioDAO->totalFuncionarios();
    return $funcionarios;
}
/**
 * Lista el numero total de los funcionarios de una zona
 * @param codzona codigo de la zona en la que se buscaran los funcionarios
 * @return [int] numero total funcionarios
 */
public  static function totalFuncionariosZona($codzona){
    $funcionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
    $funcionarios=$funcionarioDAO->cantidadFuncionariosZona($codzona);
    return $funcionarios;
}
/**
 * Lista el numero total de los funcionarios de un parqueadero
 * @param codparqueadero codigo de un parqueadero en la que se buscaran los funcionarios
 * @return [int] numero total funcionarios
 */
public  static function totalFuncionariosParqueadero($codparqueadero){
    $funcionarioDAO=FuncionarioDAO::obtenerFuncionarioDAO(self::$conexionBD);
    $funcionarios=$funcionarioDAO->cantidadFuncionariosParqueadero($codparqueadero);
    return $funcionarios;
}

/**
 * Cambia la conexión
 */
    public static function setConexionBD($conexionBD)
        {
            self::$conexionBD = $conexionBD;
        }

/**
* Genera  una contraseña aleatoria
*/
public static function contraseniaAleatoria() {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}
}
?>
