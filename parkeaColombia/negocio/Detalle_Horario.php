<?php
/*
clase que representa a la entidad Detalle_Horario
*/
class Detalle_Horario{
	
//-------------------------
//Atributos
//-------------------------

	/*
	Representa el código de identificación del dia
	*/
	private $cod_dia;

	/*
	Representa la hora de inicio de la reserva
	*/
	private $hora_inicio;

	/*
	Representa la hora final de la reserva
	*/
	private $hora_final;
	/*
	Representa el parametro al cual pertenece el horario
	*/
	private $cod_parametro;


	/**
 	* Método para obtener codigo del dia
 	* @return [int] codigo del dia
 	*/
	public function getCod_dia(){
		return $this->cod_dia;
	}

	/**
	* Método para dar el codigo del dia
	* @param [int] codigo del dia
	 */
	public function setCod_dia($cod_dia){
		$this->cod_dia=$cod_dia;
		return $this;
	}

	/**
 	* Método para obtener la hora de inicio de la reserva
 	* @return [date] nombre del dia
 	*/
	public function getHora_inicio(){
		return $this->hora_inicio;
	}

	/**
	* Método para dar la hora de finalizacion de la reserva
	* @param [date] hora de inicio
	 */
	public function setHora_inicio($hora_inicio){
		$this->hora_inicio=$hora_inicio;
		return $this;
	}

	/**
 	* Método para obtener la hora de finalizacion de la reserva
 	* @return [date] hora de finalizacion
 	*/
	public function getHora_final(){
		return $this->hora_final;
	}

	/**
	* Método para dar la hora de finalizacion de la reserva
	* @param [date] hora de finalizacion
	 */
	public function setHora_final($hora_final){
		$this->hora_final=$hora_final;
		return $this;
	}
/**
 	* Método para obtener el codigo del parametro
 	* @return [int] el codigo del parametro
 	*/
	public function getCod_parametro(){
		return $this->cod_parametro;
	}
/**
	* Método para cambiar el codigo del parametro
	* @param [int]codigo del parametro
	 */
	public function setCod_parametro($cod_parametro){
		$this->cod_parametro = $cod_parametro;
	}
}
?>
