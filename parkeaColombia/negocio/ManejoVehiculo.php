<?php
    /**
     * Importe de clases
     */
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
    require_once($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/VehiculoDAO.php';

    class ManejoVehiculo{


        /**
         * Atributo para la conexión a la base de datos
         */
        private static $conexionBD;

        function __construct(){

        }

        /**
         * Obtiene un vehiculo
        * @param  [String] $placa [placa del vehiculo a buscar]
        * @return [Vehiculo] Vehiculo encontrado
        */
        public static function consultarVehiculo($placa){

            $vehiculoDAO=VehiculoDAO::obtenerVehiculoDAO(self::$conexionBD);
            $vehiculo=$vehiculoDAO->consultar($placa);
            return $vehiculo;

        }


        /**
         * Crea un nuevo vehiculo 
         * @param Vehiculo Vehiculo a ingresar
         * @return void
         */
        public static function crearVehiculo($vehiculo){
            $vehiculoDAO=VehiculoDAO::obtenerVehiculoDAO(self::$conexionBD);
            $vehiculoDAO->crear($vehiculo);

        }

        /**
         * Lista todos los vehiculos
         * @return Vehiculo[] Lista de todos los vehiculos de la base de datos
         */
        public  static function listarVehiculos(){
            $vehiculoDAO=VehiculoDao::obtenerVehiculoDAO(self::$conexionBD);
            $vehiculos=$vehiculoDAO->listarTodo();
            return $vehiculos;
        }
        

        /**
         * Modifica un vehiculo
         * @param Vehiculo Vehiculo a modificar
         * @return void
         */
        public static function modificarVehiculo($vehiculo){
            $vehiculoDAO=VehiculoDAO::obtenerVehiculoDAO(self::$conexionBD);
            $vehiculoDAO->modificar($vehiculo);
        }

        /**
         * Consulta los vehiculos que tiene un cliente
         * @param integer Número de identificación del cliente
         * @return Vehiculo Vehiculos encontrados del cliente
         */

        public static function consultarVehiculoPorCliente($cedulaCliente){
            $vehiculoDAO=VehiculoDAO::obtenerVehiculoDAO(self::$conexionBD);
            $vehiculos=$vehiculoDAO->consultarVehiculosPorCliente($cedulaCliente);
            return $vehiculos;
        }

        /**
         * Cambia la conexión 
         */
        public static function setConexionBD($conexionBD)
            {
                self::$conexionBD = $conexionBD;
            }

    }

?>