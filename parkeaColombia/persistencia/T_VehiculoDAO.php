<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/T_Vehiculo.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Clase Dao Tipo Vehiculo
 */
class T_VehiculoDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase T_VehiculoDAO
	 * @var [TarjetaDAO]
	 */
	private static $T_VehiculoDAO;


	/**
	 * Constructor de la clase
	 */

	private 	function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Asignacion]  Nombre del tipo de vehiculo
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM T_VEHICULO WHERE cod_tipo_vehiculo=$codigo";
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
  $row=mysqli_fetch_array($result);


return $row["nom_tipo_vehiculo"];

}
/**
 * Crea una nuevo tipo de Vehiculo en la base de datos
 * @param  T_Vehiculo $t_vehiculoNuevo
 * @return void
 */
public function crear ($t_vehiculoNuevo){
	$sentencia="INSERT INTO T_VEHICULO VALUES(".$t_vehiculoNuevo->getCod_t_vehiculo().",".$t_vehiculoNuevo->getNom_t_vehiculo().")";
	mysqli_query($this->conexion, $sentencia);
}

/**
 * Modifica un tipo de Vehiculo ingresada por parámetro
 * @param  [T_Vehiculo] $pT_Vehiculo ingresada por parámetro
 * @return void
 */
public function modificar($pT_Vehiculo){
	$sentencia="UPDATE T_VEHICULO SET nom_t_vehiculo=".$pT_Vehiculo->getNom_t_vehiculo()."' WHERE cod_t_vehiculo='".$pT_Vehiculo->getCod_t_vehiculo();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de Tipo Vehiculo
 * @return [T_VehiculoArray]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM T_VEHICULO";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$T_VehiculoArray = array();

	while ($row = mysqli_fetch_array($result)) {
		$T_Vehiculo = new T_Vehiculo();
		$T_Vehiculo->setCod_t_vehiculo($row["cod_t_vehiculo"]);
    $T_Vehiculo->setNom_t_vehiculo($row["nom_t_vehiculo"]);
		array_push($T_VehiculoArray,$T_Vehiculo);
	}
	return $T_VehiculoArray;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerT_VehiculoDAO($conexion) {
            if(self::$T_VehiculoDAO == null) {
                self::$T_VehiculoDAO = new T_VehiculoDAO($conexion);
            }

            return self::$T_VehiculoDAO;
        }

}


?>
