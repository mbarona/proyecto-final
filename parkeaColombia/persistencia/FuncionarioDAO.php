<?php
/**
 * Archivo de conexión a la base de datos
 */
//require_once('../persistencia/util/Conexion.php');
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
/**
 * Archivo de entidad
 */
//require_once('../negocio/Funcionario.php');

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Funcionario.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');
require_once('ParqueaderoDAO.php');

/**
 * Dao para los clientes
 */
class FuncionarioDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase FuncionarioDAO
	 * @var [FuncionarioDAO]
	 */
	private static $funcionarioDAO;


	/**
	 * Constructor de la clase
	 */

	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Funcionario] Funcionario encontrado
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM FUNCIONARIO WHERE cedula_funcionario=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$funcionario=new Funcionario();
$funcionario->setIdentificacion($row["cedula_funcionario"]);
$funcionario->setNombre($row["nom_funcionario"]);
$funcionario->setEmail($row["email_funcionario"]);
$funcionario->setContrasena($row["password_funcionario"]);
$funcionario->setEstado($row["estado"]);
$parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO($this->conexion);
$parqueadero=$parqueaderoDAO->consultar($row["cod_parqueadero"]);

$funcionario->setParqueadero($parqueadero);
$funcionario->setIntentosFallidos($row["intentos_fallidos"]);

return $funcionario;

}


public function consultarEmail($correo){
	$sentencia="SELECT * FROM FUNCIONARIO WHERE email_funcionario='$correo'";
	if(!$result=mysqli_query($this->conexion,$sentencia))die();

$row=mysqli_fetch_array($result);
$funcionario=new Funcionario();
$funcionario->setIdentificacion($row["cedula_funcionario"]);
$funcionario->setNombre($row["nom_funcionario"]);
$funcionario->setEmail($row["email_funcionario"]);
$funcionario->setContrasena($row["password_funcionario"]);
$funcionario->setEstado($row["estado"]);

$parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO($this->conexion);
$parqueadero=$parqueaderoDAO->consultar($row["cod_parqueadero"]);

$funcionario->setParqueadero($parqueadero);
$funcionario->setIntentosFallidos($row["intentos_fallidos"]);

return $funcionario;

}


/**
 * Crea una nuevo funcionario en la base de datos
 * @param  Funcionario $funcionarioNuevo
 * @return void
 */
public function crear ($funcionarioNuevo){
	//orden: cedula_funcionario,nom_funcionario,email_funcionario,cod_parqueadero,password_funcionario,estado,intentos_fallidos
	$password = "#".password_hash($funcionarioNuevo->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="INSERT INTO FUNCIONARIO VALUES(".$funcionarioNuevo->getIdentificacion().",'".$funcionarioNuevo->getNombre()."','".$funcionarioNuevo->getEmail()."',".$funcionarioNuevo->getParqueadero().",'".$password."',1,0);";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica un funcionario ingresado por parámetro
 * @param  Funcionario $funcionario Funcionario ingresado por parámetro
 * @return void
 */
public function modificar($funcionario){
	#$password = "#".password_hash($funcionario->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="UPDATE FUNCIONARIO SET nom_funcionario='".$funcionario->getNombre()."',
	 email_funcionario ='".$funcionario->getEmail()."', password_funcionario='".$funcionario->getContrasena()."',
	 cod_parqueadero=".$funcionario->getParqueadero()->getCodParqueadero().", estado=".$funcionario->getEstado().",
	 intentos_fallidos=".$funcionario->getIntentosFallidos()." WHERE cedula_funcionario= ".$funcionario->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}
/**
 * Modifica el estado de un funcionario ingresado por parámetro
 * @param  Funcionario $funcionario Funcionario ingresado por parámetro
 * @return void
 */
public function cambiarEstado($funcionario){
	$sentencia="UPDATE FUNCIONARIO SET estado=".$funcionario->getEstado()." WHERE cedula_funcionario= ".$funcionario->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de funcionario
 * @return [Funcionarios]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM FUNCIONARIO";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$funcionarios = array();

	while ($row = mysqli_fetch_array($result)) {
		$funcionario=new Funcionario();
        $funcionario->setIdentificacion($row["cedula_funcionario"]);
        $funcionario->setNombre($row["nom_funcionario"]);
        $funcionario->setEmail($row["email_funcionario"]);
        $funcionario->setContrasena($row["password_funcionario"]);
        $funcionario->setEstado($row["estado"]);
		$funcionario->setParqueadero($row["cod_parqueadero"]);
		$funcionario->setIntentosFallidos($row["intentos_fallidos"]);
		array_push($funcionarios,$funcionario);
	}
	return $funcionarios;
}
/**
 * Lista todos los funcionarios por una zona que estan en la tabla de funcionarios
 * @param codzona codigo de la zona de los funcionarios a listar
 * @return [Parqueaderos]
 */
public function listarFuncionariosZona($codzona){
	$sentencia="select cedula_funcionario, nom_funcionario, email_funcionario, FUNCIONARIO.cod_parqueadero, FUNCIONARIO.estado from FUNCIONARIO, PARQUEADERO, ZONA where FUNCIONARIO.cod_parqueadero = PARQUEADERO.cod_parqueadero and PARQUEADERO.cod_zona = ZONA.cod_zona and ZONA.cod_zona=".$codzona;
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$funcionarios = array();

	while ($row = mysqli_fetch_array($result)) {
		$funcionario=new Funcionario();
        $funcionario->setIdentificacion($row["cedula_funcionario"]);
        $funcionario->setNombre($row["nom_funcionario"]);
        $funcionario->setEmail($row["email_funcionario"]);
        $funcionario->setEstado($row["estado"]);
		$funcionario->setParqueadero($row["cod_parqueadero"]);
		array_push($funcionarios,$funcionario);
	}
	return $funcionarios;
}
/**
 * Lista todos la cantidad de funcionarios por una zona que estan en la tabla de funcionarios
 * @param codzona codigo de la zona de los funcionarios a listar
 * @return [Funcionarios]
 */
public function cantidadFuncionariosZona($codzona){
	$sentencia="select count(cedula_funcionario) as total from FUNCIONARIO, PARQUEADERO, ZONA where FUNCIONARIO.cod_parqueadero = PARQUEADERO.cod_parqueadero and PARQUEADERO.cod_zona = ZONA.cod_zona and ZONA.cod_zona=".$codzona;
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$total = 0;

	while ($row = mysqli_fetch_array($result)) {
		$total=$row['total'];
	}
	return $total;
}
/**
 * Lista todos la cantidad de funcionarios por una parqueadero que estan en la tabla de funcionarios
 * @param codparqueadero codigo del parqueadero de los funcionarios a listar
 * @return [int]
 */
public function cantidadFuncionariosParqueadero($codparqueadero){
	$sentencia="select count(cedula_funcionario) as total from FUNCIONARIO,PARQUEADERO where FUNCIONARIO.cod_parqueadero = PARQUEADERO.cod_parqueadero and PARQUEADERO.cod_parqueadero = ".$codparqueadero;
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$total = 0;

	while ($row = mysqli_fetch_array($result)) {
		$total=$row['total'];
	}
	return $total;
}
/**
 * Lista el numero de funcionarios que hay en la base de datos
 * @return [int] numero de funcionarios
 */
public function totalFuncionarios(){
	$sentencia="select count(*) from FUNCIONARIO";
	$result = mysqli_query($this->conexion, $sentencia);
	$total=0;
	while ($row = $result->fetch_assoc()) {
		$total=$row['count(*)'];
	}

	return $total;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerFuncionarioDAO($conexion_bd) {
            if(self::$funcionarioDAO == null) {
                self::$funcionarioDAO = new FuncionarioDAO($conexion_bd);
            }
            return self::$funcionarioDAO;
        }

}


?>
