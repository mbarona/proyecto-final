<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/** 
 * Archivo de entidad
 */
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/negocio/T_Parqueadero.php';

/**
 * Interfaz DAO%
 */
require_once('DAO.php');

/**
 * Dao para los t_parqueaderos
 */
class T_ParqueaderoDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase T_ParqueaderoDAO
	 * @var [T_ParqueaderoDAO]
	 */
	private static $t_parqueaderoDAO;


	/**
	 * Constructor de la clase
	 */
	
	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [T_Parqueadero]         T_Parqueadero encontrada
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM T_PARQUEADERO WHERE cod_tipo_parqueadero=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
        $row=mysqli_fetch_array($result);
        $t_parqueadero=new T_Parqueadero();
        $t_parqueadero->setCodTParqueadero($row["cod_tipo_parqueadero"]);
        $t_parqueadero->setNomTParqueadero($row["nom_tipo_parqueadero"]);

    return $t_parqueadero;

}

/**
 * Crea una nueva t_parqueadero en la base de datos
 * @param  T_Parqueadero $t_parqueaderonuevo 
 * @return void
 */
public function crear ($t_parqueaderonuevo){
	$sentencia="INSERT INTO T_PARQUEADERO VALUES(".$t_parqueaderonuevo->getCodTParqueadero().",'".$t_parqueaderonuevo->getNomTParqueadero()."');";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una tipo de parqueadero ingresado por parámetro
 * @param  [T_Parqueadero] $t_parqueadero Tipo de Parqueadero ingresado por parámetro
 * @return void          
 */
public function modificar($t_parqueadero){
	$sentencia="UPDATE T_PARQUEADERO SET nom_tipo_parqueadero=".$t_parqueadero->getNomTParqueadero()." WHERE cod_tipo_parqueadero=".$t_parqueadero->getCodTParqueadero();


	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de t_parqueaderos
 * @return [T_Parqueadero] 
 */
public function listarTodo(){
	$sentencia="SELECT * FROM T_PARQUEADERO";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$t_parqueaderos = array();

	while ($row = mysqli_fetch_array($result)) {
		$t_parqueadero = new T_Parqueadero();
		$t_parqueadero->setCodTParqueadero($row["cod_tipo_parqueadero"]);
        $t_parqueadero->setNomTParqueadero($row["nom_tipo_parqueadero"]);
        array_push($t_parqueaderos,$t_parqueadero);
	}
	return $t_parqueaderos;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerT_ParqueaderoDAO($conexion_bd) {
            if(self::$t_parqueaderoDAO == null) {
                self::$t_parqueaderoDAO = new T_ParqueaderoDAO($conexion_bd);
            }
            return self::$t_parqueaderoDAO;
        }
}

?>