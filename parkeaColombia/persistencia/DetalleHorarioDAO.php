<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Detalle_Horario.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para las detalleHorarioS
 */
class DetalleHorarioDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase DetalleHorarioDAO
	 * @var [DetalleHorarioDAO]
	 */
	private static $detalleHorarioDAO;


	/**
	 * Constructor de la clase
	 */

	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [detalleHorario]  DetalleHorario encontrado
 */
public function consultar($codigoParametro){
	$sentencia="SELECT * FROM DETALLE_HORARIO where cod_parametro=".$codigoParametro;
	if(!$result = mysqli_query($this->conexion, $sentencia))die();
	$detalleHorarios = array();
	while ($row = mysqli_fetch_array($result)) {
		$detalleHorario=new Detalle_Horario();
		$detalleHorario->setCod_dia($row['cod_dia']);
		$detalleHorario->setHora_inicio($row['hora_inicio']);
		$detalleHorario->setHora_final($row['hora_final']);
		$detalleHorario->setCod_parametro($row['cod_parametro']);
		array_push($detalleHorarios,$detalleHorario);
	}
	return $detalleHorarios;

}



/**
 * Crea una nuevo detalleHorario en la base de datos
 * @param  DetalleHorario $detalleHorarioNuevo
 * @return void
 */
public function crear ($detalleHorarioNuevo){
	$sentencia="INSERT INTO DETALLE_HORARIO VALUES(".$detalleHorarioNuevo->getCod_Dia().",'".$detalleHorarioNuevo->getHora_Inicio()."','".$detalleHorarioNuevo->getHora_Final()."',".$detalleHorarioNuevo->getCod_Parametro().");";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una detalleHorario ingresada por parámetro
 * @param  [detalleHorario] $detalleHorario detalleHorario ingresada por parámetro
 * @return void
 */
public function modificar($detalleHorario){
	$sentencia="UPDATE DETALLE_HORARIO SET  hora_inicio ='".$detalleHorario->getHora_inicio()."', hora_final = '".$detalleHorario->getHora_final()."' WHERE cod_parametro=".$detalleHorario->getCod_parametro()." and cod_dia =".$detalleHorario->getCod_dia();

	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de DetalleHorarios
 * @return [DetalleHorario]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM DETALLE_HORARIO";
	if(!$result = mysqli_query($this->conexion, $sentencia))die();
	$detalleHorarios = array();
	while ($row = mysqli_fetch_array($result)) {
		$detalleHorario=new Detalle_Horario();
		$detalleHorario->setCod_dia($row['cod_dia']);
		$detalleHorario->setHora_inicio($row['hora_inicio']);
		$detalleHorario->setHora_final($row['hora_final']);
		$detalleHorario->setCod_parametro($row['cod_parametro']);
		array_push($detalleHorarios,$detalleHorario);
	}
	return $detalleHorarios;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerDetalleHorarioDAO($conexion_bd){
            if(self::$detalleHorarioDAO == null) {
                self::$detalleHorarioDAO = new DetalleHorarioDAO($conexion_bd);
            }

            return self::$detalleHorarioDAO;
        }

}


?>
