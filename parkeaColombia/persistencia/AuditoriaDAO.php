<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
/**
 * Archivo de entidad
 */
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/negocio/Auditoria.php';
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/AuditoriaDAO.php';

date_default_timezone_set("America/Bogota");

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para los auditorias
 */
class AuditoriaDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase AuditoriaDAO
	 * @var [AuditoriaDAO]
	 */
	private static $auditoriaDAO;


	/**
	 * Constructor de la clase
	 */

	 function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion,"utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [String] $ccpnsulta
 * @return [String] Auditorias que contengan la consulta
 */
public function buscarJS($consulta){
	$q = mysqli_real_escape_string($this->conexion,$consulta);
	$sentencia="SELECT * FROM AUDITORIA WHERE cedula_usuario LIKE'%$q%' OR tabla LIKE'%$q%' OR cod_afectado LIKE'%$q%' OR tipo_operacion LIKE'%$q%' OR fecha LIKE'%$q%'";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$auditorias = array();

	while ($row = mysqli_fetch_array($result)) {
		$auditoria=new Auditoria();
		$auditoria->setCedula_usuario($row["cedula_usuario"]);
		$auditoria->setTabla($row["tabla"]);
		$auditoria->setCod_afectado($row["cod_afectado"]);
		$auditoria->setTipo_operacion($row["tipo_operacion"]);
		$auditoria->setFecha($row["fecha"]);
		array_push($auditorias,$auditoria);
	}
	return $auditorias;
}
/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del usuario a consultar]
 * @return Auditoria[] Auditorias encontrados de un usuario
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM AUDITORIA WHERE cedula_usuario=".$codigo;
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$auditorias = array();

	while ($row = mysqli_fetch_array($result)) {
		$auditoria=new Auditoria();
		$auditoria->setCedula_usuario($row["cedula_usuario"]);
		$auditoria->setTabla($row["tabla"]);
		$auditoria->setCod_afectado($row["cod_afectado"]);
		$auditoria->setTipo_operacion($row["tipo_operacion"]);
		$auditoria->setFecha($row["fecha"]);
		array_push($auditorias,$auditoria);
	}
	return $auditorias;
}
/**
 * Realiza la consulta de un auditorias entre dos fechas pasadas por parametro
 * @param  [Date] $fecha_inicial Fecha desde la que se quiere ver las auditorias
 * @param  [Date] $fecha_final Fecha hasta donde se quieren ver las auditorias

 * @return Auditoria[] Auditorias encontrados entre las fechas indicadas
 */
public function consultarPorFechas($fecha_inicial,$fecha_final){
	$sentencia="SELECT * FROM AUDITORIA WHERE fecha between '".$fecha_inicial."' and '".$fecha_final."'";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$auditorias = array();

	while ($row = mysqli_fetch_array($result)) {
		$auditoria=new Auditoria();
		$auditoria->setCedula_usuario($row["cedula_usuario"]);
		$auditoria->setTabla($row["tabla"]);
		$auditoria->setCod_afectado($row["cod_afectado"]);
		$auditoria->setTipo_operacion($row["tipo_operacion"]);
		$auditoria->setFecha($row["fecha"]);
		array_push($auditorias,$auditoria);
	}
	return $auditorias;
}


/**
 * Crea una nuevo auditoria en la base de datos
 * @param  Auditoria $auditoriaNuevo
 * @return void
 */
public function crear ($auditoriaNuevo){
    $fecha = date('Y-m-d h:i:s');
    $sentencia="INSERT INTO AUDITORIA(cedula_usuario,tabla,cod_afectado,tipo_operacion,fecha) VALUES(".$auditoriaNuevo->getCedula_usuario().",'".$auditoriaNuevo->getTabla()."',".$auditoriaNuevo->getCod_afectado().",'".$auditoriaNuevo->getTipo_operacion()."','".$fecha."')";
    mysqli_query($this->conexion, $sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de auditoria
 * @return [Auditorias]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM AUDITORIA";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$auditorias = array();

	while ($row = mysqli_fetch_array($result)) {
		$auditoria=new Auditoria();
		$auditoria->setCedula_usuario($row["cedula_usuario"]);
		$auditoria->setTabla($row["tabla"]);
		$auditoria->setCod_afectado($row["cod_afectado"]);
		$auditoria->setTipo_operacion($row["tipo_operacion"]);
		$auditoria->setFecha($row["fecha"]);
		array_push($auditorias,$auditoria);
	}
	return $auditorias;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerAuditoriaDAO($conexion) {
            if(self::$auditoriaDAO == null) {
                self::$auditoriaDAO = new AuditoriaDAO($conexion);
            }

            return self::$auditoriaDAO;
		}
	
	public function modificar($objeto)
	{
		return $objeto;
	} 

}


?>
