<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Zona.php';

/**
 * Interfaz DAO%
 */
require_once ('DAO.php');

/**
 * Dao para las zonas
 */
class ZonaDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase ZonaDAO
	 * @var [ZonaDAO]
	 */
	private static $zonaDAO;


	/**
	 * Constructor de la clase
	 */

	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Zona]         Zona encontrada
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM ZONA WHERE cod_zona=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
        $row=mysqli_fetch_array($result);
        $zona=new Zona();
        $zona->setCodZona($row["cod_zona"]);
        $zona->setNomZona($row["nom_zona"]);
        $zona->setCodCiudad($row["cod_ciudad"]);

    return $zona;

}

/**
 * Crea una nueva zona en la base de datos
 * @param  Zona $zonanueva
 * @return void
 */
public function crear ($zonanueva){
	$sentencia="INSERT INTO ZONA(nom_zona, cod_ciudad) VALUES('".$zonanueva->getNomZona()."',".$zonanueva->getCodCiudad().");";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una zona ingresada por parámetro
 * @param  [Zona] $zona Zona ingresada por parámetro
 * @return void
 */
public function modificar($zona){
	$sentencia="UPDATE ZONA SET nom_zona=".$zona->getNomZona().",cod_ciudad='".$zona->getCodCiudad()."' WHERE cod_zona=".$zona->getCodCiudad();


	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de zonas
 * @return [Zona]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM ZONA";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$zonas = array();

	while ($row = mysqli_fetch_array($result)) {
		$zona = new Zona();
		$zona->setCodZona($row["cod_zona"]);
        $zona->setNomZona($row["nom_zona"]);
        $zona->setCodCiudad($row["cod_ciudad"]);
        array_push($zonas,$zona);
	}
	return $zonas;
}

/**
 * Lista el numero de gerentes que hay en la base de datos
 * @return [int] numero de gerentes
 */
public function totalZonas(){
	$sentencia="select count(*) from ZONA";
	$result = mysqli_query($this->conexion, $sentencia);
	$total=0;
	while ($row = $result->fetch_assoc()) {
		$total=$row['count(*)'];
	}

	return $total;
}
/**
* Lista todas las zonas con la ciudad donde esta
* @return zonas Lista de todas las zonas con la ciudad
*/
public function listarZonaCiudad(){
	$sentencia="SELECT cod_zona, nom_zona, nom_ciudad FROM ZONA, CIUDAD WHERE ZONA.cod_ciudad = CIUDAD.cod_ciudad";
	if(!$zonas = mysqli_query($this->conexion, $sentencia)) die();
		return $zonas;
}
/**
* Lista todas las zonas con la ciudad donde esta
* @return zonas Lista de todas las zonas con la ciudad
*/
public function listarZonaPorCiudadDada($cod_ciudad){
	$sentencia="SELECT * FROM ZONA WHERE ZONA.cod_ciudad =".$cod_ciudad;
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$zonas = array();

	while ($row = mysqli_fetch_array($result)) {
		$zona = new Zona();
		$zona->setCodZona($row["cod_zona"]);
        $zona->setNomZona($row["nom_zona"]);
        $zona->setCodCiudad($row["cod_ciudad"]);
        array_push($zonas,$zona);
	}
	return $zonas;

}

/**
* Lista todas las zonas con la ciudad donde esta
* @return zonas Lista de todas las zonas con la ciudad
*/
public function listarZonasSinGerente(){
	$sentencia="SELECT ZONA.cod_zona, nom_zona, nom_ciudad FROM ZONA, CIUDAD WHERE NOT EXISTS (SELECT GERENTE.cod_zona FROM GERENTE WHERE ZONA.cod_zona = GERENTE.cod_zona)  AND ZONA.cod_ciudad = CIUDAD.cod_ciudad;";
	if(!$zonas = mysqli_query($this->conexion, $sentencia)) die();
		return $zonas;

}

/**
* Lista todas las zonas de una ciudad que tengan al menos un parqueadero
* @return zonas Lista de todas las zonas de una ciudad que tengan al menos un parqueadero
*/
public function listarZonasParqueadero($cod_ciudad){
	$sentencia="SELECT DISTINCT ZONA.cod_zona, nom_zona, cod_ciudad FROM ZONA,PARQUEADERO  WHERE ZONA.cod_zona = PARQUEADERO.cod_zona AND ZONA.cod_ciudad =".$cod_ciudad;
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$zonas = array();

	while ($row = mysqli_fetch_array($result)) {
		$zona = new Zona();
		$zona->setCodZona($row["cod_zona"]);
        $zona->setNomZona($row["nom_zona"]);
        $zona->setCodCiudad($row["cod_ciudad"]);
        array_push($zonas,$zona);
	}
	return $zonas;
}
/**
* Lista todas las zonas de una ciudad que tengan al menos un parqueadero
* @return zonas Lista de todas las zonas de una ciudad que tengan al menos un parqueadero
*/
public function listarZonasParqueaderoSinCiudad(){
	$sentencia="SELECT DISTINCT ZONA.cod_zona, nom_zona,cod_ciudad FROM ZONA,PARQUEADERO  WHERE ZONA.cod_zona = PARQUEADERO.cod_zona";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$zonas = array();

	while ($row = mysqli_fetch_array($result)) {
		$zona = new Zona();
		$zona->setCodZona($row["cod_zona"]);
        $zona->setNomZona($row["nom_zona"]);
        $zona->setCodCiudad($row["cod_ciudad"]);
        array_push($zonas,$zona);
	}
	return $zonas;
}
/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerZonaDAO($conexionBD) {
            if(self::$zonaDAO == null) {
                self::$zonaDAO = new ZonaDAO($conexionBD);
            }
            return self::$zonaDAO;
        }
}

?>
