<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Dias.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para las diaS
 */
class DiaDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase diasDAO
	 * @var [diasDAO]
	 */
	private static $diaDAO;


	/**
	 * Constructor de la clase
	 */

	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [dia]  dias encontrado
 */
public function consultar($cod){
	$sentencia="SELECT * FROM DIAS where cod_dia=".$cod;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
	$row=mysqli_fetch_array($result);
	$dia=new Dias();
	$dia->setCodDia($row['cod_dia']);
	$dia->setNomDia($row['nom_dia']);


return $dia;

}



/**
 * Crea una nuevo dia en la base de datos
 * @param  dias $diaNuevo
 * @return void
 */
public function crear ($diaNuevo){
	$sentencia="INSERT INTO DIAS VALUES(".$diaNuevo->getCodDia().",".$diaNuevo->getNomDia()."');";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una dia ingresada por parámetro
 * @param  [dia] $dia dia ingresada por parámetro
 * @return void
 */
public function modificar($dia){
	$sentencia="UPDATE DIAS SET  nom_dia='".$dia->getNomDia."' WHERE cod_dia=".$dia->getCodDia();

	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que están en la tabla de dias
 * @return [dias]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM DIAS";
	if(!$result = mysqli_query($this->conexion, $sentencia))die();
	$dias = array();
	while ($row = mysqli_fetch_array($result)) {
		$dia=new Dias();
		$dia->setCodDia($row['cod_dia']);
		$dia->setNomDia($row['nom_dia']);
		array_push($dias,$dia);
	}
	return $dias;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerDiasDAO($conexion_bd){
            if(self::$diaDAO == null) {
                self::$diaDAO = new diaDAO($conexion_bd);
            }

            return self::$diaDAO;
        }

}


?>
