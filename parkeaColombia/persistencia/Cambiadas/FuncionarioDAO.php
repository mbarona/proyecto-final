<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $SERVER["DOCUMENT_ROOT"].'/parkeaColombia/persistencia/util/Conexion.php';

/** 
 * Archivo de entidad
 */
require_once $SERVER["DOCUMENT_ROOT"].'/parkeaColombia/negocio/Funcionario.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para los clientes
 */
class FuncionarioDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase GerenteDAO
	 * @var [GerenteDAO]
	 */
	private static $funcionarioDAO;


	/**
	 * Constructor de la clase
	 */
	
	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Funcionario]         Funcionario encontrado
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM FUNCIONARIO WHERE identificacion=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$funcionario=new Funcionario();
$funcionario=new Funcionario();
$funcionario->setIdentificacion($row["cedula_funcionario"]);
$funcionario->setNombre($row["nom_funcionario"]);
$funcionario->setEmail($row["email_funcionario"]);
$funcionario->setContrasena($row["password_funcionario"]);
$funcionario->setEstado($row["estado"]);
$funcionario->setParqueadero($row["cod_parqueadero"]);
$funcionario->setIntentosFallidos($row["intentos_fallidos"]);


return $funcionario;

}

/**
 * Crea una nuevo funcionario en la base de datos
 * @param  Funcionario $funcionarioNuevo
 * @return void
 */
public function crear ($funcionarioNuevo){
	//orden: cedula_funcionario,nom_funcionario,email_funcionario,cod_parqueadero,password_funcionario,estado,intentos_fallidos
	$password = password_hash($funcionarioNuevo->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="INSERT INTO FUNCIONARIO VALUES(".$funcionarioNuevo->getIdentificacion().",'".$funcionarioNuevo->getNombre()."','".$funcionarioNuevo->getEmail()."',".$funcionarioNuevo->getParqueadero().",'".$password."',1,0);";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una funcionario ingresado por parámetro
 * @param  Funcionario $funcionario Funcionario ingresado por parámetro
 * @return void          
 */
public function modificar($funcionario){
	$password = password_hash($funcionario->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="UPDATE Funcionario SET nom_funcionario='".$funcionario->getNombre()."', email_funcionario ='".$funcionario->getEmail()."', password_funcionario='".$password."', cod_parqueadero=".$funcionario->getParqueadero().", estado=".$funcionario->getEstado().", intentos_fallidos=".$funcionario->getIntentosFallidos()." WHERE cedula_funcionario= ".$funcionario->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de funcionario
 * @return [Funcionarios] 
 */
public function listarTodo(){
	$sentencia="SELECT * FROM FUNCIONARIO";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$funcionarios = array();

	while ($row = mysqli_fetch_array($result)) {
		$funcionario=new Funcionario();
        $funcionario->setIdentificacion($row["cedula_funcionario"]);
        $funcionario->setNombre($row["nom_funcionario"]);
        $funcionario->setEmail($row["email_funcionario"]);
        $funcionario->setContrasena($row["password_funcionario"]);
        $funcionario->setEstado($row["estado"]);
		$funcionario->setParqueadero($row["cod_parqueadero"]);
		$funcionario->setIntentosFallidos($row["intentos_fallidos"]);
		array_push($funcionarios,$funcionario);
	}
	return $funcionarios;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerGerenteDAO($conexion_bd) {
            if(self::$funcionarioDAO == null) {
                self::$funcionarioDAO = new FuncionarioDAO($this->conexion);
            }

            return self::$funcionarioDAO;
        }

}


?>