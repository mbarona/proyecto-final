<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

//require_once ('../persistencia/util/Conexion.php');
/** 
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Gerente.php';

//require_once ('../negocio/Gerente.php');
/**
 * Interfaz DAO
 */
require_once ('DAO.php');

/**
 * Dao para los gerentes
 */
class GerenteDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase GerenteDAO
	 * @var [GerenteDAO]
	 */
	private static $gerenteDAO;


	/**
	 * Constructor de la clase
	 */
	
	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $identificacion [id del objeto a consultar]
 * @return [Gerente]         Gerente encontrado
 */
public function consultar($identificacion){
	$sentencia="SELECT * FROM GERENTE WHERE identificacion=".$identificacion;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
	$row=mysqli_fetch_array($result);
	$gerente=new Gerente();
	$gerente->setIdentificacion($row["cedula_gerente"]);
	$gerente->setNombre($row["nom_gerente"]);
	$gerente->setEmail($row["email_gerente"]);
	$gerente->setContrasena($row["contraseña"]);
	$gerente->setEstado($row["password_gerente"]);
	$gerente->setZona($row["cod_zona"]);
	$gerente->setIntentosFallidos($row["intentos_fallidos"]);


return $gerente;

}

/**
 * Crea una nuevo gerente en la base de datos
 * @param  Gerente $gerenteNuevo
 * @return void
 */
public function crear ($gerenteNuevo){
	//orden: cedula_gerente, nom_gerente, email_gerente, estado, password_gerente, intentos_fallidos, cod_zona
	$password = password_hash($gerenteNuevo->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="INSERT INTO GERENTE VALUES(".$gerenteNuevo->getIdentificacion().",'".$gerenteNuevo->getNombre()."','".$gerenteNuevo->getEmail()."',1,'".$password."',0,".$gerenteNuevo->getZona().");";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una gerente ingresado por parámetro
 * @param  Gerente $gerente Gerente ingresado por parámetro
 * @return void          
 */
public function modificar($gerente){
	$password = password_hash($gerente->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="UPDATE GERENTE SET nom_gerente='".$gerente->getNombre()."', email_gerente ='".$gerente->getEmail()."', estado=".$gerente->getEstado().", password_gerente='".$password."', intentos_fallidos=".$gerente->getIntentosFallidos().", cod_zona=".$gerente->getZona()." WHERE cedula_gerente= ".$gerente->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de gerente
 * @return [Gerentes] 
 */
public function listarTodo(){
	$sentencia="SELECT * FROM Gerente";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$gerentes = array();

	while ($row = mysqli_fetch_array($result)) {
		$gerente=new Gerente();
		$gerente->setIdentificacion($row["cedula_gerente"]);
		$gerente->setNombre($row["nom_gerente"]);
		$gerente->setEmail($row["email_gerente"]);
		$gerente->setContrasena($row["contraseña"]);
		$gerente->setEstado($row["password_gerente"]);
		$gerente->setZona($row["cod_zona"]);
		$gerente->setIntentosFallidos($row["intentos_fallidos"]);
		array_push($gerentes,$gerente);
	}
	return $gerentes;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerGerenteDAO($conexionBD) {
            if(self::$gerenteDAO == null) {
                self::$gerenteDAO = new GerenteDAO($conexionBD);
            }

            return self::$gerenteDAO;
        }

}


?>