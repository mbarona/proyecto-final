<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $SERVER["DOCUMENT_ROOT"].'/parkeaColombia/persistencia/util/Conexion.php';

/** 
 * Archivo de entidad
 */
require_once $SERVER["DOCUMENT_ROOT"].'/parkeaColombia/negocio/Administrador.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para los administradors
 */
class AdministradorDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase administradorDAO
	 * @var [administradorDAO]
	 */
	private static $administradorDAO;


	/**
	 * Constructor de la clase
	 */
	
	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [administrador]         administrador encontrado
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM ADMINISTRADOR WHERE identificacion=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$administrador=new Administrador();
$administrador->setIdentificacion($row["identificacion"]);
$administrador->setNombre($row["nombre"]);
$administrador->setEmail($row["email"]);
$administrador->setContraseña($row["contraseña"]);

return $administrador;

}

/**
 * Crea una nuevo administrador en la base de datos
 * @param  administrador $administradorNuevo
 * @return void
 */
public function crear ($administradorNuevo){
	$sentencia="INSERT INTO ADMINISTRADOR VALUES(".$administradorNuevo->getIdentificacion().",'".$administradorNuevo->getNombre()."','".$administradorNuevo->getEmail()."','".$administradorNuevo->getContraseña().");";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una administrador ingresado por parámetro
 * @param  administrador $administrador administrador ingresado por parámetro
 * @return void          
 */
public function modificar($administrador){
	$sentencia="UPDATE ADMINISTRADOR SET nombre='".$administrador->getNombre()."', email ='".$administrador->getEmail()."','".$administrador->getContraseña()."', zona=".$administrador->getZona()." WHERE identificacion= ".$administrador->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de administrador
 * @return [administradors] 
 */
public function listarTodo(){
	$sentencia="SELECT * FROM ADMINISTRADOR";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$administradors = array();

	while ($row = mysqli_fetch_array($result)) {
		$administrador=new Administrador();
		$administrador->setIdentificacion($row["identificacion"]);
		$administrador->setNombre($row["nombre"]);
		$administrador->setEmail($row["email"]);
		$administrador->setContraseña($row["contraseña"]);

		array_push($administradors,$administrador);
	}
	return $administradors;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obteneradministradorDAO($conexion_bd) {
            if(self::$administradorDAO == null) {
                self::$administradorDAO = new administradorDAO($this->conexion);
            }

            return self::$administradorDAO;
        }

}


?>