<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/** 
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Parametro.php';


/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * 
 */
class ParametroDAO implements DAO
{
	
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase TarjetaDAO
	 * @var [TarjetaDAO]
	 */
	private static $parametroDAO;


	/**
	 * Constructor de la clase
	 */
	
	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}


	
/**
 * Crea un nuevo parámetro en la base de atos
 * @param  [Parámetro] $nuevoParametro Objeto de tipo parámetro que se va a crear en la base de datos
 * @param  [Parqueadero] $parqueadero    Objeto de tipo parqueadero 
 * @return void
 */
public function crear($nuevoParametro){
	$sentencia="INSERT INTO PARAMETRO (cod_parqueadero,tarifa,capacidad,capacidad_disponible) VALUES(".$nuevoParametro->getCodparqueadero().",".$nuevoParametro->getTarifa().",".$nuevoParametro->getCapacidad().",".$nuevoParametro->getCapacidadDisponible().")";
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Edita un parámetro ingresado por parámetro
 * @param  [Parametro] $parametro   Parámetro a modificar
 * @param  [Parqueadero] $parqueadero Parqueadero que tiene el parámetro asignado
 * @return [void] [description]
 */
public function modificar($parametro){
	$sentencia="UPDATE PARAMETRO SET cod_parqueadero=".$parametro->getCodParqueadero().", tarifa=".$parametro->getTarifa().", capacidad=".$parametro->getCapacidad().", capacidad_disponible=".$parametro->getCapacidadDisponible()." WHERE cod_parametro=".$parametro->getCodigo();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Obtiene un parámetro
 * @param  [int] $codigo Código del parámetro
 * @return [Parametro]        Parámetro encontrado
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM PARAMETRO WHERE cod_parametro=".$codigo;
	if(!$result = mysqli_query($this->conexion, $sentencia))die();
	$row=mysqli_fetch_array($result);
	$parametro= new Parametro();
	$parametro->setCodigo($row["cod_parametro"]);
	$parametro->setCodParqueadero($row["cod_parqueadero"]);
	$parametro->setTarifa($row["tarifa"]);
	$parametro->setCapacidad($row["capacidad"]);
	$parametro->setCapacidadDisponible($row["capacidad_disponible"]);

	return $parametro;
}

/**
 * Obtiene la lista de todos los parámetros
 * @return [Parametro[]] Lista de parámetros
 */
public function listarTodo(){
	$sentencia="SELECT * FROM PARAMETRO";
	if(!$result=mysqli_query($this->conexion,$sentencia))die();

	$parametros=array();
	while($row=mysqli_fetch_array($result)){
		$parametro= new Parametro();
		$parametro->setCodigo($row["cod_parametro"]);
		$parametro->setCapacidad($row["cod_parqeuadero"]);
		$parametro->setTarifa($row["tarifa"]);
		$parametro->setCapacidad($row["capacidad"]);
		$parametro->setCapacidadDisponible($row["capacidad_disponible"]);

		$parametros[]=$parametro;
	}
	return $parametros;
}

/**
 * Obtiene los parametros de un parqueadero específico
 * @param $codigo Código del parqueadero del cul se consultan los parámetros
 * @return $parametros  Todos los parámetros asociados a ese parqueadero
 */
public function parametroDeUnParqueadero($codigo){
	$sentencia= "SELECT * FROM PARAMETRO WHERE PARAMETRO.cod_parqueadero=".$codigo;
	if(!$result = mysqli_query($this->conexion, $sentencia))die();
	$row=mysqli_fetch_array($result);
	$parametro= new Parametro();
	$parametro->setCodigo($row["cod_parametro"]);
	$parametro->setCodParqueadero($row["cod_parqueadero"]);
	$parametro->setTarifa($row["tarifa"]);
	$parametro->setCapacidad($row["capacidad"]);
	$parametro->setCapacidadDisponible($row["capacidad_disponible"]);

	return $parametro;
}
	

/**
 * Lista el ultimo parametro registrado
 * @return [Parametro] 
 */
public function listarUltimo(){
	$sentencia="SELECT cod_parametro FROM PARAMETRO  ORDER by cod_parametro DESC LIMIT 1";
	if(!$result = mysqli_query($this->conexion, $sentencia))die();
	$row=mysqli_fetch_array($result);
	$parametro= new Parametro();
	$parametro->setCodigo($row["cod_parametro"]);

	return $parametro;
	
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerParametroDAO($conexion) {
		if(self::$parametroDAO == null) {
			self::$parametroDAO = new ParametroDAO($conexion);
		}

		return self::$parametroDAO;
	}

}
?>