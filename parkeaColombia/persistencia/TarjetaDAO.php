<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/** 
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Tarjeta.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para las tarjetas
 */
class TarjetaDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase TarjetaDAO
	 * @var [TarjetaDAO]
	 */
	private static $tarjetaDAO;


	/**
	 * Constructor de la clase
	 */
	
	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Tarjeta]         Tarjeta encontrada
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM TARJETA WHERE numero_tarjeta=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$tarjeta=new Tarjeta();
$tarjeta->setNumeroTarjeta($row["numero_tarjeta"]);
$tarjeta->setCodigoVerificacion($row["codigo_verificacion_tarjeta"]);
$tarjeta->setFechaVencimiento($row["fecha_vencimiento"]);

return $tarjeta;

}

/**
 * Crea una nueva tarjeta en la base de datos
 * @param  Tarjeta $tarjetanueva 
 * @return void
 */
public function crear ($tarjetanueva){
	$sentencia="INSERT INTO TARJETA VALUES(".$tarjetanueva->getNumeroTarjeta().",".$tarjetanueva->getCodigoVerificacion().",'".$tarjetanueva->getFechaVencimiento()."');";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una tarjeta ingresada por parámetro
 * @param  [Tarjeta] $tarjeta Tarjeta ingresada por parámetro
 * @return void          
 */
public function modificar($tarjeta){
	$sentencia="UPDATE TARJETA SET codigo_verificacion=".$tarjeta->getCodigoVerificacion().", fecha_vencimiento='".$tarjeta->getFechaVencimiento()."' WHERE numero_tarjeta=".$tarjeta->getNumeroTarjeta();


	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de tarjetas
 * @return [Tarjeta] 
 */
public function listarTodo(){
	$sentencia="SELECT * FROM TARJETA";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$tarjetas = array();

	while ($row = mysqli_fetch_array($result)) {
		$tarjeta = new Tarjeta();
		$tarjeta->setNumeroTarjeta($row["numero_tarjeta"]);
		$tarjeta->setCodigoVerificacion($row["codigo_verifiacion"]);
		$tarjeta->setFechaVencimiento($row["fecha_vencimiento"]);
		array_push($tarjetas,$tarjeta);
	}
	return $tarjetas;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerTarjetaDAO($conexion) {
            if(self::$tarjetaDAO == null) {
                self::$tarjetaDAO = new TarjetaDAO($conexion);
            }

            return self::$tarjetaDAO;
        }

}


?>