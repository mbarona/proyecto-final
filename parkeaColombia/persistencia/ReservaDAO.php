<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/ParqueaderoDAO.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/VehiculoDAO.php';

/** 
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Reserva.php';
 
/**
 * Interfaz DAO
 */
require_once('DAO.php'); 

/**
 * Dao para las reservas
 */
class ReservaDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase ReservaDAO
	 * @var [ReservaDAO]
	 */
	private static $reservaDAO;


	/**
	 * Constructor de la clase
	 */
	
	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [reserva]         reserva encontrada
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM RESERVA WHERE cod_reserva=".$codigo;

	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$reserva=new Reserva();
$parqueaderoDAO=ParqueaderoDAO::obtenerParqueaderoDAO($this->conexion);
$parqueadero=$parqueaderoDAO->consultar($row["cod_parqueadero"]);
$vehiculoDAO=VehiculoDAO::obtenerVehiculoDAO($this->conexion);
$vehiculo=$vehiculoDAO->consultar($row["placa_vehiculo"]);
$reserva->setParqueadero($parqueadero);
$reserva->setHoraInicial($row["fecha_hora_inicial"]);
$reserva->setHoraFinal($row["fecha_hora_final"]);
$reserva->setEstado($row["cod_estado_reserva"]);
$reserva->setVehiculo($vehiculo);
$reserva->setHora_ingreso_real($row["hora_ingreso_real"]);
$reserva->setHora_salida_real($row["hora_salida_real"]);

return $reserva;

}

/**
 * Crea una nueva reserva en la base de datos
 * @param  reserva $reservanueva 
 * @return void
 */
public function crear ($reservanueva){
	if ($reservanueva->getHoraFinal()!="null") {
		$sentencia="INSERT INTO RESERVA VALUES(0,".$reservanueva->getParqueadero()->getCodParqueadero().",'".$reservanueva->getHoraInicial()."','".$reservanueva->getHoraFinal()."',".$reservanueva->getEstado().",'".$reservanueva->getVehiculo()->getPlaca()."',null,'".$reservanueva->getHora_ingreso_real()."');";

	mysqli_query($this->conexion, $sentencia);
	}
	else{
	$sentencia="INSERT INTO RESERVA VALUES(0,".$reservanueva->getParqueadero()->getCodParqueadero().",'".$reservanueva->getHoraInicial()."',null,".$reservanueva->getEstado().",'".$reservanueva->getVehiculo()->getPlaca()."',null,'".$reservanueva->getHora_ingreso_real()."');";

	mysqli_query($this->conexion, $sentencia);
	}
}

/**
 * Modifica una reserva ingresada por parámetro
 * @param  [reserva] $reserva reserva ingresada por parámetro
 * @return void          
 */
public function modificar($reserva){
$fechaIngreso=$reserva->getHoraInicial();
if($reserva->getHora_salida_real()==null || $reserva->getHora_salida_real()=="" || $reserva->getHoraFinal()=="" || $reserva->getHoraFinal()==null){
	$sentencia="UPDATE RESERVA SET fecha_hora_inicial='".$reserva->getHoraInicial()."', fecha_hora_final=null, cod_estado_reserva=".$reserva->getEstado().", placa_vehiculo='".$reserva->getVehiculo()->getPlaca()."', hora_ingreso_real='".$reserva->getHora_ingreso_real()."', hora_salida_real=null  WHERE cod_parqueadero=".$reserva->getParqueadero()->getCodParqueadero()." 
	AND DATE_FORMAT(fecha_hora_inicial,'%Y-%m-%d %H')=DATE_FORMAT('$fechaIngreso','%Y-%m-%d %H') AND placa_vehiculo='".$reserva->getVehiculo()->getPlaca()."'";
}else{
	$salidaReal=$reserva->getHora_salida_real();
	$sentencia="UPDATE RESERVA SET fecha_hora_inicial='".$reserva->getHoraInicial()."', fecha_hora_final='".$reserva->getHoraFinal()."', cod_estado_reserva=".$reserva->getEstado().", placa_vehiculo='".$reserva->getVehiculo()->getPlaca()."', hora_ingreso_real='".$reserva->getHora_ingreso_real()."', hora_salida_real='".$salidaReal."'  WHERE cod_parqueadero=".$reserva->getParqueadero()->getCodParqueadero()." 
	AND DATE_FORMAT(fecha_hora_inicial,'%Y-%m-%d %H')=DATE_FORMAT('$fechaIngreso','%Y-%m-%d %H') AND placa_vehiculo='".$reserva->getVehiculo()->getPlaca()."'";
}
	
	
	mysqli_query($this->conexion,$sentencia);

} 

/**
 * Modifica el estado de una reserva
 * @param  [reserva] $reserva reserva ingresada por parámetro
 * @return void          
 */
public function modificarEstado($estado, $placaVeh){
	$sentencia="UPDATE RESERVA SET cod_estado_reserva=".$estado." WHERE placa_vehiculo='".$placaVeh."'";
	mysqli_query($this->conexion,$sentencia);
}


/**
 * Lista todos los objetos que se están en la tabla de reservas
 * @return [reserva] 
 */
public function listarTodo(){
	$sentencia="SELECT * FROM RESERVA";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$reservas = array();

	while ($row = mysqli_fetch_array($result)) {
		$reserva = new Reserva();
		$reserva->setParqueadero($row["cod_parqueadero"]);
		$reserva->setHoraInicial($row["fecha_hora_inicial"]);
		$reserva->setHoraFinal($row["fecha_hora_final"]);
		$reserva->setEstado($row["cod_estado_reserva"]);
		$reserva->setVehiculo($row["placa_vehiculo"]);
		$reserva->setHora_ingreso_real($row["hora_ingreso_real"]);
		$reserva->setHora_salida_real($row["hora_salida_real"]);
		array_push($reservas,$reserva);
	}
	return $reservas;
}
/**
 * Consulta una reserva de un vehiculo, en un parqueadero especifico y fecha especifica
 * @param $placaVehiculo Placa del vehiculo relacionado con la reserva
 * @param $codParqueadero Codigo del parqueadero que está realizando la consulta
 * @param $fechaActual Fecha en la que se está haciendo la consulta
 * @return $resultado Código de la reserva de ese vehiculo, en ese parqueadero a esa hora  
 */
public function consultarReservadeUnVehiculoEnUnParqueaderoConFecha($placaVehiculo,$codParqueadero,$fechaActual)
{
	
$NuevaFecha = strtotime ( '+0 hour' , strtotime ($fechaActual) ) ; 
$fechaMenor = strtotime ( '-15 minute' , $NuevaFecha) ; 
$fechaMayor = strtotime ( '+15 minute' , $NuevaFecha ) ; 
$fechaMenor = date ( 'Y-m-d H:i' , $fechaMenor); 
$fechaMayor = date ( 'Y-m-d H:i' , $fechaMayor);

	$sentencia="SELECT cod_reserva FROM RESERVA, PARQUEADERO, VEHICULO WHERE VEHICULO.placa_vehiculo='$placaVehiculo' AND  PARQUEADERO.cod_parqueadero=$codParqueadero AND RESERVA.cod_parqueadero=PARQUEADERO.cod_parqueadero AND RESERVA.cod_estado_reserva=1 AND DATE_FORMAT(RESERVA.fecha_hora_inicial,'%Y-%m-%d %H:%i') BETWEEN '$fechaMenor' AND '$fechaMayor'";
	$resultado=mysqli_query($this->conexion,$sentencia);
	$row=mysqli_fetch_array($resultado);
	return $row["cod_reserva"];
}

/**
 * Consulta una reserva de un vehiculo, en un parqueadero especifico sin fecha 
 * @param $placaVehiculo Placa del vehiculo relacionado con la reserva
 * @param $codParqueadero Codigo del parqueadero que está realizando la consulta
 * @return $resultado Código de la reserva de ese vehiculo en ese parqueadero  
 */
public function consultarReservadeUnVehiculoEnUnParqueadero($placaVehiculo,$codParqueadero)
{
	$sentencia="SELECT cod_reserva FROM RESERVA, PARQUEADERO, VEHICULO WHERE VEHICULO.placa_vehiculo='$placaVehiculo'  AND PARQUEADERO.cod_parqueadero=$codParqueadero AND RESERVA.cod_parqueadero=PARQUEADERO.cod_parqueadero;";
	$result=mysqli_query($this->conexion,$sentencia);
	$resultado=mysqli_fetch_array($result);
	return $resultado;
}

 /**
         * Consulta el código de reserva de un vehiculo para la salida
         * @param Int Placa del vehiculo al que se le registra la salida
         * @param int Codigo del parqueadero en el que se encuentra el vehiculo
         * @return int codigo de la reserva encontrada
         */
public function consultarVehiculoParaSalida($placa,$codParqueadero){

	$sentencia="SELECT cod_reserva FROM RESERVA, VEHICULO, PARQUEADERO WHERE VEHICULO.placa_vehiculo='$placa' AND PARQUEADERO.cod_parqueadero=$codParqueadero AND RESERVA.cod_estado_reserva=5 AND 
	RESERVA.placa_vehiculo=VEHICULO.placa_vehiculo AND RESERVA.cod_parqueadero=PARQUEADERO.cod_parqueadero";
	$resultado=mysqli_query($this->conexion,$sentencia);
	$row=mysqli_fetch_array($resultado);
	return $row["cod_reserva"];

}

/**
         * Consulta el las reservas de un cliente
         * @param Int Cedula del CLiente
         * @return Array reserva del cliente
         */
public function consultarReservasCliente($cedula){

	$sentencia="SELECT * FROM RESERVA, VEHICULO, CLIENTE WHERE CLIENTE.cedula_cliente='$cedula' AND RESERVA.placa_vehiculo=VEHICULO.placa_vehiculo AND VEHICULO.cedula_cliente=CLIENTE.cedula_cliente";
	$resultado=mysqli_query($this->conexion,$sentencia);
	$reservas = array();

	while ($row = mysqli_fetch_array($resultado)) {
		$reserva1 = new Reserva();
		$reserva1->setParqueadero($row["cod_parqueadero"]);
		$reserva1->setHoraInicial($row["fecha_hora_inicial"]);
		$reserva1->setHoraFinal($row["fecha_hora_final"]);
		$reserva1->setEstado($row["cod_estado_reserva"]);
		$reserva1->setVehiculo($row["placa_vehiculo"]);
		$reserva1->setHora_ingreso_real($row["hora_ingreso_real"]);
		$reserva1->setHora_salida_real($row["hora_salida_real"]);
		array_push($reservas,$reserva1);
	}
	return $reservas;
}

/**
 * Lista todos los objetos que se están en la tabla de reservas
 * @return [reserva] 
 */
public function contarReservasPorDia($mes, $ano){
	$sentencia="SELECT  DAY(fecha_hora_inicial) AS dia, COUNT(*) AS cantidad
				FROM  RESERVA
				WHERE  YEAR(fecha_hora_inicial) = ".$ano." and  MONTH(fecha_hora_inicial) = ".$mes." GROUP BY DAY(fecha_hora_inicial);";

	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	
	$reservasDia = array();
	$dia = 0;
	$cantidad = 0; 

	while ($row = mysqli_fetch_array($result)) {
		$dia = $row["dia"];
		$cantidad=$row["cantidad"];
		array_push($reservasDia,$dia, $cantidad);
	}
	return $reservasDia;
}

/**
 * Lista todos los objetos que se están en la tabla de reservas
 * @return [reserva] 
 */
public function listarAnos(){
	$sentencia= "SELECT DISTINCT YEAR(fecha_hora_inicial) AS anos
		FROM RESERVA;";

	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	
	$anosReserva = array();
	$anos = 0;

	while ($row = mysqli_fetch_array($result)) {
		$anos=$row["anos"];
		array_push($anosReserva,$anos);
	}
	return $anosReserva;
}

/**
 * Lista todos los objetos que se están en la tabla de reservas
 * @return [reserva] 
 */
public function listarMesesPorAno($ano){
	$sentencia= "SELECT DISTINCT MONTh(fecha_hora_inicial) AS mes
	FROM RESERVA
	WHERE YEAR(fecha_hora_inicial) =".$ano;

	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	
	$mesesReserva = array();
	$mes = 0;

	while ($row = mysqli_fetch_array($result)) {
		$mes=$row["mes"];
		array_push($mesesReserva,$mes);
	}
	return $mesesReserva;
}


public function listarAnosPorZona($cod_zona){
	$sentencia= "SELECT DISTINCT YEAR(fecha_hora_inicial) AS anos
	FROM RESERVA, PARQUEADERO, ZONA
	WHERE RESERVA.cod_parqueadero = PARQUEADERO.cod_parqueadero AND PARQUEADERO.cod_zona =".$cod_zona;

	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	
	$anosReserva = array();
	$anos = 0;

	while ($row = mysqli_fetch_array($result)) {
		$anos=$row["anos"];
		array_push($anosReserva,$anos);
	}
	return $anosReserva;
}

/**
 * Lista todos los objetos que se están en la tabla de reservas
 * @return [reserva] 
 */
public function listarMesesPorAnoPorZona($ano, $cod_zona){
	$sentencia= "SELECT DISTINCT MONTh(fecha_hora_inicial) AS mes
	FROM RESERVA, PARQUEADERO
	WHERE YEAR(fecha_hora_inicial) = ".$ano." AND
	 RESERVA.cod_parqueadero = PARQUEADERO.cod_parqueadero AND
	 PARQUEADERO.cod_zona =".$cod_zona;

	if(!$result = mysqli_query($this->conexion, $sentencia)) die();

	$mesesReserva = array();
	$mes = 0;

	while ($row = mysqli_fetch_array($result)) {
		$mes=$row["mes"];
		array_push($mesesReserva,$mes);
	}
	return $mesesReserva;
}

/**
 * Lista todos los objetos que se están en la tabla de reservas
 * @return [reserva] 
 */
public function contarReservasPorDiaPorZona($mes, $ano, $cod_zona){
	$sentencia="SELECT  DAY(fecha_hora_inicial) AS dia, COUNT(*) AS cantidad
	FROM  RESERVA, PARQUEADERO	WHERE  YEAR(RESERVA.fecha_hora_inicial) = ".$ano." AND MONTH(RESERVA.fecha_hora_inicial) = ".$mes." 
	AND RESERVA.cod_parqueadero = PARQUEADERO.cod_parqueadero AND PARQUEADERO.cod_zona =".$cod_zona." GROUP BY DAY(fecha_hora_inicial)";

	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	
	$reservasDia = array();
	$dia = 0;
	$cantidad = 0; 

	while ($row = mysqli_fetch_array($result)) {
		$dia = $row["dia"];
		$cantidad=$row["cantidad"];
		array_push($reservasDia,$dia, $cantidad);
	}
	return $reservasDia;
}

/*
	*Obtiene el objeto de esta clase
	*@param $conexion
	*@return void
	*/
	public static function obtenerReservaDAO($conexion) {
            if(self::$reservaDAO == null) {
                self::$reservaDAO = new reservaDAO($conexion);
            }

            return self::$reservaDAO;
        }

}


?>