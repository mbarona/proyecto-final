<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Ciudad.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para las Ciudades
 */
class CiudadDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase CiudadDAO
	 * @var [CiudadDAO]
	 */
	private static $ciudadDAO;


	/**
	 * Constructor de la clase
	 */

	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Ciudad del objeto a consultar]
 * @return [Ciudad]    Ciudad encontrada
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM CIUDAD WHERE cod_ciudad=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$ciudad=new Ciudad();
$ciudad->setCodCiudad($row["cod_ciudad"]);
$ciudad->setNomCiudad($row["nom_ciudad"]);

return $ciudad;

}
/**
 * Realiza la consulta de un objeto
 * @param  [String] $nombre [nombre del objeto a consultar]
 * @return [Ciudad]    Ciudad encontrada
 */
public function consultarPorNombre($nombre){
	$sentencia="SELECT * FROM CIUDAD WHERE nom_ciudad='".$nombre."'";
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$ciudad=new Ciudad();
$ciudad->setCodCiudad($row["cod_ciudad"]);
$ciudad->setNomCiudad($row["nom_ciudad"]);

return $ciudad;

}
/**
 * Crea una nueva Ciudad en la base de datos
 * @param  Ciudad $ciudadNueva
 * @return void
 */
public function crear ($ciudadNueva){
	$sentencia="INSERT INTO CIUDAD VALUES(".$ciudadNueva->getCodCiudad().",".$ciudadNueva->getNomCiudad()."');";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una ciudad ingresada por parámetro
 * @param  [Ciudad] $ciudad Ciudad ingresada por parámetro
 * @return void
 */
public function modificar($ciudad){
	$sentencia="UPDATE CIUDAD SET nom_ciudad='".$ciudad->getNomCiudad()."' WHERE cod_ciudad=".$ciudad->getCodCiudad();

	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de ciudades
 * @return [Ciudad]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM CIUDAD";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$ciudades = array();

	while ($row = mysqli_fetch_array($result)) {
		$ciudad = new Ciudad();
		$ciudad->setCodCiudad($row["cod_ciudad"]);
		$ciudad->setNomCiudad($row["nom_ciudad"]);
		array_push($ciudades,$ciudad);
	}
	return $ciudades;
}

/**
 * Lista todos los objetos que se están en la tabla de ciudades por un departamento dado
 * @return [Ciudad]
 */
public function listarPorDepartamento($departamento){
	$sentencia="SELECT * FROM CIUDAD WHERE cod_departamento=".$departamento." ORDER BY nom_ciudad ASC";
	if(!$result =mysqli_query($this->conexion, $sentencia))die();
	$ciudades =array();

	while ($row = mysqli_fetch_array($result)) {
		$ciudad = new Ciudad();
		$ciudad->setCodCiudad($row["cod_ciudad"]);
		$ciudad->setNomCiudad($row["nom_ciudad"]);
		array_push($ciudades,$ciudad);
	}
	return $ciudades;
}
/**
 * Lista todos los objetos que se están en la tabla de ciudades que tengan al menos una zona registrada
 * @return [Ciudad]
 */
public function listarConZona(){
	$sentencia="SELECT DISTINCT CIUDAD.cod_ciudad, nom_ciudad FROM ZONA,CIUDAD,PARQUEADERO  WHERE ZONA.cod_ciudad = CIUDAD.cod_ciudad AND ZONA.cod_zona = PARQUEADERO.cod_zona ORDER BY nom_ciudad ASC";
	if(!$result =mysqli_query($this->conexion, $sentencia))die();
	$ciudades =array();

	while ($row = mysqli_fetch_array($result)) {
		$ciudad = new Ciudad();
		$ciudad->setCodCiudad($row["cod_ciudad"]);
		$ciudad->setNomCiudad($row["nom_ciudad"]);
		array_push($ciudades,$ciudad);
	}
	return $ciudades;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerCiudadDAO($conexion_bd) {
            if(self::$ciudadDAO == null) {
                self::$ciudadDAO = new CiudadDAO($conexion_bd);
            }
            return self::$ciudadDAO;
        }

}


?>
