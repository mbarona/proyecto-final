<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/TarjetaDAO.php';

/**
 * Archivo de entidad
 */
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/negocio/Cliente.php';
require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/negocio/Tarjeta.php';
//require_once('../negocio/Cliente.php');
//require_once('../negocio/Tarjeta.php');

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para los clientes
 */
class ClienteDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase ClienteDAO
	 * @var [ClienteDAO]
	 */
	private static $clienteDAO;


	/**
	 * Constructor de la clase
	 */

	 function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion,"utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Cliente]         Cliente encontrado
 */
public function consultar($codigo){
	#echo "valor";
	$sentencia="SELECT * FROM CLIENTE WHERE cedula_cliente=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);

$cliente=new Cliente();
$cliente->setIdentificacion($row["cedula_cliente"]);
$cliente->setNombre($row["nom_cliente"]);
$cliente->setEmail($row["email_cliente"]);
$cliente->setContrasena($row["password_cliente"]);
$cliente->setEstado($row["estado"]);
#echo "pruebA1";
#$tarjetaDAO=TarjetaDAO::obtenerTarjetaDAO($this->conexion);
#$tarjeta=$tarjetaDAO->consultar($row["numero_tarjeta"]);
$cliente->setTarjeta($row["numero_tarjeta"]);
$cliente->setPuntos($row["puntos"]);
$cliente->setIntentosFallidos($row["intentos_fallidos"]);


return $cliente;

}
public function consultarEmail($correo){
	$sentencia="SELECT * FROM CLIENTE WHERE email_cliente='$correo'";
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$cliente=new Cliente();
$cliente->setIdentificacion($row["cedula_cliente"]);
$cliente->setNombre($row["nom_cliente"]);
$cliente->setEmail($row["email_cliente"]);
$cliente->setContrasena($row["password_cliente"]);
$cliente->setTarjeta($row["numero_tarjeta"]);
$cliente->setEstado($row["estado"]);
$cliente->setPuntos($row["puntos"]);
$cliente->setIntentosFallidos($row["intentos_fallidos"]);


return $cliente;

}


/**
 * Crea una nuevo cliente en la base de datos
 * @param  Cliente $clienteNuevo
 * @return void
 */
public function crear ($clienteNuevo){
	// orden de insercion cedula, nombre, email, contaseña, tarjeta, estado, puntos, intentos
	$password = password_hash($clienteNuevo->getContrasena(), PASSWORD_BCRYPT);
	$passwordFinal="#".$password;
	$numeroTarjeta=$clienteNuevo->getTarjeta()->getNumeroTarjeta();
	$sentencia="INSERT INTO CLIENTE VALUES(".$clienteNuevo->getIdentificacion().",'".$clienteNuevo->getNombre()."','".$clienteNuevo->getEmail()."','".$passwordFinal."',".$numeroTarjeta.",1,0,0)";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una cliente ingresado por parámetro
 * @param  Cliente $cliente Cliente ingresado por parámetro
 * @return void
 */
public function modificar($cliente){
	//$password = password_hash($cliente->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="UPDATE CLIENTE SET nom_cliente='".$cliente->getNombre()."', email_cliente ='".$cliente->getEmail()."', password_cliente='".$cliente->getContrasena()."', numero_tarjeta=".$cliente->getTarjeta().", estado=".$cliente->getEstado().", puntos=".$cliente->getPuntos().", intentos_fallidos=".$cliente->getIntentosFallidos()." WHERE cedula_cliente= ".$cliente->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}
/**
 * Modifica una cliente ingresado por parámetro
 * @param  Cliente $cliente Cliente ingresado por parámetro
 * @return void
 */
public function modificarDatos($cliente){
	$sentencia="UPDATE CLIENTE SET nom_cliente='".$cliente->getNombre()."', email_cliente ='".$cliente->getEmail()."' WHERE cedula_cliente= ".$cliente->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}
/**
 * Modifica el estado de un cliente ingresado por parámetro
 * @param  Cliente $cliente Cliente ingresado por parámetro
 * @return void
 */
public function cambiarEstado($cliente){
	$sentencia="UPDATE CLIENTE SET estado=".$cliente->getEstado()." WHERE cedula_cliente= ".$cliente->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Modifica la trajeta de un cliente ingresado
 * @param  Cliente $cliente Cliente ingresado por parámetro
 * @return void
 */
public function cambiarTarjeta($cliente){
	$sentencia="UPDATE CLIENTE SET numero_tarjeta=".$cliente->getTarjeta()->getNumeroTarjeta()." WHERE cedula_cliente= ".$cliente->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de cliente
 * @return [Clientes]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM CLIENTE";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$clientes = array();

	while ($row = mysqli_fetch_array($result)) {
		$cliente=new Cliente();
        $cliente->setIdentificacion($row["cedula_cliente"]);
        $cliente->setNombre($row["nom_cliente"]);
        $cliente->setEmail($row["email_cliente"]);
        $cliente->setContrasena($row["password_cliente"]);
        $cliente->setEstado($row["estado"]);
        $cliente->setTarjeta($row["numero_tarjeta"]);
		$cliente->setPuntos($row["puntos"]);
		$cliente->setIntentosFallidos($row["intentos_fallidos"]);
		array_push($clientes,$cliente);
	}
	return $clientes;
}


/**
 * Lista el numero de Clientes que hay en la base de datos
 * @return [int] numero de Clientes
 */
public function totalClientes(){
	$sentencia="select count(*) from CLIENTE";
	$result = mysqli_query($this->conexion, $sentencia);
	$total=0;
	while ($row = $result->fetch_assoc()) {
		$total=$row['count(*)'];
	}

	return $total;
}

/**
 *Consulta un cliente asociado a una reserva
 * @param  int $codReserva código de la reserva
 * @return int cedula del cliente asociado a esa reserva
 */
public function obtenerClienteDeUnaReserva($codReserva){
$sentencia="SELECT CLIENTE.cedula_cliente FROM CLIENTE, VEHICULO, RESERVA WHERE RESERVA.cod_reserva=$codReserva AND RESERVA.placa_vehiculo=VEHICULO.placa_vehiculo AND CLIENTE.cedula_cliente=VEHICULO.cedula_cliente";
$result=mysqli_query($this->conexion,$sentencia);
$row=mysqli_fetch_array($result);
return $row["cedula_cliente"];
}
/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerClienteDAO($conexion) {
            if(self::$clienteDAO == null) {
                self::$clienteDAO = new ClienteDAO($conexion);
            }

            return self::$clienteDAO;
        }

}


?>
