<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Factura.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para la factura
 */
class FacturaDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase FacturaDAO
	 * @var [facturaDAO]
	 */
	private static $facturaDAO;


	/**
	 * Constructor de la clase
	 */

	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Funcionario]         Funcionario encontrado
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM FACTURA WHERE cod_factura=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
$row=mysqli_fetch_array($result);
$factura=new Factura();
$factura->getCod_reserva($row["cod_reserva"]);
$factura->getPrecio_total($row["precio_total"]);
$factura->getCod_f_pago($row["cod_f_pago"]);

return $factura;

}

/**
 * Crea una nueva factura en la base de datos
 * @param  Factura $facturaNueva
 * @return void
 */
public function crear ($facturaNueva){
	$sentencia="INSERT INTO FACTURA VALUES(0,".$facturaNueva->getCod_reserva().", ".$facturaNueva->getPrecio_total().", ".$facturaNueva->getCod_f_pago().");";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una factura ingresado por parámetro
 * @param  Factura $factura Factura ingresado por parámetro
 * @return void
 */
public function modificar($factura){
	$sentencia="UPDATE FACTURA SET precio_total='".$factura->getPrecio_total()."', cod_f_pago=".$factura->getCod_f_pago()." WHERE cod_reserva= ".$factura->getCod_reserva();
  mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla Factura
 * @return [Facturas]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM FACTURA";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$facturas = array();

	while ($row = mysqli_fetch_array($result)) {
		$factura=new Factura();
    $factura->setCod_reserva($row["cod_reserva"]);
    $factura->setPrecio_total($row["precio_total"]);
    $factura->setCod_f_pago($row["cod_f_pago"]);
		array_push($facturas,$factura);
	}
	return $facturas;
}
/**
         * Consulta las facturas de un cliente
         * @param Int Cedula del CLiente
         * @return Array reserva del cliente
         */
public function consultarFacturasCliente($cedula){

	$sentencia="SELECT * FROM FACTURA, RESERVA, VEHICULO, CLIENTE WHERE CLIENTE.cedula_cliente='$cedula' AND FACTURA.cod_reserva=RESERVA.cod_reserva AND RESERVA.placa_vehiculo=VEHICULO.placa_vehiculo AND VEHICULO.cedula_cliente=CLIENTE.cedula_cliente";
	$resultado=mysqli_query($this->conexion,$sentencia);
	$facturas = array();

	while ($row = mysqli_fetch_array($resultado)) {
		$factura = new Factura();
		$factura->setCod_reserva($row["cod_reserva"]);
    	$factura->setPrecio_total($row["precio_total"]);
    	$factura->setCod_f_pago($row["cod_f_pago"]);
		array_push($facturas,$factura);
	}
	return $facturas;
}
 
	/**
	 * Consulta las facturas totales de las zonas en el mes actual
	 * @return Result resultado del msql query
	 */
	public function facturasPorZonaPorMesPorAño($año,$mes){

		$sentencia="SELECT ZONA.nom_zona , CIUDAD.nom_ciudad,  nom_gerente,  PARQUEADERO.nom_parqueadero, COUNT(cod_factura) AS facturas, SUM(precio_total)AS total,
			(SELECT EXTRACT(MONTH FROM RESERVA.hora_salida_real)) AS mes,  RESERVA.hora_salida_real
			FROM FACTURA,RESERVA,PARQUEADERO, ZONA, GERENTE, CIUDAD
			WHERE FACTURA.cod_reserva = RESERVA.cod_reserva AND RESERVA.cod_parqueadero = PARQUEADERO.cod_parqueadero 
			AND PARQUEADERO.cod_zona = ZONA.cod_zona AND ZONA.cod_zona = GERENTE.cod_zona 
			AND ZONA.cod_ciudad = CIUDAD.cod_ciudad
			AND (SELECT EXTRACT(MONTH FROM RESERVA.hora_salida_real)) = '".$mes."'
			AND (SELECT EXTRACT(YEAR FROM RESERVA.hora_salida_real)) = '".$año."'
			GROUP BY nom_parqueadero,nom_zona,nom_gerente,mes, nom_ciudad, hora_salida_real
			ORDER BY total DESC";
		$resultado=mysqli_query($this->conexion,$sentencia);
		
		return $resultado;
	}

/**
	 * Consulta las facturas totales de las zonas en el mes actual
	 * @return Result resultado del msql query
	 */
	public function facturasPorZonaPorMesPorAño2($año,$mes){

		$sentencia="SELECT ZONA.nom_zona , CIUDAD.nom_ciudad,  nom_gerente,  PARQUEADERO.nom_parqueadero, COUNT(cod_factura) AS facturas, SUM(precio_total)AS total,
			(SELECT EXTRACT(MONTH FROM RESERVA.hora_salida_real)) AS mes
			FROM FACTURA,RESERVA,PARQUEADERO, ZONA, GERENTE, CIUDAD
			WHERE FACTURA.cod_reserva = RESERVA.cod_reserva AND RESERVA.cod_parqueadero = PARQUEADERO.cod_parqueadero 
			AND PARQUEADERO.cod_zona = ZONA.cod_zona AND ZONA.cod_zona = GERENTE.cod_zona 
			AND ZONA.cod_ciudad = CIUDAD.cod_ciudad
			AND (SELECT EXTRACT(MONTH FROM RESERVA.hora_salida_real)) = '".$mes."'
			AND (SELECT EXTRACT(YEAR FROM RESERVA.hora_salida_real)) = '".$año."'
			GROUP BY nom_parqueadero,nom_zona,nom_gerente,mes, nom_ciudad
			ORDER BY facturas DESC";
		$resultado=mysqli_query($this->conexion,$sentencia);
		
		return $resultado;
	}


	/**
	 * Consulta las facturas totales de las zonas en el mes actual
	 * @return Result resultado del msql query
	 */
	public function añosFacturas(){

		$sentencia="SELECT  (SELECT EXTRACT(YEAR FROM RESERVA.hora_salida_real)) AS año
		FROM FACTURA,RESERVA
		WHERE FACTURA.cod_reserva = RESERVA.cod_reserva 
		GROUP BY año
		ORDER BY año DESC ";
		$resultado=mysqli_query($this->conexion,$sentencia);
		
		return $resultado;
	}
	/**
	 * Consulta los meses con facturas por un año dado
	 * @return Result resultado del msql query
	 */
	public function mesesFacturas($año){

		$sentencia="SELECT  (SELECT EXTRACT(MONTH FROM RESERVA.hora_salida_real)) AS mes
		FROM FACTURA,RESERVA
		WHERE FACTURA.cod_reserva = RESERVA.cod_reserva 
		AND (SELECT EXTRACT(YEAR FROM RESERVA.hora_salida_real)) = '".$año."'
		GROUP BY mes
		ORDER BY mes DESC ";
		$resultado=mysqli_query($this->conexion,$sentencia);
		
		return $resultado;
	}

	/**
	 * Consulta las facturas totales de las zonas en el mes actual
	 * @return Result resultado del msql query
	 */
	public function facturasPorZonaPorFechaDada($inicial, $final){

		$sentencia="SELECT ZONA.nom_zona , CIUDAD.nom_ciudad,  nom_gerente,  PARQUEADERO.nom_parqueadero, COUNT(cod_factura) AS facturas, SUM(precio_total)AS total,
		(SELECT EXTRACT( MONTH  FROM RESERVA.hora_salida_real)) AS mes, (SELECT EXTRACT(YEAR FROM RESERVA.hora_salida_real)) AS año
		FROM FACTURA,RESERVA,PARQUEADERO, ZONA, GERENTE, CIUDAD
		WHERE FACTURA.cod_reserva = RESERVA.cod_reserva AND RESERVA.cod_parqueadero = PARQUEADERO.cod_parqueadero
		AND PARQUEADERO.cod_zona = ZONA.cod_zona AND ZONA.cod_zona = GERENTE.cod_zona
		AND ZONA.cod_ciudad = CIUDAD.cod_ciudad
		AND RESERVA.hora_salida_real BETWEEN '$inicial' AND '$final'
		GROUP BY nom_parqueadero,nom_zona,nom_gerente,mes,año, nom_ciudad
		ORDER BY facturas DESC , total ASC";
		$resultado=mysqli_query($this->conexion,$sentencia);
		
		return $resultado;
	}

	
	/**
	 * Consulta las facturas totales de las zonas en el año actual
	 * @return Result resultado del msql query
	 */
	public function facturasPorZonaPorAñoDado($año){

		$sentencia="SELECT ZONA.nom_zona , CIUDAD.nom_ciudad,  nom_gerente,  PARQUEADERO.nom_parqueadero, COUNT(cod_factura) AS facturas, SUM(precio_total)AS total,
		(SELECT EXTRACT(MONTH FROM RESERVA.hora_salida_real)) AS mes,	(SELECT EXTRACT(YEAR FROM RESERVA.hora_salida_real)) AS año
			FROM FACTURA,RESERVA,PARQUEADERO, ZONA, GERENTE, CIUDAD
			WHERE FACTURA.cod_reserva = RESERVA.cod_reserva AND RESERVA.cod_parqueadero = PARQUEADERO.cod_parqueadero 
			AND PARQUEADERO.cod_zona = ZONA.cod_zona AND ZONA.cod_zona = GERENTE.cod_zona 
			AND ZONA.cod_ciudad = CIUDAD.cod_ciudad	AND (SELECT EXTRACT(YEAR FROM RESERVA.hora_salida_real)) = '".$año."'
			GROUP BY nom_parqueadero,nom_zona,nom_gerente,mes,año, nom_ciudad
			ORDER BY facturas DESC , total DESC";
		$resultado=mysqli_query($this->conexion,$sentencia);
		
		return $resultado;
	}

	/**
	 * Consulta las facturas totales de las zonas
	 * @return Result resultado del msql query
	 */
	public function facturasPorZona(){

		$sentencia="SELECT ZONA.nom_zona , CIUDAD.nom_ciudad,  nom_gerente, COUNT(cod_factura) AS facturas, SUM(precio_total)AS total,
		(SELECT EXTRACT(MONTH FROM RESERVA.hora_salida_real)) AS mes, (SELECT EXTRACT(YEAR FROM RESERVA.hora_salida_real)) AS año
		FROM FACTURA,RESERVA,PARQUEADERO, ZONA, GERENTE, CIUDAD
		WHERE FACTURA.cod_reserva = RESERVA.cod_reserva AND RESERVA.cod_parqueadero = PARQUEADERO.cod_parqueadero 
		AND PARQUEADERO.cod_zona = ZONA.cod_zona AND ZONA.cod_zona = GERENTE.cod_zona 
		AND ZONA.cod_ciudad = CIUDAD.cod_ciudad
		GROUP BY nom_zona,nom_gerente,mes,año, nom_ciudad
		ORDER BY facturas DESC, total DESC	";
		$resultado=mysqli_query($this->conexion,$sentencia);
		
		return $resultado;
	}
	

 
/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerFacturaDAO($conexion) {
            if(self::$facturaDAO == null) {
                self::$facturaDAO = new FacturaDAO($conexion);
            }

            return self::$facturaDAO;
        }

}


?>
