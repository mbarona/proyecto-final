<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Departamento.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para las departamentoS
 */
class DepartamentoDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase DepartamentoDAO
	 * @var [DepartamentoDAO]
	 */
	private static $departamentoDAO;


	/**
	 * Constructor de la clase
	 */

	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [departamento]  Departamento encontrado
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM DEPARTAMENTO WHERE cod_departamento=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
	$row=mysqli_fetch_array($result);
	$departamento=new Departamento();
	$departamento->setCodDepartamento($row["cod_departamento"]);
	$departamento->setNomDepartamento($row["nom_departamento"]);

return $departamento;

}

/**
 * Crea una nuevo departamento en la base de datos
 * @param  Departamento $departamentoNuevo
 * @return void
 */
public function crear ($departamentoNuevo){
	$sentencia="INSERT INTO DEPARTAMENTO VALUES(".$departamentoNuevo->getCodDepartamento().",".$departamentoNuevo->getNomDepartamento()."');";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una departamento ingresada por parámetro
 * @param  [departamento] $departamento departamento ingresada por parámetro
 * @return void
 */
public function modificar($departamento){
	$sentencia="UPDATE DEPARTAMENTO SET nom_departamento='".$departamento->getNomDepartamento()."' WHERE cod_departamento=".$departamento->getCodDepartamento();

	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de Departamentos
 * @return [Departamento]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM DEPARTAMENTO";
	if(!$result = mysqli_query($this->conexion, $sentencia))die();
	$departamentos = array();
	while ($row = mysqli_fetch_array($result)) {
		$departamento = new Departamento();
		$departamento->setCodDepartamento($row["cod_departamento"]);
		$departamento->setNomDepartamento($row["nom_departamento"]);
		array_push($departamentos,$departamento);
	}
	return $departamentos;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerDepartamentoDAO($conexion_bd){
            if(self::$departamentoDAO == null) {
                self::$departamentoDAO = new DepartamentoDAO($conexion_bd);
            }

            return self::$departamentoDAO;
        }

}


?>
