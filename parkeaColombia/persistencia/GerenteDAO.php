<?php
/**
 * Archivo de conexión a la base de datos
 */
//require_once('../persistencia/util/Conexion.php');
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
//require_once('../negocio/Gerente.php');

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Gerente.php';
/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Dao para los gerentes
 */
class GerenteDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase GerenteDAO
	 * @var [GerenteDAO]
	 */
	private static $gerenteDAO;


	/**
	 * Constructor de la clase
	 */

	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $identificacion [id del objeto a consultar]
 * @return [Gerente]         Gerente encontrado
 */
public function consultar($identificacion){
	$sentencia="SELECT * FROM GERENTE WHERE cedula_gerente=".$identificacion;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
	$row=mysqli_fetch_array($result);
	$gerente=new Gerente();
	$gerente->setIdentificacion($row["cedula_gerente"]);
	$gerente->setNombre($row["nom_gerente"]);
	$gerente->setEmail($row["email_gerente"]);
	$gerente->setContrasena($row["password_gerente"]);
	$gerente->setEstado($row["estado"]);
	$gerente->setZona($row["cod_zona"]);
	$gerente->setIntentosFallidos($row["intentos_fallidos"]);


return $gerente;

}
/**
 * Realiza la consulta de un objeto
 * @param  [String] $correo [correo del objeto a consultar]
 * @return [Gerente]         Gerente encontrado
 */
public function consultarEmail($correo){
	$sentencia="SELECT * FROM GERENTE WHERE email_gerente='$correo'";
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
	$row=mysqli_fetch_array($result);
	$gerente=new Gerente();
	$gerente->setIdentificacion($row["cedula_gerente"]);
	$gerente->setNombre($row["nom_gerente"]);
	$gerente->setEmail($row["email_gerente"]);
	$gerente->setContrasena($row["password_gerente"]);
	$gerente->setEstado($row["estado"]);
	$gerente->setZona($row["cod_zona"]);
	$gerente->setIntentosFallidos($row["intentos_fallidos"]);


return $gerente;

}


/**
 * Crea una nuevo gerente en la base de datos
 * @param  Gerente $gerenteNuevo
 * @return void
 */
public function crear ($gerenteNuevo){
	//orden: cedula_gerente, nom_gerente, email_gerente, estado, password_gerente, intentos_fallidos, cod_zona
	$password = "#".password_hash($gerenteNuevo->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="INSERT INTO GERENTE VALUES(".$gerenteNuevo->getIdentificacion().",'".$gerenteNuevo->getNombre()."','".$gerenteNuevo->getEmail()."',1,'".$password."',0,".$gerenteNuevo->getZona().");";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica una gerente ingresado por parámetro
 * @param  Gerente $gerente Gerente ingresado por parámetro
 * @return void
 */
public function modificar($gerente){
	// $password = password_hash($gerente->getContrasena(), PASSWORD_BCRYPT);
	$sentencia="UPDATE GERENTE SET nom_gerente='".$gerente->getNombre()."', email_gerente ='".$gerente->getEmail()."', estado=".$gerente->getEstado().", password_gerente='".$gerente->getContrasena()."', intentos_fallidos=".$gerente->getIntentosFallidos().", cod_zona=".$gerente->getZona()." WHERE cedula_gerente= ".$gerente->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}
/**
 * Modifica el estado de un gerente ingresado por parámetro
 * @param  Gerente $gerente Gerente ingresado por parámetro
 * @return void
 */
public function cambiarEstado($gerente){
	$sentencia="UPDATE GERENTE SET estado=".$gerente->getEstado()." WHERE cedula_gerente= ".$gerente->getIdentificacion();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de gerente
 * @return [Gerentes]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM GERENTE ORDER BY nom_gerente ASC";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$gerentes = array();

	while ($row = mysqli_fetch_array($result)) {
		$gerente=new Gerente();
		$gerente->setIdentificacion($row["cedula_gerente"]);
		$gerente->setNombre($row["nom_gerente"]);
		$gerente->setEmail($row["email_gerente"]);
		$gerente->setContrasena($row["password_gerente"]);
		$gerente->setEstado($row["estado"]);
		$gerente->setZona($row["cod_zona"]);
		$gerente->setIntentosFallidos($row["intentos_fallidos"]);
		array_push($gerentes,$gerente);
	}
	return $gerentes;
}

/**
 * Lista el numero de gerentes que hay en la base de datos
 * @return [int] numero de gerentes
 */
public function totalGerentes(){
	$sentencia="select count(*) from GERENTE";
	$result = mysqli_query($this->conexion, $sentencia);
	$total=0;
	while ($row = $result->fetch_assoc()) {
		$total=$row['count(*)'];
	}

	return $total;
}

/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerGerenteDAO($conexion_bd) {
            if(self::$gerenteDAO == null) {
                self::$gerenteDAO = new GerenteDAO($conexion_bd);
            }

            return self::$gerenteDAO;
        }

}


?>
