<?php
/**
 * Archivo de conexión a la base de datos
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

/**
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Vehiculo.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/T_VehiculoDAO.php';

/**
 * Interfaz DAO
 */
require_once('DAO.php');

/**
 * Clase Dao Vehiculo
 */
class VehiculoDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase VehiculoDAO
	 * @var [VehiculoDAO]
	 */
	private static $VehiculoDAO;


	/**
	 * Constructor de la clase
	 */

	private 	function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Asignacion] Asignacion Encontrada
 */
public function consultar($placa){
	$sentencia="SELECT * FROM VEHICULO WHERE placa_vehiculo='$placa'";
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
 	 $row=mysqli_fetch_array($result);
		$Vehiculo=new Vehiculo();
		$Vehiculo->setPlaca($row["placa_vehiculo"]);
		$Vehiculo->setCodCliente($row["cedula_cliente"]);
		$Vehiculo->setTipo_vehiculo($row["cod_tipo_vehiculo"]);

return $Vehiculo;

}
/**
 * Crea una nuevo Vehiculo en la base de datos
 * @param  Vehiculo $VehiculoNuevo
 * @return void
 */
public function crear ($VehiculoNuevo){
	$sentencia="INSERT INTO VEHICULO VALUES('".$VehiculoNuevo->getPlaca()."', ".$VehiculoNuevo->getCodCliente().", ".$VehiculoNuevo->getTipo_vehiculo().")";
	mysqli_query($this->conexion, $sentencia);
}

/**
 * Modifica un Vehiculo ingresada por parámetro
 * @param  [Vehiculo] $pVehiculo ingresada por parámetro
 * @return void
 */
public function modificar($pVehiculo){
	$sentencia="UPDATE VEHICULO SET cod_tipo_vehiculo=".$pVehiculo->getTipo_vehiculo()."' WHERE placa_vehiculo='.$pVehiculo->getPlaca().'";
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla Vehiculo
 * @return [VehiculoArray]
 */
public function listarTodo(){
	$sentencia="SELECT * FROM VEHICULO";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$T_VehiculoArray = array();

	while ($row = mysqli_fetch_array($result)) {
		$Vehiculo = new Vehiculo();
		$Vehiculo->setPlaca($row["placa_vehiculo"]);
    $Vehiculo->setCodCliente($row["cedula_cliente"]);
    $Vehiculo->setTipo_vehiculo($row["cod_tipo_vehiculo"]);
		array_push($VehiculoArray,$T_Vehiculo);
	}
	return $VehiculoArray;
}

/**
 * Consulta los vehiculos de un cliente
 * @param integer Número de identificación del cliente
 * @return Vehiculo Arreglo de los vehiculos del cliente
 */
public function consultarVehiculosPorCliente($codCliente){

	$sentencia="SELECT * FROM VEHICULO WHERE cedula_cliente=$codCliente";	
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	
	$vehiculos=array();
	while($row =mysqli_fetch_array($result)){;
		$vehiculo=new Vehiculo();
		$tvehiculoDAO=T_VehiculoDAO::obtenerT_VehiculoDAO($this->conexion);
		$tVehiculo=$tvehiculoDAO->consultar($row["cod_tipo_vehiculo"]);
		$vehiculo->setTipo_vehiculo($tVehiculo);
		$vehiculo->setPlaca($row["placa_vehiculo"]);
		array_push($vehiculos,$vehiculo);
	}
	return $vehiculos;


}


/*
	*Obtiene el objeto de esta clase
	*
	*@param $conexion
	*@return void
	*/
	public static function obtenerVehiculoDAO($conexion) {
            if(self::$VehiculoDAO == null) {
                self::$VehiculoDAO = new VehiculoDAO($conexion);
            }

            return self::$VehiculoDAO;
        }

}


?>
