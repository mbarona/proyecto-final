<?php
/**
 * Archivo de conexión a la base de datos
 */

require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';

require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/persistencia/util/Conexion.php';


/** 
 * Archivo de entidad
 */
require_once $_SERVER["DOCUMENT_ROOT"].'/proyecto-final/parkeaColombia/negocio/Parqueadero.php';

require_once ($_SERVER["DOCUMENT_ROOT"]).'/proyecto-final/parkeaColombia/negocio/Parqueadero.php';


/**
 * Interfaz DAO%
 */
require_once('DAO.php');

/**
 * Dao para los parqueaderos
 */
class ParqueaderoDAO implements DAO
{
	/**
	 * Conexión a la base de datos
	 * @var [Object]
	 */
	private $conexion;

	/**
	 * Objeto de la clase ParqueaderoDAO
	 * @var [ParqueaderoDAO]
	 */
	private static $parqueaderoDAO;


	/**
	 * Constructor de la clase
	 */
	
	private function __construct($conexion)
	{
		$this->conexion=$conexion;
		mysqli_set_charset($this->conexion, "utf8");
	}

/**
 * Realiza la consulta de un objeto
 * @param  [int] $codigo [Código del objeto a consultar]
 * @return [Parqueadero] Parqueadero encontrado
 */
public function consultar($codigo){
	$sentencia="SELECT * FROM PARQUEADERO WHERE cod_parqueadero=".$codigo;
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
        $row=mysqli_fetch_array($result);
        $parqueadero=new Parqueadero();
        $parqueadero->setCodParqueadero($row["cod_parqueadero"]);
        $parqueadero->setNomParqueadero($row["nom_parqueadero"]);
        $parqueadero->setCodZona($row["cod_zona"]);
        $parqueadero->setCodTipoParqueadero($row["cod_tipo_parqueadero"]);
        $parqueadero->setDireccion($row["direccion"]);
        $parqueadero->setLongitud($row["longitud"]);
        $parqueadero->setLatitud($row["latitud"]);
        $parqueadero->setEstado($row["estado"]);

    return $parqueadero;

}

/**
 * Crea un nuevo parqueadero en la base de datos
 * @param  Parqueadero $parqueaderonuevo
 * @return void
 */
public function crear ($parqueaderonuevo){
	$sentencia="INSERT INTO PARQUEADERO (nom_parqueadero,cod_zona,cod_tipo_parqueadero,direccion,longitud,latitud,estado) VALUES('".$parqueaderonuevo->getNomParqueadero()."',".$parqueaderonuevo->getCodZona().",".$parqueaderonuevo->getCodTipoParqueadero().",'".$parqueaderonuevo->getDireccion()."',".$parqueaderonuevo->getLongitud().",".$parqueaderonuevo->getLatitud().",'Abierto');";
	mysqli_query($this->conexion, $sentencia);

}

/**
 * Modifica un parqueadero ingresado por parámetro
 * @param  [Parqueadero] $parqueadero Parqueadero ingresado por parámetro
 * @return void          
 */
public function modificar($parqueadero){
	$sentencia="UPDATE PARQUEADERO SET nom_parqueadero='".$parqueadero->getNomParqueadero()."', cod_zona=".$parqueadero->getCodZona().", cod_tipo_parqueadero=".$parqueadero->getCodTipoParqueadero().", direccion='".$parqueadero->getDireccion()."',longitud=".$parqueadero->getLongitud().",latitud=".$parqueadero->getLatitud().",estado='".$parqueadero->getEstado()."' WHERE cod_parqueadero=".$parqueadero->getCodParqueadero();
	mysqli_query($this->conexion,$sentencia);
}

/**
 * Lista todos los objetos que se están en la tabla de parqueaderos
 * @return [Parqueaderos] 
 */
public function listarTodo(){
	$sentencia="SELECT * FROM PARQUEADERO";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$parqueaderos = array();

	while ($row = mysqli_fetch_array($result)) {
		$parqueadero = new Parqueadero();
		$parqueadero->setCodParqueadero($row["cod_parqueadero"]);
        $parqueadero->setNomParqueadero($row["nom_parqueadero"]);
        $parqueadero->setCodZona($row["cod_zona"]);
		$parqueadero->setCodTipoParqueadero($row["cod_tipo_parqueadero"]);
		$parqueadero->setDireccion($row["direccion"]);
        $parqueadero->setLongitud($row["longitud"]);
        $parqueadero->setLatitud($row["latitud"]);
        $parqueadero->setEstado($row["estado"]);
        array_push($parqueaderos,$parqueadero);
	}
	return $parqueaderos;
}


/**
 * Lista el numero de parqueaderos que hay en la base de datos
 * @return [int] numero de parqueaderos
 */
public function totalParqueaderos(){
	$sentencia="select count(*) from PARQUEADERO";
	$result = mysqli_query($this->conexion, $sentencia);
	$total=0;
	while ($row = $result->fetch_assoc()) {
		$total=$row['count(*)'];
	}
	return $total;
}

/*
*Obtiene el objeto de esta clase
*
*@param $conexion
*@return void
*/
public static function obtenerParqueaderoDAO($conexion_bd) {
	if(self::$parqueaderoDAO == null) {
		self::$parqueaderoDAO = new ParqueaderoDAO($conexion_bd);
	}
	return self::$parqueaderoDAO;
}



/**
 * Lista todos los parqueaderos disponibles que están en la tabla de parqueaderos
 * @return [Parqueaderos] 
 */
public function listarDisponibles(){
	$sentencia="SELECT * FROM PARQUEADERO where estado='D'";
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$parqueaderos = array();

	while ($row = mysqli_fetch_array($result)) {
		$parqueadero = new Parqueadero();
		$parqueadero->setCodParqueadero($row["cod_parqueadero"]);
        $parqueadero->setNomParqueadero($row["nom_parqueadero"]);
		$parqueadero->setCodZona($row["cod_zona"]);
		$parqueadero->setCodTipoParqueadero($row["cod_tipo_parqueadero"]);
		$parqueadero->setDireccion($row["direccion"]);
        $parqueadero->setLongitud($row["longitud"]);
        $parqueadero->setLatitud($row["latitud"]);
        $parqueadero->setEstado($row["estado"]);
        array_push($parqueaderos,$parqueadero);
	}
	return $parqueaderos;
}

/**
 * Lista todos los parqueaderos por una zona que estan en la tabla de parqueaderos
 * @param codzona codigo de a zona de los parqueaderos a listar
 * @return [Parqueaderos] 
 */
public function listarParqueaderosZona($codzona){
	$sentencia="SELECT * FROM PARQUEADERO where cod_zona=".$codzona;
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$parqueaderos = array();

	while ($row = mysqli_fetch_array($result)) {
		$parqueadero = new Parqueadero();
		$parqueadero->setCodParqueadero($row["cod_parqueadero"]);
        $parqueadero->setNomParqueadero($row["nom_parqueadero"]);
		$parqueadero->setCodZona($row["cod_zona"]);
		$parqueadero->setCodTipoParqueadero($row["cod_tipo_parqueadero"]);
		$parqueadero->setDireccion($row["direccion"]);
        $parqueadero->setLongitud($row["longitud"]);
        $parqueadero->setLatitud($row["latitud"]);
        $parqueadero->setEstado($row["estado"]);
        array_push($parqueaderos,$parqueadero);
	}
	return $parqueaderos;
}


/**
 * Lista el ultimo parqueadero registrado
 * @return [Parqueadero] 
 */
public function listarUltimo(){
	$sentencia="SELECT cod_parqueadero FROM PARQUEADERO ORDER by cod_parqueadero DESC limit 1";
	if(!$result=mysqli_query($this->conexion,$sentencia))die();
		$row=mysqli_fetch_array($result);
		$parqueadero=new Parqueadero();
		$parqueadero->setCodParqueadero($row["cod_parqueadero"]);

	return $parqueadero;
	
}
	



/**
 * Lista todos la cantidad de parqueaderos por una zona que estan en la tabla de parqueaderos
 * @param codzona codigo de la zona de los parqueaderos a listar
 * @return [Parqueaderos] 
 */
public function cantidadParqueaderosZona($codzona){
	$sentencia="select count(cod_parqueadero) as total from PARQUEADERO where PARQUEADERO.cod_zona=".$codzona;
	if(!$result = mysqli_query($this->conexion, $sentencia)) die();
	$total = 0;

	while ($row = mysqli_fetch_array($result)) {
		$total=$row['total'];
	}
	return $total;
}
/**
 * Lista todos los parqueaderos disponibles que se están en la tabla de parqueaderos
 * @return [parqueaderos] 
 */
public function listarParqueaderosZonaCiudad(){
	$sentencia="SELECT cod_parqueadero, nom_parqueadero, nom_zona, nom_ciudad from PARQUEADERO, ZONA, CIUDAD WHERE PARQUEADERO.cod_zona=ZONA.cod_zona AND ZONA.cod_ciudad = CIUDAD.cod_ciudad";
	if(!$parqueaderos = mysqli_query($this->conexion, $sentencia)) die();
		return $parqueaderos;

}

}
?>